FROM nginx

COPY ./build/ /usr/share/nginx/html/app/
COPY ./nginx/ /etc/nginx/templates/

#COPY docker-compose-prod.yaml /build

#VOLUME ["/build"]