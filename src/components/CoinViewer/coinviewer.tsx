
import React, { useContext }               from 'react';
import {
	BigNumber,
	CollateralItem,
	ERC20Type,
	_AssetType,
	combineURLs,
	compactString,
	tokenToFloat
} from '@envelop/envelop-client-core';

import default_icon from '../../static/pics/coins/_default.svg';
import default_nft  from '../../static/pics/coins/_default_nft.svg';
import {
	ERC20Context,
	Web3Context
} from '../../dispatchers';
import TippyWrapper from '../TippyWrapper';

type CoinViewerProps = {
	tokens        : Array<CollateralItem>,
	position?     : string,
	onClick?      : Function,
	onMouseEnter? : Function,
	onMouseLeave? : Function,
}

export default function CoinViewer(props: CoinViewerProps) {

	const {
		tokens,
		position,
		onClick,
		onMouseEnter,
		onMouseLeave,
	} = props;

	const {
		currentChain,
	} = useContext(Web3Context);
	const {
		erc20List,
		requestERC20Token,
	} = useContext(ERC20Context);

	const getCollateralItem = (item: CollateralItem) => {

		if ( !currentChain ) { return null; }

		// native token
		if ( item.assetType === _AssetType.native && item.amount ) {
			return (
				<tr key={ 'native' }>
					<td>
						<span className="unit-sum">
							{ tokenToFloat(item.amount, currentChain.decimals).toString() }
						</span>
					</td>
					<td>
						<span className="field-unit">
							<span className="i-coin">
								<img src={ currentChain.tokenIcon || default_icon } alt="" />
							</span>
							{ currentChain.symbol }
						</span>
					</td>
				</tr>
			)
		}

		if ( item.assetType === _AssetType.ERC20 && item.amount ) {
			// Common ERC20
			const foundERC20 = erc20List.find((iitem: ERC20Type) => {
				return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase()
			});
			if ( foundERC20 ) {
				// known ERC20
				return (
					<tr key={ item.contractAddress }>
						<td>
							<span className="unit-sum">
								{ tokenToFloat(item.amount, foundERC20.decimals || 18).toString() }
							</span>
						</td>
						<td>
							<span className="field-unit">
								<span className="i-coin">
									<img src={ foundERC20.icon || default_icon } alt="" />
								</span>
								<a target="_blank" rel="noopener noreferrer" href={ combineURLs(currentChain.explorerBaseUrl, `/token/${foundERC20.contractAddress}`) }>
									{ foundERC20.symbol }
								</a>
							</span>
						</td>
					</tr>
				)
			} else {
				// unknown ERC20
				requestERC20Token(item.contractAddress);
				return (
					<tr key={ item.contractAddress }>
						<td>
							<span className="unit-sum">
								<TippyWrapper msg={ 'decimals is unknown; amount shown in wei' }>
									<span className="unit-sum">
										{ item.amount.toString() }*
									</span>
								</TippyWrapper>
							</span>
						</td>
						<td>
							<span className="field-unit">
								<span className="i-coin">
									<img src={ default_icon } alt="" />
								</span>
								<a target="_blank" rel="noopener noreferrer" href={ combineURLs(currentChain.explorerBaseUrl, `/token/${item.contractAddress}`) }>
									{ compactString(item.contractAddress) }
								</a>
							</span>
						</td>
					</tr>
				)
			}
		}

		if ( item.assetType === _AssetType.ERC721 ) {
			return (
				<tr key={ `${item.contractAddress}${item.tokenId}` }>
					<td>
						<span className="unit-sum">
							<span className="unit-sum">
								{ item.amount && !item.amount.eq(0) ? item.amount.toString() : '1' } { ' ' } ({ currentChain.EIPPrefix }-721)
							</span>
						</span>
					</td>
					<td>
						<span className="field-unit">
							<span className="i-coin">
								<img src={ item.tokenImg || default_nft } alt="" />
							</span>
							<a target="_blank" rel="noopener noreferrer" href={ combineURLs(currentChain.explorerBaseUrl, `/token/${item.contractAddress}?a=${item.tokenId || ''}`) }>
								{ compactString(item.contractAddress) }
							</a>
						</span>
					</td>
				</tr>
			)
		}

		if ( item.assetType === _AssetType.ERC1155 ) {
			return (
				<tr key={ `${item.contractAddress}${item.tokenId}` }>
					<td>
						<span className="unit-sum">
							<span className="unit-sum">
								{ item.amount && !item.amount.eq(0) ? item.amount.toString() : '1' } { ' ' } ({ currentChain.EIPPrefix }-1155)
							</span>
						</span>
					</td>
					<td>
						<span className="field-unit">
							<span className="i-coin">
								<img src={ item.tokenImg || default_nft } alt="" />
							</span>
							<a target="_blank" rel="noopener noreferrer" href={ combineURLs(currentChain.explorerBaseUrl, `/token/${item.contractAddress}?a=${item.tokenId || ''}`) }>
								{ compactString(item.contractAddress) }
							</a>
						</span>
					</td>
				</tr>
			)
		}

		return null;
	}


	return (
		<div
			className    = { `field-collateral__details ${ position || '' }` }
			onClick      = {() => { if (onClick) { onClick() } }}
			onMouseEnter = {() => { if (onMouseEnter) { onMouseEnter() } }}
			onMouseLeave = {() => { if (onMouseLeave) { onMouseLeave() } }}
		>
		<div className="inner">
			<table>
				<tbody>
					{
						tokens
							.sort((item, prev) => {
								if ( item.assetType < prev.assetType ) { return -1 }
								if ( item.assetType > prev.assetType ) { return  1 }

								if ( item.contractAddress.toLowerCase() < prev.contractAddress.toLowerCase() ) { return -1 }
								if ( item.contractAddress.toLowerCase() > prev.contractAddress.toLowerCase() ) { return  1 }

								if ( item.tokenId && prev.tokenId ) {
									try {
										if ( new BigNumber(item.tokenId).isNaN() || new BigNumber(prev.tokenId).isNaN() ) {
											if ( parseInt(`${item.tokenId}`) < parseInt(`${prev.tokenId}`) ) { return -1 }
											if ( parseInt(`${item.tokenId}`) > parseInt(`${prev.tokenId}`) ) { return  1 }
										}
										const itemTokenIdNumber = new BigNumber(item.tokenId);
										const prevTokenIdNumber = new BigNumber(prev.tokenId);

										if ( itemTokenIdNumber.lt(prevTokenIdNumber) ) { return -1 }
										if ( itemTokenIdNumber.gt(prevTokenIdNumber) ) { return  1 }
									} catch ( ignored ) {
										if ( `${item.tokenId}`.toLowerCase() < `${prev.tokenId}`.toLowerCase() ) { return -1 }
										if ( `${item.tokenId}`.toLowerCase() > `${prev.tokenId}`.toLowerCase() ) { return  1 }
									}
								}

								return 0
							})
							.map((item) => { return getCollateralItem(item) })
					}
				</tbody>
			</table>
		</div>
		</div>
	)

}