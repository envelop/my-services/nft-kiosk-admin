import {
	NFT, Web3, _AssetType, combineURLs, getChainId, setApprovalERC1155, setApprovalERC721, setApprovalForAllERC721
} from "@envelop/envelop-client-core";
import { useContext, useState } from "react";
import { InfoModalContext, Web3Context, _ModalTypes } from "../../dispatchers";

type ApprovePopupProps = {
	token     : NFT,
	closePopup: Function,
}

export default function ApprovePopup(props: ApprovePopupProps) {

	const {
		token,
		closePopup
	} = props;

	const [ inputApproveToAddress, setInputApproveToAddress ] = useState('');

	const {
		setLoading,
		setModal,
		unsetModal,
	} = useContext(InfoModalContext);
	const {
		web3,
		getWeb3Force,
		currentChain,
		userAddress,
	} = useContext(Web3Context);

	const approveSubmit = async (all: boolean) => {
		if ( !currentChain ) { return; }
		let _web3 = web3;
		let _userAddress = userAddress;
		let _currentChain = currentChain;

		if ( !web3 || !userAddress || await getChainId(web3) !== _currentChain.chainId ) {
			setLoading('Waiting for wallet');

			try {
				const web3Params = await getWeb3Force(_currentChain.chainId);

				_web3 = web3Params.web3;
				_userAddress = web3Params.userAddress;
				_currentChain = web3Params.chain;
				unsetModal();
			} catch(e: any) {
				setModal({
					type: _ModalTypes.error,
					title: 'Error with wallet',
					details: [
						e.message || e
					]
				});
			}
		}

		if ( !_web3 || !_userAddress || !_currentChain ) { return; }

		setLoading('Waiting to approve');

		let txResp: any;
		try {
			if ( all ) {
				if ( token.assetType === _AssetType.ERC1155 ) {
					txResp = await setApprovalERC1155(_web3, token.contractAddress, _userAddress, inputApproveToAddress);
				} else {
					txResp = await setApprovalForAllERC721(_web3, token.contractAddress, _userAddress, inputApproveToAddress);
				}
			} else {
				txResp = await setApprovalERC721(_web3, token.contractAddress, token.tokenId, _userAddress, inputApproveToAddress);
			}
		} catch(e: any) {
			setModal({
				type: _ModalTypes.error,
				title: all ? `Cannot approve tokens`: `Cannot approve token`,
				details: [
					all ? `Token to approve: all of ${token.contractAddress}` : `Token to approve ${token.contractAddress}: ${token.tokenId}`,
					`User address: ${_userAddress}`,
					`Address to: ${inputApproveToAddress}`,
					'',
					e.message || e,
				]
			});
			return;
		}

		unsetModal();
		setModal({
			type: _ModalTypes.success,
			title: all ? `Tokens successfully approved` : `Tokens successfully approved`,
			buttons: [{
				text: 'Ok',
				clickFunc: () => {
					unsetModal();
					closePopup();
				}
			}],
			links: [{
				text: `View on ${_currentChain.explorerName}`,
				url: combineURLs(_currentChain.explorerBaseUrl, `/tx/${txResp.transactionHash}`)
			}],
		});
	}

	const getApproveAllSubmitBtn = () => {
		return (
			<div className="col col-12 col-sm-4 mb-2">
				<button
					className="btn btn-yellow w-100"
					type="button"
					disabled={ inputApproveToAddress === '' || !Web3.utils.isAddress(inputApproveToAddress) }
					onClick={() => {
						approveSubmit(true);
					}}
				>Approve all</button>
			</div>
		)
	}
	const getApproveSubmitBtn = () => {
		if ( token.assetType  === _AssetType.ERC1155 ) { return null; }

		return (
			<div className="col col-12 col-sm-4 mb-2">
				<button
					className="btn w-100"
					type="button"
					disabled={ inputApproveToAddress === '' || !Web3.utils.isAddress(inputApproveToAddress) }
					onClick={() => {
						approveSubmit(false);
					}}
				>Approve</button>
			</div>
		)
	}

	return (
		<div className="modal">
			<div
				className="modal__inner"
				onClick={(e) => {
					e.stopPropagation();
					if ((e.target as HTMLTextAreaElement).className === 'modal__inner') {
						closePopup();
					}
				}}
			>
				<div className="modal__bg"></div>
				<div className="container">
					<div className="modal__content">
						<div
							className="modal__close"
							onClick={() => { closePopup() }}
						>
							<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
								<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
							</svg>
						</div>
						<div className="c-add">
							<div className="c-add__text">
								<div className="h2">Set approval for token</div>
								<div className="d-flex"><span className="text-muted">Allow the specified address to manage only the current NFT token or all NFT tokens of the smart contract that created them</span></div>
							</div>
							<div className="c-add__coins">
								<div className="c-add__form">

										<div className="form-row">
											<div className="col col-12">
												<div className="input-group">
													<input
														className="input-control"
														type="text"
														placeholder={ 'Paste address' }
														value={ inputApproveToAddress }
														onChange={(e) => { setInputApproveToAddress(e.target.value) }}
													/>
												</div>
											</div>

											{ getApproveSubmitBtn() }
											{ getApproveAllSubmitBtn() }
										</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)

}