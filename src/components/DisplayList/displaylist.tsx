
import {
	BigNumber,
	getDisplaysOfUserFromAPI,
	_Display,
	dateToStr,
	unixtimeToDate,
} from "@envelop/envelop-client-core";
import {
	useContext,
	useEffect,
	useState
} from "react"
import { Link } from "react-router-dom";
import { Web3Context } from "../../dispatchers";

export default function DisplayList() {

	const {
		currentChain,
		userAddress,
		getWeb3Force,
	} = useContext(Web3Context);

	const [ displayList, setDisplayList ] = useState<Array<_Display>>([]);

	useEffect(() => {

		if ( !currentChain ) { return; }
		if ( !userAddress  ) { return; }

		const fetchDisplays = async (page: number, addedTokens: Array<_Display>) => {
			getDisplaysOfUserFromAPI(currentChain.chainId, userAddress, page)
				.then((data) => {

					if ( !data ) { return; }

					const tokensOnPage = 9;

					const combinedList = [
						...addedTokens,
						...data
					];

					setDisplayList(combinedList);

					if ( data.length === tokensOnPage ) {
						fetchDisplays(page + 1, combinedList)
					}
				})
				.catch((e) => { console.log('Cannot fetch user NFTs', e); })
		}

		fetchDisplays(1, []);

	}, [ currentChain, userAddress ])

	const getDisplays = () => {

		if ( !userAddress ) {
			return (
				<div className="db-section">
				<div className="container">
				<div className="row justify-content-center">
					<div className="col-auto">
						<button
							className="btn btn-lg btn-outline"
							onClick={async () => {try { await getWeb3Force(); } catch(e: any) { console.log('Cannot connect', e); } }}
						>
							Connect to&nbsp;your wallet to&nbsp;load&nbsp;showcases
						</button>
					</div>
				</div>
				</div>
				</div>
			)
		}

		return (
			<div className="container">
				<div className="ms-content">
					<div className="row justify-content-sm-between mb-5">
						<div className="col-auto">
							<h2 className="h3 mb-0">Showcases</h2>
						</div>
						{
							userAddress ? (
								<div className="col-auto">
									<Link
										className="btn btn-outline"
										to="/showcase"
									>
										Create a new showcase
									</Link>
								</div>
							) : null
						}
					</div>
					<div className="row lp-cards">
						{
							displayList.map((item) => {
								return (
									<div
									key={`${item.chain_id}${item.contract_address}${item.name_hash}`}
										className="col-12 col-sm-6 col-lg-4"
									>
										<Link
											className="lp-card"
											to={`/showcase/${item.chain_id}/${item.contract_address}/${item.name_hash}`}
										>
											<div className="h4 mb-2">{ item.title || '<Untitled>' }</div>
											<p>{ item.description }</p>
											<div className="stat">
												<div className="row">
													<div className="col-auto">
														<div className="t-tag">Amount</div>
														<div className="value">{ item.tokens_count || 0 } { item.tokens_count === 1 ? 'NFT' : 'NFTs' }</div>
													</div>
													<div className="col-auto">
														<div className="t-tag">Available</div>
														<div className="value">{ dateToStr(unixtimeToDate(new BigNumber(item.enable_after))) } — { dateToStr(unixtimeToDate(new BigNumber(item.disable_after))) }</div>
													</div>
												</div>
											</div>
										</Link>
									</div>
								)
							})
						}
					</div>
				</div>
			</div>
		)

	}

	return getDisplays();

}