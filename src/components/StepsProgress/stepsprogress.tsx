
import { useContext } from "react";
import {
	Link,
	useNavigate
} from "react-router-dom";

import { Display } from "@envelop/envelop-client-core"

import {
	InfoModalContext,
	_ModalTypes
} from "../../dispatchers";

type StepsProgressProps = {
	current: string,
	display: Display | undefined,
	confirmClose?: boolean,
}

export default function StepsProgress(props: StepsProgressProps) {

	const {
		current,
		display,
		confirmClose
	} = props;

	const {
		setModal,
		unsetModal,
	} = useContext(InfoModalContext);
	const navigate = useNavigate();

	const steps = [
		{ name: 'showcase', text: 'Showcase', linkPrefix: 'showcase' },
		// { name: 'project',  text: 'Project',  linkPrefix: 'project'  },
		{ name: 'price',    text: 'Price',    linkPrefix: 'price'    },
		{ name: 'discount', text: 'Discount', linkPrefix: 'discount' },
		{ name: 'token',    text: 'NFTs',     linkPrefix: 'token'    },
		{ name: 'summary',  text: 'Summary',  linkPrefix: 'summary'  },
	];

	const getLink = (item: typeof steps[0]) => {

		const clazz = current.toLowerCase() === item.name ? 'current' : undefined;

		if ( !display ) {
			return (
				<li key={ item.name }>
					<div className={ `lp-steps-nav__item ${clazz || 'disabled'}` }>
						<span>{ item.text }</span>
					</div>
				</li>
			)
		}

		if ( confirmClose ) {
			return (
				<li key={ item.name }>
					<button
						className={ `lp-steps-nav__item ${clazz || ''}` }
						onClick={() => {
						setModal({
							type: _ModalTypes.info,
							title: 'You have unsaved data. Continue?',
							buttons: [
								{
									text: 'Yes',
									clickFunc: () => {
										navigate(`/${item.linkPrefix}/${display.chainId}/${display.contractAddress}/${display.nameHash}`);
										unsetModal();
									},
								},
								{
									text: 'No',
									clickFunc: () => { unsetModal(); },
									clazz: 'btn-grad'
								},
							]
						})
					}}>
						<span>{ item.text }</span>
					</button>
				</li>
			)
		}

		return (
			<li key={ item.name }>
				<Link
					className={ `lp-steps-nav__item ${clazz || ''}` }
					to={ `/${item.linkPrefix}/${display.chainId}/${display.contractAddress}/${display.nameHash}` }
				>
					<span>{ item.text }</span>
				</Link>
			</li>
		)
	}
	return (
		<ul className="lp-steps-nav">
			{ steps.map((item) => { return getLink(item) }) }
		</ul>
	)
}