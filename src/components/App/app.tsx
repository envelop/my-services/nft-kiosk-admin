
import React, {
	useContext,
	useEffect,
	useState
} from 'react';
import config from '../../app.config.json';
import {
	Route,
	Routes,
	useLocation,
	useNavigate,
} from "react-router-dom";
import 'tippy.js/dist/tippy.css';

import Header              from '../Header';
import Footer              from '../Footer';

import {
	DiscountsStepPage,
	PriceStepPage,
	ProjectStepPage,
	ShowcaseStepPage,
	TokensStepPage,
	Summary,
} from '../StepPages';
import { DisplayList } from '../DisplayList';
import {
	getDisplayFromAPI,
	Display,
	getDisplayParamsFromAPI,
	DisplayParam,
	_Display,
} from '@envelop/envelop-client-core';
import {
	InfoModalContext,
	SubscriptionDispatcher,
	Web3Context,
	_ModalTypes
} from '../../dispatchers';

import bg_right from '../../static/pics/../pics/bg/ms-right.svg';
import bg_left from '../../static/pics/../pics/bg/ms-left.svg';

export default function App() {

	const [ displayLoading, setDisplayLoading ] = useState(false);
	const [ displayToEdit, setDisplayToEdit ] = useState<Display | undefined>(undefined);
	const [ kioskAddress,  setKioskAddress  ] = useState('');

	const {
		setLoading,
		unsetModal,
		setModal
	} = useContext(InfoModalContext);
	const {
		web3,
		currentChain,
		userAddress
	} = useContext(Web3Context);

	const location = useLocation();
	const navigate = useNavigate();
	useEffect(() => {
		if ( !location ) { return; }
		const foundArgs = location.pathname.match(/\/\w*\/(?<chainId>\d*)\/(?<contractAddress>\w*)\/(?<displayHash>\w*)/);
		if ( userAddress && foundArgs && foundArgs.groups ) {
			const { chainId, contractAddress, displayHash } = foundArgs.groups;

			if (
				!displayToEdit ||
				displayToEdit.contractAddress.toLowerCase() !== contractAddress.toLowerCase() ||
				displayToEdit.nameHash.toLowerCase() !== displayHash.toLowerCase() ||
				displayToEdit.chainId !== parseInt(chainId)
			) {
				setDisplayToEdit(undefined);
				setLoading('Loading showcase');
				fetchDisplay(parseInt(chainId), contractAddress, displayHash);
			}
		} else {
			setDisplayToEdit(undefined);
		}

		//eslint-disable-next-line
	}, [ location ]);
	useEffect(() => {

		parseUrlAndFetchDisplay();
		getKioskAddress();

		//eslint-disable-next-line
	}, [ currentChain, userAddress, web3 ]);

	const parseUrlAndFetchDisplay = async () => {

		if ( !userAddress ) { return; }
		if ( web3 === undefined ) { return; }

		const foundArgs = window.location.pathname.match(/\/\w*\/(?<chainId>\d*)\/(?<contractAddress>\w*)\/(?<displayHash>\w*)/);
		if ( foundArgs && foundArgs.groups ) {

			if ( web3 === null ) {
				setModal({
					type: _ModalTypes.error,
					title: `Authorization error`,
					text: [{ text: `Login with your wallet to edit display` }],
					buttons: [{
						text: 'Ok',
						clickFunc: () => {
							unsetModal();
							navigate('/');
						}
					}],
				});
				return;
			}

			const { chainId, contractAddress, displayHash } = foundArgs.groups;
			setLoading('Loading showcase');
			fetchDisplay(parseInt(chainId), contractAddress, displayHash);
		}
	}
	const fetchDisplay = async (chainId: number, contractAddress: string, displayHash: string) => {

		if ( displayLoading ) { return; }

		setDisplayLoading(true);
		console.log('fetch showcase',  chainId, contractAddress, displayHash);
		getDisplayFromAPI(chainId, contractAddress, displayHash)
			.then((data: Display | undefined) => {
				if ( !data ) { return; }
				console.log('Showcase', data);
				if ( data.owner.toLowerCase() !== userAddress?.toLowerCase() ) {
					setModal({
						type: _ModalTypes.error,
						text: [{ text: `Not yours showcase` }],
						buttons: [{
							text: 'Ok',
							clickFunc: () => {
								unsetModal();
								navigate('/');
							}
						}],
					});
					setDisplayLoading(false);
					return;
				}

				getDisplayParamsFromAPI(chainId, contractAddress, displayHash)
					.then((ddata: Array<DisplayParam> | undefined) => {
						if ( !ddata ) { return; }

						setDisplayToEdit({
							...data,
							params: ddata
						});
						unsetModal();
						setDisplayLoading(false);
					})
					.catch((e) => {
						console.log('Cannot load showcase params', e);
						setDisplayToEdit(data);
						unsetModal();
						setDisplayLoading(false);
					})
			})
	}
	const getKioskAddress = () => {
		if ( !currentChain ) { return; }
		const foundChainConfig = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === currentChain.chainId });
		if ( !foundChainConfig ) { return; }
		if ( !foundChainConfig.launchpadContract ) { return; }

		setKioskAddress(foundChainConfig.launchpadContract);
	}

	return (
		<>
			<Header />

			<main className="s-main">
				<img className="ms-bg-right" src={ bg_right } alt="" />
				<img className="ms-bg-left" src={ bg_left } alt="" />
				<Routes>
					<Route path={ `/` } element={<DisplayList />}>
					</Route>

					<Route
						path={ `/showcase/:chainId?/:contractAddress?/:displayHash?` }
						element={
							<SubscriptionDispatcher
								TX_names={{ singular: 'launchpad', plural: 'launchpads' }}
								serviceContract={ kioskAddress }
							>
								<ShowcaseStepPage
									displayToEdit={ displayToEdit }
									onUpdate={() => { parseUrlAndFetchDisplay(); }}
									kioskAddress={ kioskAddress }
								/>
							</SubscriptionDispatcher>
						}
					>
					</Route>

					<Route
						path={ `/project/:chainId?/:contractAddress?/:displayHash?` }
						element={
							<ProjectStepPage
								displayToEdit={ displayToEdit }
								onUpdate={() => { parseUrlAndFetchDisplay(); }}
							/>
						}
					>
					</Route>

					<Route
						path={ `/price/:chainId?/:contractAddress?/:displayHash?` }
						element={
							<PriceStepPage
								onUpdate={() => { parseUrlAndFetchDisplay(); }}
								displayToEdit={ displayToEdit }
							/>
						}
					>
					</Route>

					<Route
						path={ `/discount/:chainId?/:contractAddress?/:displayHash?` }
						element={
							<DiscountsStepPage
								displayToEdit={ displayToEdit }
								onUpdate={() => { parseUrlAndFetchDisplay(); }}
							/>
						}
					>
					</Route>

					<Route
						path={ `/token/:chainId?/:contractAddress?/:displayHash?` }
						element={
							<TokensStepPage
								displayToEdit={ displayToEdit }
								onUpdate={() => { parseUrlAndFetchDisplay(); }}
							/>
						}
					>
					</Route>

					<Route
						path={ `/summary/:chainId?/:contractAddress?/:displayHash?` }
						element={
							<Summary
								displayToEdit={ displayToEdit }
							/>
						}
					>
					</Route>
				</Routes>
			</main>

			<Footer />
		</>
	)

}