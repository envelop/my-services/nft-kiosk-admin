
import React, {
	useContext,
	useEffect,
	useState
} from "react";

import {
	BigNumber,
	DenominatedPrice,
	ERC20Type,
	Price,
	Web3,
	_AssetType,
	addThousandSeparator,
	compactString,
	derationalize,
	removeThousandSeparator,
	tokenToFloat,
	tokenToInt,
} from "@envelop/envelop-client-core";
import { AddedBatch } from "./tokens";
import {
	ERC20Context,
	Web3Context
} from "../../../dispatchers";
import CoinSelector from "../../CoinSelector";

import default_coin from "@envelop/envelop-client-core/static/pics/coins/_default.svg";
import i_edit       from "../../../static/pics/icons/i-edit.svg";
import i_del        from "../../../static/pics/icons/i-del.svg";
import i_lock       from "../../../static/pics/icons/i-lock.svg";

type DefaultEditableField = {
	currentValue: {
		token     : string,
		amount    : string,
	},
	editedValue?: {
		token     : string,
		amount    : string,
	},
	isEdit: boolean,
	id: number,
	idx: number,
	del?: boolean,
}

type TokenPriceModalProps = {
	closePopup     : (batch?: AddedBatch) => void,
	batch          : AddedBatch,
	defaultPrice   : Array<Price>
	collateralPrice: Array<{ collateralToken: string, price: Array<DenominatedPrice> }>
}

export default function TokenPriceModal(props: TokenPriceModalProps) {

	const {
		closePopup,
		batch,
		defaultPrice,
		collateralPrice,
	} = props;

	const {
		erc20List,
		requestERC20Token
	} = useContext(ERC20Context);
	const {
		userAddress
	} = useContext(Web3Context);

	const [ inputPriceType      , setInputPriceType       ] = useState<'default' | 'special'>('default');

	const [ inputDefaultPriceToken , setInputDefaultPriceToken  ] = useState('');
	const [ inputDefaultPriceAmount, setInputDefaultPriceAmount ] = useState('');
	const [ defaultPriceToAdd      , setDefaultPriceToAdd       ] = useState<Array<DefaultEditableField>>([]);
	const [ defaultPriceSaved      , setDefaultPriceSaved       ] = useState<Array<DefaultEditableField>>([]);
	const [ defaultPriceToEdit     , setDefaultPriceToEdit      ] = useState<Array<DefaultEditableField>>([]);
	const [ defaultPriceToDel      , setDefaultPriceToDel       ] = useState<Array<DefaultEditableField>>([]);


	useEffect(() => {
		if ( batch.nftPrices && batch.nftPrices.length ) {
			setInputPriceType('special');
			setDefaultPriceSaved(batch.nftPrices.map((item) => {

				let foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === item.payWith.toLowerCase() });
				if ( !foundToken ) {
					foundToken = {
						assetType: _AssetType.ERC20,
						contractAddress: item.payWith,
						decimals: 18,
						balance: new BigNumber(0),
						icon: default_coin,
						name: compactString(item.payWith),
						symbol: compactString(item.payWith),
						allowance: [],
					}
				}

				return {
					id: item.idx,
					idx: item.idx,
					isEdit: false,
					currentValue: {
						token: item.payWith,
						amount: tokenToFloat(item.amount, foundToken.decimals).toString()
					}
				}
			}));
		}

		if ( batch.edit && batch.edit.length ) {
			setInputPriceType('special');
			batch.edit.forEach((item) => {
				if ( item.action !== 'del' ) { return; }
				if ( !item.nftPricesUpdated ) { return; }
				setDefaultPriceSaved((prevState) => { return prevState.filter((iitem) => { if ( item.nftPricesUpdated ) { return item.nftPricesUpdated[0].idx !== iitem.idx } }) })
			})

			const addPrices: Array<DefaultEditableField> = [];
			batch.edit.forEach((item) => {
				if ( item.action !== 'add' ) { return; }
				if ( !item.nftPricesUpdated ) { return; }

				item.nftPricesUpdated.forEach((iitem) => {
					let foundToken = erc20List.find((iiitem) => { return iiitem.contractAddress.toLowerCase() === iitem.payWith.toLowerCase() });
					if ( !foundToken ) {
						foundToken = {
							assetType: _AssetType.ERC20,
							contractAddress: iitem.payWith,
							decimals: 18,
							balance: new BigNumber(0),
							icon: default_coin,
							name: compactString(iitem.payWith),
							symbol: compactString(iitem.payWith),
							allowance: [],
						}
					}

					addPrices.push({
						id: iitem.idx,
						idx: iitem.idx,
						isEdit: false,
						currentValue: {
							token: iitem.payWith,
							amount: tokenToFloat(iitem.amount, foundToken.decimals).toString()
						}
					})
				})
			})
			setDefaultPriceToAdd(addPrices);

			const editPrices: Array<DefaultEditableField> = [];
			batch.edit.forEach((item) => {
				if ( item.action !== 'edit' ) { return; }
				if ( !item.nftPricesUpdated ) { return; }

				item.nftPricesUpdated.forEach((iitem) => {
					let foundToken = erc20List.find((iiitem) => { return iiitem.contractAddress.toLowerCase() === iitem.payWith.toLowerCase() });
					if ( !foundToken ) {
						foundToken = {
							assetType: _AssetType.ERC20,
							contractAddress: iitem.payWith,
							decimals: 18,
							balance: new BigNumber(0),
							icon: default_coin,
							name: compactString(iitem.payWith),
							symbol: compactString(iitem.payWith),
							allowance: [],
						}
					}

					editPrices.push({
						id: iitem.idx,
						idx: iitem.idx,
						isEdit: false,
						currentValue: {
							token: iitem.payWith,
							amount: tokenToFloat(iitem.amount, foundToken.decimals).toString()
						}
					})

					setDefaultPriceSaved((prevState) => { return prevState.filter((iiitem) => { if ( item.nftPricesUpdated ) { return iitem.idx !== iiitem.idx } }) })
				})
			})
			setDefaultPriceToEdit(editPrices);
		}

	}, [])

	const getPageSelector = () => {
		return (
			<div className="row row-sm mb-5">
				<div className="col-auto py-2">
					<label className="checkbox">
						<input
							type="radio"
							name="price-type"
							disabled={ batch.source === 'saved' && batch.nftPrices && !!batch.nftPrices.length }
							checked={ inputPriceType === 'default' }
							onChange={() => { setInputPriceType('default') }}
						/>
						<span className="check"> </span>
						<span className="check-text">Default price</span>
					</label>
				</div>
				<div className="col-auto py-2">
					<label className="checkbox">
						<input
							type="radio"
							name="price-type"
							checked={ inputPriceType === 'special' }
							onChange={() => { setInputPriceType('special') }}
						/>
						<span className="check"> </span>
						<span className="check-text">Special price</span>
					</label>
				</div>
			</div>
		)
	}

	const getDefaultPricesList = (prices: Array<Price>) => {
		return (
			<div className="c-wrap__table light mb-6">
				{
					prices.map((item) => {
						let foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === item.payWith.toLowerCase() });
						if ( !foundToken ) {
							foundToken = {
								assetType: _AssetType.ERC20,
								contractAddress: item.payWith,
								decimals: 18,
								balance: new BigNumber(0),
								icon: default_coin,
								name: compactString(item.payWith),
								symbol: compactString(item.payWith),
								allowance: [],
							}
						}

						return (
							<div className="item" key={`${item.payWith}`}>
								<div className="row">
									<div className="col-auto col-md-2 mb-2">
									<div className="tb-coin">
										<span className="i-coin"><img src={ foundToken.icon } alt="" /></span>
										<span className="name">{ foundToken.symbol }</span></div>
									</div>
									<div className="col mb-2 d-flex align-items-center">
										<span className="text-break">{ tokenToFloat(item.amount, foundToken.decimals).toString() }</span>
									</div>
								</div>
							</div>
						)
					})
				}
			</div>
		)
	}
	const getCollateralPricesList = (prices: Array<{ collateralToken: string, price: Array<DenominatedPrice> }>) => {
		return (
			<div className="c-wrap__table light mb-6">
				{
					prices.map((item) => {
						let foundCollateralToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === item.collateralToken.toLowerCase() });
						if ( !foundCollateralToken ) {
							foundCollateralToken = {
								assetType: _AssetType.ERC20,
								contractAddress: item.collateralToken,
								decimals: 18,
								balance: new BigNumber(0),
								icon: default_coin,
								name: compactString(item.collateralToken),
								symbol: compactString(item.collateralToken),
								allowance: [],
							}
						}

						return (
							<React.Fragment key={ item.collateralToken }>
								<div className="d-flex align-center my-3">
									<span className="i-coin mr-2">
										<img src={ foundCollateralToken.icon } alt="" />
									</span>
									<span className="name">
										<b className="mr-1">1 { foundCollateralToken.symbol } </b>for
									</span>
								</div>
								<div className="c-wrap__table light mb-6">
								{
									item.price.map((iitem) => {
										let foundToken = erc20List.find((iiitem) => { return iitem.payWith.toLowerCase() === iiitem.contractAddress.toLowerCase() });
										if ( !foundToken ) {
											foundToken = {
												assetType: _AssetType.ERC20,
												contractAddress: iitem.payWith,
												decimals: 18,
												balance: new BigNumber(0),
												icon: default_coin,
												name: compactString(iitem.payWith),
												symbol: compactString(iitem.payWith),
												allowance: [],
											}
										}
										return (
											<div className="item" key={ iitem.payWith }>
												<div className="row">
													<div className="col-auto col-md-2 mb-2">
														<div className="tb-coin">
															<span className="i-coin">
																<img src={ foundToken.icon } alt="" />
															</span>
															<span className="name">{ foundToken.symbol }</span>
														</div>
													</div>
													<div className="col mb-2 d-flex align-items-center">
														<span className="text-break">{ tokenToFloat(derationalize(iitem.amount, iitem.denominator, new BigNumber((foundCollateralToken?.decimals || 18) - foundToken.decimals))).toString() }</span>
													</div>
												</div>
											</div>
										)
									})
								}
								</div>
							</React.Fragment>
						)
					})
				}
			</div>
		)
	}
	const getDefaultPrices = () => {

		if ( batch.batchType === 'nft' ) {
			return getDefaultPricesList(defaultPrice);
		}

		return getCollateralPricesList(collateralPrice);
	}

	const getCustomPriceDelBtn = (item: DefaultEditableField) => {

		const foundInSaved = defaultPriceSaved.find((iitem) => { return item.id === iitem.id });
		const foundInEdit = defaultPriceToEdit.find((iitem) => { return item.id === iitem.id });
		if ( foundInEdit || foundInSaved ) {
			if ( batch.nftPrices && item.idx !== batch.nftPrices.length - 1 ) {
				return (
					<button
						className="btn btn-md btn-gray btn-img"
					>
						<img src={ i_lock } alt="" />
					</button>
				)
			}

			return (
				<button
					className="btn btn-md btn-gray btn-img"
					onClick={() => {
						setDefaultPriceToEdit(defaultPriceToEdit.filter((iitem) => { return iitem.id !== item.id }));
						setDefaultPriceSaved(defaultPriceSaved.filter((iitem) => { return iitem.id !== item.id }));
						setDefaultPriceToDel([
							...defaultPriceToDel,
							item
						]);
					}}
				>
					<img src={ i_del } alt="" />
				</button>
			)
		}

		return (
			<button
				className="btn btn-md btn-gray btn-img"
				onClick={() => {
					setDefaultPriceToAdd(defaultPriceToAdd.filter((iitem) => { return item.id !== iitem.id }));
				}}
			>
				<img src={ i_del } alt="" />
			</button>
		)
	}
	const getCustomPriceRow = (item: DefaultEditableField) => {

		if ( item.isEdit ) { return getCustomPriceRowEditMode(item) }

		let foundToken: ERC20Type | undefined = erc20List.find((iitem: ERC20Type) => { return iitem.contractAddress.toLowerCase() === item.currentValue.token.toLowerCase() });
		if ( !foundToken ) {
			foundToken = {
				assetType: _AssetType.ERC20,
				contractAddress: item.currentValue.token,
				decimals: 18,
				balance: new BigNumber(0),
				icon: default_coin,
				name: compactString(item.currentValue.token),
				symbol: compactString(item.currentValue.token),
				allowance: [],
			}
		}

		return (
			<div className="item item-narrow" key={item.id}>
				<div className="row">
					<div className="col-auto col-md-2 mb-2">
						<div className="tb-coin">
							<span className="i-coin">
								<img src={ foundToken.icon } alt="" />
							</span>
							<span className="name">{ foundToken.symbol }</span>
						</div>
					</div>
					<div className="col mb-2 d-flex align-items-center"><span className="text-break">{ item.currentValue.amount }</span></div>
					<div className="col-sm-3 mb-2 d-flex">
						<div className="btns-group mr-0 ml-auto">
							<div className="btn-group">
								<button
									className="btn btn-md btn-gray btn-img"
									onClick={() => {
										const foundInSaved = defaultPriceSaved.find((iitem) => { return item.id === iitem.id });
										if ( foundInSaved ) {
											setDefaultPriceSaved([
												...defaultPriceSaved.filter((iitem) => { return item.id !== iitem.id }),
												{
													...item,
													isEdit: true,
													editedValue: item.currentValue,
												}
											])
											return;
										}
										const foundInEdit = defaultPriceToEdit.find((iitem) => { return item.id === iitem.id });
										if ( foundInEdit ) {
											setDefaultPriceToEdit([
												...defaultPriceToEdit.filter((iitem) => { return item.id !== iitem.id }),
												{
													...item,
													isEdit: true,
													editedValue: item.currentValue,
												}
											])
											return;
										}
										setDefaultPriceToAdd([
											...defaultPriceToAdd.filter((iitem) => { return item.id !== iitem.id }),
											{
												...item,
												isEdit: true,
												editedValue: item.currentValue,
											}
										])
									}}
								>
									<img src={ i_edit } alt="" />
								</button>
							</div>
							<div className="btn-group">
								{ getCustomPriceDelBtn(item) }
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
	const getCustomPriceRowEditMode = (item: DefaultEditableField) => {
		let foundToken: ERC20Type | undefined = erc20List.find((iitem: ERC20Type) => { return iitem.contractAddress.toLowerCase() === item.currentValue.token.toLowerCase() });
		if ( !foundToken ) {
			foundToken = {
				assetType: _AssetType.ERC20,
				contractAddress: item.currentValue.token,
				decimals: 18,
				balance: new BigNumber(0),
				icon: default_coin,
				name: compactString(item.currentValue.token),
				symbol: compactString(item.currentValue.token),
				allowance: [],
			}
		}

		const undoEdit = () => {
			const foundInSaved = defaultPriceSaved.find((iitem) => { return item.id === iitem.id });
			if ( foundInSaved ) {
				setDefaultPriceSaved([
					...defaultPriceSaved.filter((iitem) => { return item.id !== iitem.id }),
					{
						...foundInSaved,
						isEdit: false,
						editedValue: undefined,
					}
				])
				return;
			}
			const foundInEdited = defaultPriceToEdit.find((iitem) => { return item.id === iitem.id });
			if ( foundInEdited ) {
				setDefaultPriceToEdit([
					...defaultPriceToEdit.filter((iitem) => { return item.id !== iitem.id }),
					{
						...foundInEdited,
						isEdit: false,
						editedValue: undefined,
					}
				])
				return;
			}
			setDefaultPriceToAdd([
				...defaultPriceToAdd.filter((iitem) => { return item.id !== iitem.id }),
				{
					...item,
					isEdit: false,
					editedValue: undefined,
				}
			])
		}
		const saveEdit = () => {
			const foundInSaved = defaultPriceSaved.find((iitem) => { return item.id === iitem.id });
			if ( foundInSaved ) {
				setDefaultPriceSaved(defaultPriceSaved.filter((iitem) => { return item.id !== iitem.id }));
				setDefaultPriceToEdit([
					...defaultPriceToEdit.filter((iitem) => { return item.id !== iitem.id }),
					{
						...foundInSaved,
						isEdit: false,
						currentValue: item.editedValue || { amount: '0', token: item.currentValue.token },
					}
				])
				return;
			}
			const foundInEdited = defaultPriceToEdit.find((iitem) => { return item.id === iitem.id });
			if ( foundInEdited ) {
				setDefaultPriceToEdit([
					...defaultPriceToEdit.filter((iitem) => { return item.id !== iitem.id }),
					{
						...foundInEdited,
						isEdit: false,
						currentValue: item.editedValue || { amount: '0', token: item.currentValue.token },
					}
				])
				return;
			}
			setDefaultPriceToAdd([
				...defaultPriceToAdd.filter((iitem) => { return item.id !== iitem.id }),
				{
					...item,
					isEdit: false,
					currentValue: item.editedValue || { amount: '0', token: item.currentValue.token },
				}
			])
		}

		return (
			<div className="item item-narrow" key={ item.id }>
				<div className="row-edit">
					<div className="col-md-8 mb-2">
						<div className="select-group">
							<div className="select-coin">
								<div className="select-coin__value">
									<span className="field-unit">
										<span className="i-coin">
											<img src={ foundToken.icon } alt="" />
										</span>
										{ foundToken.symbol }
									</span>
								</div>
							</div>
							<input
								className="input-control"
								type="text"
								placeholder="0.000"
								value={ item.editedValue?.amount }
								onChange={(e) => {
									let value = removeThousandSeparator(e.target.value);
									if ( value !== '' && !value.endsWith('.') && !value.endsWith('0') ) {
										if ( new BigNumber(value).isNaN() ) { return; }
										value = new BigNumber(value).toString();
									}

									const foundInSaved = defaultPriceSaved.find((iitem) => { return item.id === iitem.id });
									if ( foundInSaved ) {
										setDefaultPriceSaved(defaultPriceSaved.filter((iitem) => { return item.id !== iitem.id }));
										setDefaultPriceToEdit([
											...defaultPriceToEdit.filter((iitem) => { return item.id !== iitem.id }),
											{
												...foundInSaved,
												editedValue: {
													token: item.currentValue.token,
													amount: value,
												},
											}
										])
										return;
									}
									const foundInEdited = defaultPriceToEdit.find((iitem) => { return item.id === iitem.id });
									if ( foundInEdited ) {
										setDefaultPriceToEdit([
											...defaultPriceToEdit.filter((iitem) => { return item.id !== iitem.id }),
											{
												...foundInEdited,
												editedValue: {
													token: item.currentValue.token,
													amount: value,
												},
											}
										])
										return;
									}
									setDefaultPriceToAdd([
										...defaultPriceToAdd.filter((iitem) => { return item.id !== iitem.id }),
										{
											...item,
											editedValue: {
												token: item.currentValue.token,
												amount: value,
											},
										}
									])
								}}
							/>
						</div>
					</div>
					<div className="col-6 col-md-2 mb-2">
						<button
							className="btn btn-primary w-100"
							disabled={ item.editedValue && new BigNumber(item.editedValue.amount).eq(0) }
							onClick={() => {
								if (
									item.editedValue &&
									item.currentValue.amount.toLowerCase() === item.editedValue.amount.toLowerCase() &&
									item.currentValue.token.toLowerCase() === item.editedValue.token.toLowerCase()
								) {
									// undo
									undoEdit();
									return
								}

								saveEdit();

							}}
						>
							Save
						</button>
					</div>
					<div className="col-6 col-md-2 mb-2">
						<button
							className="btn btn-gray w-100"
							onClick={() => {
								undoEdit();
							}}
						>
							Undo
						</button>
					</div>
				</div>
			</div>
		)
	}
	const getCustomPriceBlock = () => {
		return (
			<>
			<div className="row">
				<div className="col-md-10">
					<div className="input-group">
						<label className="input-label">Choose ERC-20 and set amount</label>
						<div className="select-group">
							<CoinSelector
								tokens={erc20List}
								onChange={(item) => { setInputDefaultPriceToken(item) }}
								selectedToken={ inputDefaultPriceToken }
							/>
							<input
								className="input-control"
								type="text"
								placeholder="Token"
								value={ inputDefaultPriceToken }
								onChange={(e) => {
									const value = e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "");
									if ( Web3.utils.isAddress(value) ) {
										const foundToken = erc20List.find((item: ERC20Type) => { return item.contractAddress.toLowerCase() === value.toLowerCase() });
										if ( !foundToken ) {
											requestERC20Token(value, userAddress);
										}
									}
									setInputDefaultPriceToken(value);
								}}
							/>
						</div>
					</div>
				</div>
				<div className="col-md-10">
					<input
						className="input-control"
						type="text"
						placeholder="Amount"
						value={ addThousandSeparator(inputDefaultPriceAmount) }
						onChange={(e) => {
							let value = removeThousandSeparator(e.target.value);
							if ( value !== '' && !value.endsWith('.') && !value.endsWith('0') ) {
								if ( new BigNumber(value).isNaN() ) { return; }
								value = new BigNumber(value).toString();
							}
							setInputDefaultPriceAmount(value)
						}}
					/>
				</div>
				<div className="col-md-2">
					<div className="input-group">
						<button
							className="btn btn-outline w-100"
							type="submit"
							disabled={
								inputDefaultPriceToken === '' ||
								inputDefaultPriceAmount === '' ||
								new BigNumber(inputDefaultPriceAmount).eq(0)
							}
							onClick={() => {
								setInputDefaultPriceAmount('');
								setInputDefaultPriceToken('');
								const foundInSaved = defaultPriceSaved.find((item) => { return inputDefaultPriceToken.toLowerCase() === item.currentValue.token.toLowerCase() });
								if ( foundInSaved ) {
									setDefaultPriceSaved(defaultPriceSaved.filter((item) => { return inputDefaultPriceToken.toLowerCase() !== item.currentValue.token.toLowerCase() }))
									setDefaultPriceToEdit([
										...defaultPriceToEdit.filter((item) => { return item.currentValue.token.toLowerCase() !== inputDefaultPriceToken.toLowerCase() }),
										{
											id: foundInSaved.id,
											idx: foundInSaved.idx,
											isEdit: false,
											currentValue: {
												token: inputDefaultPriceToken,
												amount: inputDefaultPriceAmount
											}
										}
									]);
									return;
								}
								const foundInToEdit = defaultPriceToEdit.find((iitem) => { return inputDefaultPriceToken.toLowerCase() === iitem.currentValue.token.toLowerCase() });
								if ( foundInToEdit ) {
									setDefaultPriceToEdit([
										...defaultPriceToEdit.filter((item) => { return item.currentValue.token.toLowerCase() !== inputDefaultPriceToken.toLowerCase() }),
										{
											id: foundInToEdit.id,
											idx: foundInToEdit.idx,
											isEdit: false,
											currentValue: {
												token: inputDefaultPriceToken,
												amount: inputDefaultPriceAmount
											}
										}
									]);
									return;
								}
								setDefaultPriceToAdd([
									...defaultPriceToAdd.filter((item) => { return item.currentValue.token.toLowerCase() !== inputDefaultPriceToken.toLowerCase() }),
									{
										id: new Date().getTime(),
										idx: 0,
										isEdit: false,
										currentValue: {
											token: inputDefaultPriceToken,
											amount: inputDefaultPriceAmount
										}
									}
								])
							}}
						>Add</button>
					</div>
				</div>
			</div>
			<div className="c-wrap__table light mb-6">
				{
					[
						...defaultPriceSaved,
						...defaultPriceToEdit
					]
					.sort((item, prev) => { return item.idx - prev.idx })
					.map((item) => { return getCustomPriceRow(item) })
				}
				{/* { defaultPriceSaved.map((item) => { return getCustomPriceRow(item) }) }
				{ defaultPriceToEdit.map((item) => { return getCustomPriceRow(item) }) } */}
				{ defaultPriceToAdd.map((item) => { return getCustomPriceRow(item) }) }
			</div>
			</>
		)
	}

	const getSpecialPrices = () => {
		return getCustomPriceBlock();
	}

	const isSaveButtonDisabled = () => {
		if (
			inputPriceType === 'special' &&
			!defaultPriceToAdd.length &&
			!defaultPriceToEdit.length &&
			!defaultPriceToDel.length
		) { return true; }

		return false;
	}
	const savePrices = () => {

		if ( inputPriceType === 'default' ) {
			closePopup({
				...batch,
				nftPrices: undefined,
				edit: undefined,
			});
			return;
		}

		if ( batch.source === 'saved' ) {
			const delActions = defaultPriceToDel.map((item) => {
				let foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === item.currentValue.token.toLowerCase() });
				if ( !foundToken ) {
					foundToken = {
						assetType: _AssetType.ERC20,
						contractAddress: item.currentValue.token,
						decimals: 18,
						balance: new BigNumber(0),
						icon: default_coin,
						name: compactString(item.currentValue.token),
						symbol: compactString(item.currentValue.token),
						allowance: [],
					}
				}

				return {
					action: 'del',
					nftPricesUpdated: [{
						payWith: item.currentValue.token,
						amount: tokenToInt(new BigNumber(item.currentValue.amount), foundToken.decimals),
						idx: item.idx,
					}]
				}
			});
			const editActions = defaultPriceToEdit.map((item) => {
				let foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === item.currentValue.token.toLowerCase() });
				if ( !foundToken ) {
					foundToken = {
						assetType: _AssetType.ERC20,
						contractAddress: item.currentValue.token,
						decimals: 18,
						balance: new BigNumber(0),
						icon: default_coin,
						name: compactString(item.currentValue.token),
						symbol: compactString(item.currentValue.token),
						allowance: [],
					}
				}

				return {
					action: 'edit',
					nftPricesUpdated: [{
						payWith: item.currentValue.token,
						amount: tokenToInt(new BigNumber(item.currentValue.amount), foundToken.decimals),
						idx: item.idx,
					}]
				}
			});
			const addActions = defaultPriceToAdd.map((item) => {
				let foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === item.currentValue.token.toLowerCase() });
				if ( !foundToken ) {
					foundToken = {
						assetType: _AssetType.ERC20,
						contractAddress: item.currentValue.token,
						decimals: 18,
						balance: new BigNumber(0),
						icon: default_coin,
						name: compactString(item.currentValue.token),
						symbol: compactString(item.currentValue.token),
						allowance: [],
					}
				}

				return {
					action: 'add',
					nftPricesUpdated: [{
						payWith: item.currentValue.token,
						amount: tokenToInt(new BigNumber(item.currentValue.amount), foundToken.decimals),
						idx: item.idx,
					}]
				}
			});
			closePopup({
				...batch,
				edit: [
					...delActions,
					...editActions,
					...addActions,
				]
			})
			return;
		}


		closePopup({
			...batch,
			nftPrices: defaultPriceToAdd.map((item) => {
				let foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === item.currentValue.token.toLowerCase() });
				if ( !foundToken ) {
					foundToken = {
						assetType: _AssetType.ERC20,
						contractAddress: item.currentValue.token,
						decimals: 18,
						balance: new BigNumber(0),
						icon: default_coin,
						name: compactString(item.currentValue.token),
						symbol: compactString(item.currentValue.token),
						allowance: [],
					}
				}
				return {
					payWith: item.currentValue.token,
					amount: tokenToInt(new BigNumber(item.currentValue.amount), foundToken.decimals),
					idx: 0,
				}
			}),
		})
	}
	return (
		<div className="modal">
		<div className="modal__inner">
		<div className="modal__bg"></div>
		<div className="container">
			<div className="modal__content">
				<div
					className="modal__close"
					onClick={() => { closePopup() }}
				>
					<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
						<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
					</svg>
				</div>
				<div className="c-add">
					<div className="c-add__text mb-0">
						<div className="h2 mb-4">Set price for { batch.tokens.length } { batch.batchType === 'nft' ? 'NFTs' : 'wNFTs' }</div>
						<p className="mb-4">Please set sell prices for the selected NFT/wNFT batch. You can set up common prices for all NFTs/wNFTs in the "Pricing info" section. Select "Special prices", and set the custom price for every NFT/wNFT. You can set prices in ERC20 tokens or native coins.</p>

						{ getPageSelector() }

						{ inputPriceType === 'default' ? getDefaultPrices() : getSpecialPrices() }

					</div>
				</div>
				<div className="row justify-content-sm-end">
					<div className="col-auto">
						<button
							className="btn btn-primary"
							disabled={isSaveButtonDisabled()}
							onClick={ savePrices }
						>Save price</button>
					</div>
				</div>
			</div>
		</div>
		</div>
		</div>
	)
}