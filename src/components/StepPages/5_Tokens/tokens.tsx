
import {
	Link,
	useNavigate
} from "react-router-dom";
import {
	useContext,
	useEffect,
	useState
} from "react";
import { CopyToClipboard } from 'react-copy-to-clipboard';

import {
	NFT,
	NFTorWNFT,
	Price,
	WNFT,
	_AssetType,
	_Display,
	checkApprovalERC1155,
	compactString,
	connect,
	getChainId,
	getUserAddress,
	getUserNFTsFromAPI,
	getUserWNFTsFromAPI,
	setApprovalERC1155,
	setApprovalForAllERC721,
	checkApprovalForAllERC721,
	Display,
	DenominatedPrice,
	BigNumber,
	fetchTokenJSON,
	getNFTById,
	castToWNFT,
	_AssetItem,
	_Price,
	decodePrice,
	LockType,
	fetchTokenMetadata,
} from "@envelop/envelop-client-core";

import {
	addTokenPrice,
	addTokensBatchToDisplay,
	editTokenPrice,
	getDisplayCollateralUnitPricesChain,
	getDisplayDefaultPricesChain,
	removeTokenPrice
} from "../../../models";

import {
	AdvancedLoaderStageType,
	ERC20Context,
	InfoModalContext,
	Web3Context,
	_AdvancedLoadingStatus,
	_ModalTypes
} from "../../../dispatchers";
import {
	getAssetItemsOfDisplayAPI,
	getAssetItemsPriceOfDisplayChain,
	removeTokenFromDisplay
} from "../../../models/kioskadmin";

import AddTokensModal  from "./addtokensmodal";
import StepsProgress   from "../../StepsProgress";
import TokenPriceModal from "./tokenpricemodal";

import default_nft  from '@envelop/envelop-client-core/static/pics/_default_nft.svg';
import default_coin from "@envelop/envelop-client-core/static/pics/coins/_default.svg";
import i_edit       from "../../../static/pics/icons/i-edit.svg";
import i_copy       from "../../../static/pics/icons/i-copy.svg";
import i_del        from "../../../static/pics/icons/i-del.svg";
import i_nft        from "../../../static/pics/icons/i-nft.svg";
import i_wnft       from "../../../static/pics/icons/i-wnft.svg";
import icon_loading from '../../../static/pics/loading.svg';

export type AddedBatch = {
	id: number,
	tokens: Array<NFTorWNFT>,
	batchType: string,
	nftPrices?: Array<Price>,
	source: string,
	edit?: Array<{
		action: string,
		nftPricesUpdated?: Array<Price>,
	}>,
}

type TokensStepPageProps = {
	displayToEdit: Display | undefined,
	onUpdate     : () => void,
}

export default function TokensStepPage(props: TokensStepPageProps) {

	const { displayToEdit } = props;
	const navigate          = useNavigate();

	const {
		setModal,
		unsetModal,
		setLoading,
		createAdvancedLoader,
		updateStepAdvancedLoader,
	} = useContext(InfoModalContext);
	const {
		web3,
		userAddress,
		currentChain,
		currentChainId,
	} = useContext(Web3Context);
	const {
		erc20List
	} = useContext(ERC20Context);

	const [ showTokensModal    , setShowTokensModal     ] = useState<'' | 'nft' | 'wnft'>('');
	const [ showTokenPriceModal, setShowTokenPriceModal ] = useState<AddedBatch | undefined>(undefined);

	const [ defaultPriceSaved   , setDefaultPriceSaved    ] = useState<Array<Price>>([]);
	const [ collateralPriceSaved, setCollateralPriceSaved ] = useState<Array<{ collateralToken: string, price: Array<DenominatedPrice> }>>([]);

	const [ userWNFTs, setUserWNFTs ] = useState<Array<WNFT>>([]);
	const [ userNFTs , setUserNFTs  ] = useState<Array<NFT>>([]);

	const [ tokensLoading, setTokensLoading ] = useState(false);
	const [ displayItemsLoading, setDisplayItemsLoading ] = useState(false);

	const [ savedTokens, setSavedTokens ] = useState<Array<AddedBatch>>([]);
	const [ selectedBatches, setSelectedBatches   ] = useState<Array<AddedBatch>>([]);
	const [ tokensToRemove, setTokensToRemove ] = useState<Array<NFTorWNFT>>([]);

	const [ displayLinkCopied , setDisplayLinkCopied ] = useState(false);

	const [ hasChanges , setHasChanges ] = useState(false);

	//#region TOKENS LOADING
	const fetchTokensWNFT721 = async (page: number) => {
		if ( !currentChain ) { return; }
		if ( !web3 ) { return; }
		if ( !userAddress ) { return; }

		getUserWNFTsFromAPI(currentChain.chainId, _AssetType.ERC721, userAddress, page)
			.then((data) => {
				const tokensOnPage = 12;

				const dataParsed = data
					.filter((item) => {

						if ( item.assetType === _AssetType.wNFTv0 ) { return false; }

						if ( item.rules.noUnwrap   ) { return false; }
						if ( item.rules.noTransfer ) { return false; }

						if ( item.fees.find( (iitem) => { return iitem.value.gt(0) }) ) { return false; }

						// if ( item.locks && item.locks.length ) {
						// 	const foundTimelock = item.locks.find((iitem) => { return iitem.lockType === LockType.time });
						// 	if ( !foundTimelock ) { return true; }
						// 	const now = new BigNumber(new Date().getTime()).dividedToIntegerBy(1000);
						// 	if ( foundTimelock.param.gt(now) ) { return false }
						// }

						return true;
					})
					//.map((iitem) => { return { ...iitem, image: 'service:pending' } })

				setUserWNFTs((prevState) => {
					return [
						...prevState.filter((item) => {
							return !data.find((iitem) => { return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() && item.tokenId.toLowerCase() === iitem.tokenId.toLowerCase() })
						}),
						...dataParsed
					]
				});
				setUserNFTs((prevState) => {
					return prevState.filter((item) => {
						return !data.find((iitem) => { return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() && item.tokenId.toLowerCase() === iitem.tokenId.toLowerCase() })
					})
				});

				if ( data.length === tokensOnPage ) {
					fetchTokensWNFT721(page + 1);
				} else {
					// fetchTokensWNFT1155(1);
					setTokensLoading(false);
				}
			})
			.catch((e) => { console.log('Cannot fetch user wNFTs', e); })
	}
	const fetchTokensWNFT1155 = async (page: number) => {
		if ( !currentChain ) { return; }
		if ( !userAddress ) { return; }

		getUserWNFTsFromAPI(currentChain.chainId, _AssetType.ERC1155, userAddress, page)
			.then((data) => {
				const tokensOnPage = 12;

				const dataParsed = data
					.filter((item) => {

						if ( item.assetType === _AssetType.wNFTv0 ) { return false; }

						if ( item.rules.noUnwrap   ) { return false; }
						if ( item.rules.noTransfer ) { return false; }

						if ( item.fees.find( (iitem) => { return iitem.value.gt(0) }) ) { return false; }

						// if ( item.locks && item.locks.length ) {
						// 	const foundTimelock = item.locks.find((iitem) => { return iitem.lockType === LockType.time });
						// 	if ( !foundTimelock ) { return true; }
						// 	const now = new BigNumber(new Date().getTime()).dividedToIntegerBy(1000);
						// 	if ( foundTimelock.param.gt(now) ) { return false }
						// }

						return true;
					})
					//.map((iitem) => { return { ...iitem, image: 'service:pending' } })

				setUserWNFTs((prevState) => {
					return [
						...prevState.filter((item) => {
							return !data.find((iitem) => { return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() && item.tokenId.toLowerCase() === iitem.tokenId.toLowerCase() })
						}),
						...dataParsed
					]
				});
				setUserNFTs((prevState) => {
					return prevState.filter((item) => {
						return !data.find((iitem) => { return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() && item.tokenId.toLowerCase() === iitem.tokenId.toLowerCase() })
					})
				})

				if ( data.length === tokensOnPage ) {
					fetchTokensWNFT1155(page + 1);
				} else {
					setTokensLoading(false);
				}
			})
			.catch((e) => { console.log('Cannot fetch user wNFTs', e); })
	}
	const fetchTokensNFT721 = async (page: number) => {
		if ( !currentChain || currentChain.chainId === 0 ) { return; }
		const web3 = await connect();
		if ( !web3 ) { return; }
		const userAddress = await getUserAddress(web3);
		if ( !userAddress || userAddress === '0x0000000000000000000000000000000000000000' ) { return; }

		getUserNFTsFromAPI(currentChain.chainId, _AssetType.ERC721, userAddress, page, { withJSON: true })
			.then((data) => {
				const tokensOnPage = 12;

				setUserNFTs((prevState) => {
					return [
						...prevState,
						...data
					]
				});

				if ( data.length === tokensOnPage ) {
					fetchTokensNFT721(page + 1)
				} else {
					fetchTokensWNFT721(1);
				}
			})
			.catch((e) => { console.log('Cannot fetch user NFTs', e); })
	}
	const fetchTokensNFT1155 = async (page: number) => {
		if ( !currentChain || currentChain.chainId === 0 ) { return; }
		const web3 = await connect();
		if ( !web3 ) { return; }
		const userAddress = await getUserAddress(web3);
		if ( !userAddress || userAddress === '0x0000000000000000000000000000000000000000' ) { return; }

		getUserNFTsFromAPI(currentChain.chainId, _AssetType.ERC1155, userAddress, page, { withJSON: true })
			.then((data) => {
				const tokensOnPage = 12;

				setUserNFTs((prevState) => {
					return [
						...prevState,
						...data
					]
				});

				if ( data.length === tokensOnPage ) {
					fetchTokensNFT1155(page + 1)
				} else {
					fetchTokensNFT721(1);
				}
			})
			.catch((e) => { console.log('Cannot fetch user NFTs', e); })
	}

	const fetchUserTokens = () => {
		if ( tokensLoading ) { return; }

		setTokensLoading(true);
		setUserNFTs([]);
		setUserWNFTs([]);
		fetchTokensNFT721(1);
	}
	// #endregion TOKENS LOADING

	useEffect(() => {
		setUserNFTs([]);
		setUserWNFTs([]);
		setTokensLoading(false);
	}, [ currentChain, userAddress, web3 ])

	useEffect(() => {
		const getPrices = async () => {
			if ( displayToEdit ) {
				const prices = await getDisplayDefaultPricesChain(currentChainId, displayToEdit.priceModel, displayToEdit.nameHash);
				setDefaultPriceSaved(
					prices.filter((item) => {
						if ( item.amount.eq(0) ) { return false; }

						return true;
					})
				);

				const collPrices = await getDisplayCollateralUnitPricesChain(currentChainId, displayToEdit.priceModel, displayToEdit.nameHash, erc20List.map((item) => { return item.contractAddress }));
				setCollateralPriceSaved(
					collPrices.filter((item) => {
						if (
							item.price.length &&
							item.price[0].amount.eq(0) &&
							item.price[0].denominator.eq(0) &&
							item.price[0].payWith === '0x0000000000000000000000000000000000000000'
						) { return false; }

						return true;
					})
				)
			}
		}

		getPrices();
	}, [ displayToEdit ])

	const fetchDisplayItemsImages = () => {
		const foundNoImage = savedTokens.find((item) => { return item.tokens[0].image === 'service:pending' });
		if ( !foundNoImage ) { return; }
		if ( !foundNoImage.tokens[0].tokenUrl ) { return; }

		fetchTokenMetadata(foundNoImage.tokens[0])
			.then((data) => {
				setSavedTokens((prevState) => {
					if ( !data ) { return prevState; }

					return [
						...prevState.filter((item) => { return item.id !== foundNoImage.id }),
						{
							...foundNoImage,
							tokens: [{
								...foundNoImage.tokens[0],
								...data
							}]
						}
					]
				});
			})
			.catch((e: any) => {
				setSavedTokens((prevState) => {
					return [
						...prevState.filter((item) => { return item.id !== foundNoImage.id }),
						{
							...foundNoImage,
							tokens: [{
								...foundNoImage.tokens[0],
								image: 'service:cannotload'
							}]
						}
					]
				});
			})
	}
	useEffect(() => { if ( !displayItemsLoading ) { fetchDisplayItemsImages(); } }, [ savedTokens, tokensLoading ])
	useEffect(() => {
		const getTokens = async (page: number, prices: Array<{ nft: _AssetItem, owner: string, prices: Array<_Price> }>) => {
			if ( !displayToEdit ) { return; }
			const savedItems = await getAssetItemsOfDisplayAPI(currentChainId, displayToEdit.contractAddress, displayToEdit.nameHash, page, 10);
			setSavedTokens((prevState) => {
				return [
					...prevState,
					...savedItems.map((item, idx) => {

						const foundPrice = prices.find((iitem: { nft: _AssetItem, owner: string, prices: Array<_Price> }) => {
							return iitem.nft.asset.contractAddress.toLowerCase() === item.token.contractAddress.toLowerCase() &&
								`${iitem.nft.tokenId}` === `${item.token.tokenId}` &&
								iitem.prices && iitem.prices.length
						});

						return {
							id: new Date().getTime() + idx,
							batchType: item.tokenType,
							source: 'saved',
							tokens: [item.token],
							nftPrices: foundPrice ? foundPrice.prices.map((item, idx) => { return decodePrice(item, idx) }) : [],
						}
					})
				]
			});

			if ( savedItems.length === 10 ) {
				getTokens(page + 1, prices)
			}

			setDisplayItemsLoading(false);
		}

		if ( !displayToEdit ) { return; }
		if ( displayItemsLoading ) { return; }
		setDisplayItemsLoading(true);
		setSavedTokens([]);
		getAssetItemsPriceOfDisplayChain(currentChainId, displayToEdit.contractAddress, displayToEdit.nameHash)
			.then((data: Array<{ nft: _AssetItem, owner: string, prices: Array<_Price> }>) => {
				getTokens(1, data);
			})
	}, [ displayToEdit ])

	const getPriceBtnLabel = (batch: AddedBatch) => {
		if ( ( batch.nftPrices === undefined || !batch.nftPrices.length ) && !batch.edit ) {
			return (
				<><span className="mr-3">Default price</span><img src={ i_edit } alt="" /></>
			)
		} else {
			return (
				<><span className="text-green mr-3">Special price</span><img src={ i_edit } alt="" /></>
			)
		}
	}
	const getBatchSaved = (batch: AddedBatch) => {

		if ( batch.edit ) {
			const toRemove = batch.edit.find((item) => { return item.action === 'removeToken' });
			if ( toRemove ) { return null; }
		}

		return (
			batch.tokens
				.sort((item, prev) => {
					if ( item.contractAddress.toLowerCase() < prev.contractAddress.toLowerCase() ) { return -1 }
					if ( item.contractAddress.toLowerCase() > prev.contractAddress.toLowerCase() ) { return  1 }

					if ( prev.contractAddress.toLowerCase() === item.contractAddress.toLowerCase() ) {
						if ( parseInt(item.tokenId) < parseInt(prev.tokenId) ) { return  1 }
						if ( parseInt(item.tokenId) > parseInt(prev.tokenId) ) { return -1 }
					}

					return 0;
				})
				.map((token) => {
					return (
						<div className="item item-narrow" key={ `${token.contractAddress}${token.tokenId}` }>
							<div className="row">
								<div className="mb-2 col-6 col-md-auto col-lg-2">
									<div className="tb-nft">
										{
											token.image === 'service:pending' || token.image === 'service:loading' ? (
												<img src={ icon_loading } alt="" />
											) :
												!token.image || token.image === 'service:cannotload' ? (
													<img src={ default_nft } alt="" />
												) : (
													token.thumbnail ? (
														<img src={ token.thumbnail } alt="" />
													) : (
														<img src={ token.image } alt="" />
													)
												)
										}
									</div>
								</div>
								<div className="mb-2 col-md-2">
									<span className="col-legend">Contract: </span>
									<span>{ compactString(token.contractAddress) }</span>
								</div>
								<div className="mb-2 col-md">
									<span className="text-break">ID { compactString(token.tokenId) }</span>
								</div>
								<div className="mb-2 col-md">
									<div className="tb-nft-price justify-content-md-end">
										{/* ~0.002 DAI */}
										{ token.collateral && token.collateral.length ? (
											<>
												<span className="ml-2 mr-2">{ token.collateral.length } collateral items</span>
												{/* <img src={ token.collateral[0].tokenImg || default_coin } alt="" /> */}
											</>
										) : null }
									</div>
								</div>
								<div className="mb-2 col-12 col-sm-auto mt-">
									<button
										className="btn btn-md btn-gray w-100"
										onClick={() => { setShowTokenPriceModal(batch); }}
									>
										{ getPriceBtnLabel(batch) }
									</button>
								</div>

								<div className="col col-md-2 col-lg-1 mb-2 d-flex order-2 order-md-5">
									<div className="btns-group mr-0 ml-auto">
										<div className="btn-group">
											<button
												className="btn btn-md btn-gray btn-img"
												onClick={() => {
													setSavedTokens([
														...savedTokens.filter((item) => { return item.id !== batch.id }),
														{
															...batch,
															edit: [{
																action: 'removeToken'
															}]
														}
													])
												}}
											>
												<img src={ i_del } alt="" />
											</button>
										</div>
									</div>
								</div>

							</div>
						</div>
					)
				})
		)
	}
	const getBatchAdded = (batch: AddedBatch) => {
		return (
			<div className="c-wrap__table mb-6" key={ batch.id }>
				<div className="item item-narrow lp-bach-header">
					<div className="row">
						<div className="col-12 col-sm mb-2"><b>{ batch.tokens.length }</b> { batch.tokens.length === 1 ? 'item' : 'items' } <b className="ml-2"> {batch.batchType === 'nft' ? 'NFT' : 'wNFT' }</b></div>
						<div className="col-12 col-sm-auto mb-2">
							<button
								className="btn btn-md btn-gray w-100"
								onClick={() => {
									setShowTokenPriceModal(batch);
								}}
							>
								{ getPriceBtnLabel(batch) }
							</button>
						</div>
					</div>
				</div>
				{
					batch.tokens
					.sort((item, prev) => {
						if ( item.contractAddress.toLowerCase() < prev.contractAddress.toLowerCase() ) { return -1 }
						if ( item.contractAddress.toLowerCase() > prev.contractAddress.toLowerCase() ) { return  1 }

						if ( prev.contractAddress.toLowerCase() === item.contractAddress.toLowerCase() ) {
							if ( parseInt(item.tokenId) < parseInt(prev.tokenId) ) { return  1 }
							if ( parseInt(item.tokenId) > parseInt(prev.tokenId) ) { return -1 }
						}

						return 0;
					})
					.map((token) => {
						return (
							<div className="item item-narrow" key={ `${token.contractAddress}${token.tokenId}` }>
								<div className="row">
									<div className="col-6 col-md-2 mb-2 order-1">
										<div className="tb-nft">
											{
												token.image === 'service:pending' || token.image === 'service:loading' ? (
													<img src={ icon_loading } alt="" />
												) :
													!token.image || token.image === 'service:cannotload' ? (
														<img src={ default_nft } alt="" />
													) : (
														token.thumbnail ? (
															<img src={ token.thumbnail } alt="" />
														) : (
															<img src={ token.image } alt="" />
														)
													)
											}
										</div>
									</div>
									<div className="col-md-2 mb-2 order-3 order-md-2">
										<span className="col-legend">Contract: </span>
										<span>{ compactString(token.contractAddress) }</span>
									</div>
									<div className="mb-2 col-md order-4 order-md-3">
										<span className="text-break">ID { compactString(token.tokenId) }</span>
									</div>
									<div className="mb-2 col-md order-5 order-md-4">
										<div className="tb-nft-price justify-content-md-end">
											{/* ~0.002 DAI */}
											{ token.collateral && token.collateral.length ? (
												<>
													<span className="ml-2 mr-2">{ token.collateral.length } collateral items</span>
													{/* <img src={ token.collateral[0].tokenImg || default_coin } alt="" /> */}
												</>
											) : null }
										</div>
									</div>
									<div className="col col-md-2 col-lg-1 mb-2 d-flex order-2 order-md-5">
										<div className="btns-group mr-0 ml-auto">
											<div className="btn-group">
												<button
													className="btn btn-md btn-gray btn-img"
													onClick={() => {
														const existedBatch = selectedBatches.find((item) => { return item.id === batch.id });
														if ( !existedBatch ) { return; }

														const listUpdated = [
															...selectedBatches.filter((item) => { return item.id !== batch.id }),
															{
																...existedBatch,
																tokens: existedBatch.tokens.filter((item) => {
																	return item.contractAddress.toLowerCase() !== token.contractAddress.toLowerCase() || item.tokenId.toLowerCase() !== token.tokenId.toLowerCase()
																})
															}
														].filter((item) => { return !!item.tokens.length });

														setSelectedBatches(listUpdated);
													}}
												>
													<img src={ i_del } alt="" />
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						)
					})
				}
			</div>
		)
	}

	const updateSubmit = async () => {

		if ( !displayToEdit ) { return; }
		const web3 = await connect();
		if ( !web3 ) { return; }
		const chain = await getChainId(web3);
		if ( !chain ) { return; }
		const user = await getUserAddress(web3);
		if ( !user ) { return; }

		const loaderStages: Array<AdvancedLoaderStageType> = [];

		let tokensToApprove1155: Array<string> = [];
		let tokensToApprove721: Array<string> = [];

		const removeTokenExisted: Array<NFTorWNFT> = [];
		savedTokens.forEach((item) => {
			if ( !item.edit ) { return; }
			item.edit.forEach((iitem) => {
				if ( iitem.action === 'removeToken' ) {
					removeTokenExisted.push(item.tokens[0])
				}
			})
		});
		if ( removeTokenExisted.length ) {
			loaderStages.push({
				id: 'removeTokenExisted',
				sortOrder: 1,
				text: `Remove token`,
				total: removeTokenExisted.length,
				current: 0,
				status: _AdvancedLoadingStatus.queued
			});
		}

		const delExisted: Array<NFTorWNFT> = [];
		savedTokens.forEach((item) => {
			if ( !item.edit ) { return; }
			item.edit.forEach((iitem) => {
				if ( iitem.action === 'del' ) {
					delExisted.push(item.tokens[0])
				}
			})
		});
		if ( delExisted.length ) {
			loaderStages.push({
				id: 'delExisted',
				sortOrder: 1,
				text: `Editing personal prices`,
				total: delExisted.length,
				current: 0,
				status: _AdvancedLoadingStatus.queued
			});
		}

		const editExisted: Array<{ token: NFTorWNFT, price: Price }> = [];
		savedTokens.forEach((item) => {
			if ( !item.edit ) { return; }
			item.edit.forEach((iitem) => {
				if ( iitem.action !== 'edit' ) { return; }
				if ( !iitem.nftPricesUpdated ) { return; }
				iitem.nftPricesUpdated.forEach((iiitem) => {
					editExisted.push({
						token: item.tokens[0],
						price: iiitem
					});
				})
			})
		});
		if ( editExisted.length ) {
			loaderStages.push({
				id: 'editExisted',
				sortOrder: 2,
				text: `Editing personal prices`,
				total: editExisted.length,
				current: 0,
				status: _AdvancedLoadingStatus.queued
			});
		}

		const addExisted: Array<{ token: NFTorWNFT, prices: Array<Price> }> = [];
		savedTokens.forEach((item) => {
			if ( !item.edit ) { return; }
			item.edit.forEach((iitem) => {
				if ( iitem.action !== 'add' ) { return; }
				if ( !iitem.nftPricesUpdated ) { return; }
				addExisted.push({
					token: item.tokens[0],
					prices: iitem.nftPricesUpdated
				});
			})
		});
		if ( addExisted.length ) {
			loaderStages.push({
				id: 'addExisted',
				sortOrder: 3,
				text: `Adding prices`,
				total: addExisted.length,
				current: 0,
				status: _AdvancedLoadingStatus.queued
			});
		}

		if ( selectedBatches.length ) {
			const tokensToApprove = selectedBatches.flatMap((item) => { return item.tokens });
			tokensToApprove1155 = tokensToApprove
				.filter((item) => { return item.assetType === _AssetType.ERC1155 })
				.reduce((acc: Array<string>, item: NFTorWNFT) => {
					const existedEl = acc.find((iitem) => { return item.contractAddress.toLowerCase() === iitem.toLowerCase() });
					if ( existedEl ) { return acc; }
					return [ ...acc, item.contractAddress ]
				}, []);

			tokensToApprove721 = tokensToApprove
				.filter((item) => { return item.assetType !== _AssetType.ERC1155 })
				.reduce((acc: Array<string>, item: NFTorWNFT) => {
					const existedEl = acc.find((iitem) => { return item.contractAddress.toLowerCase() === iitem.toLowerCase() });
					if ( existedEl ) { return acc; }
					return [ ...acc, item.contractAddress ]
				}, []);

			if ( tokensToApprove1155.length ) {
				loaderStages.push({
					id: 'approve1155',
					sortOrder: 10,
					text: `Approving ${currentChain?.EIPPrefix || 'ERC'}-1155 tokens`,
					total: tokensToApprove1155.length,
					current: 0,
					status: _AdvancedLoadingStatus.queued
				});
			}
			if ( tokensToApprove721.length ) {
				loaderStages.push({
					id: 'approve721',
					sortOrder: 11,
					text: `Approving ${currentChain?.EIPPrefix || 'ERC'}-721 tokens`,
					total: tokensToApprove721.length,
					current: 0,
					status: _AdvancedLoadingStatus.queued
				});
			}

			loaderStages.push({
				id: 'add',
				sortOrder: 12,
				text: `Adding tokens`,
				total: selectedBatches.length,
				current: 0,
				status: _AdvancedLoadingStatus.queued
			});
		}

		const pageEdited = !!loaderStages.length;
		if ( !pageEdited ) {
			navigate(`/summary/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.nameHash}`);
			return;
		}

		const advLoader = {
			title: 'Waiting to update',
			stages: loaderStages
		};
		createAdvancedLoader(advLoader);

		if ( removeTokenExisted.length ) {
			for (const idx in removeTokenExisted) {
				const item = removeTokenExisted[idx];

				updateStepAdvancedLoader({
					id: 'removeTokenExisted',
					text: `Remove token: ${compactString(item.contractAddress)}:${item.tokenId}`,
					current: parseInt(idx)+1,
					status: _AdvancedLoadingStatus.loading
				});

				try {
					await removeTokenFromDisplay(web3, displayToEdit.contractAddress, item);
				} catch(e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot remove token`,
						details: [
							`User address: ${userAddress}`,
							`Showcase address: ${displayToEdit.contractAddress}`,
							`Showcase hash: ${displayToEdit.nameHash}`,
							`Item: ${item.contractAddress} : ${item.tokenId}`,
							'',
							e.message || e,
						],
						buttons: [{
							text: 'Ok',
							clickFunc: () => { window.location.reload(); }
						}]
					});
					return;
				}

			}
			updateStepAdvancedLoader({
				id: 'removeTokenExisted',
				text: `Removing personal prices`,
				current: delExisted.length,
				status: _AdvancedLoadingStatus.complete
			});
		}

		if ( delExisted.length ) {
			for (const idx in delExisted) {
				const item = delExisted[idx];

				updateStepAdvancedLoader({
					id: 'delExisted',
					text: `Removing personal price of token: ${compactString(item.contractAddress)}:${item.tokenId}`,
					current: parseInt(idx)+1,
					status: _AdvancedLoadingStatus.loading
				});

				try {
					await removeTokenPrice(web3, displayToEdit.contractAddress, item);
				} catch(e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot remove price of token`,
						details: [
							`User address: ${userAddress}`,
							`Showcase address: ${displayToEdit.contractAddress}`,
							`Showcase hash: ${displayToEdit.nameHash}`,
							`Item: ${item.contractAddress} : ${item.tokenId}`,
							'',
							e.message || e,
						],
						buttons: [{
							text: 'Ok',
							clickFunc: () => { window.location.reload(); }
						}]
					});
					return;
				}

			}
			updateStepAdvancedLoader({
				id: 'delExisted',
				text: `Removing personal prices`,
				current: delExisted.length,
				status: _AdvancedLoadingStatus.complete
			});
		}

		if ( editExisted.length ) {
			for (const idx in editExisted) {
				const item = editExisted[idx];

				let foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === item.price.payWith.toLowerCase() });
				if ( !foundToken ) {
					foundToken = {
						assetType: _AssetType.ERC20,
						contractAddress: item.price.payWith,
						decimals: 18,
						balance: new BigNumber(0),
						icon: default_coin,
						name: compactString(item.price.payWith),
						symbol: compactString(item.price.payWith),
						allowance: [],
					}
				}

				updateStepAdvancedLoader({
					id: 'editExisted',
					text: `Editing ${foundToken.symbol} price of ${compactString(item.token.contractAddress)}:${item.token.tokenId}`,
					current: parseInt(idx)+1,
					status: _AdvancedLoadingStatus.loading
				});

				try {
					await editTokenPrice(web3, displayToEdit.contractAddress, item.token, item.price);
				} catch(e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot edit price of token`,
						details: [
							`User address: ${userAddress}`,
							`Showcase address: ${displayToEdit.contractAddress}`,
							`Showcase hash: ${displayToEdit.nameHash}`,
							`Price: ${item.price.idx}: ${foundToken.symbol} (${item.price.payWith})`,
							`Item: ${item.token.contractAddress} : ${item.token.tokenId}`,
							'',
							e.message || e,
						],
						buttons: [{
							text: 'Ok',
							clickFunc: () => { window.location.reload(); }
						}]
					});
					return;
				}

			}
			updateStepAdvancedLoader({
				id: 'editExisted',
				text: `Editing personal prices`,
				current: editExisted.length,
				status: _AdvancedLoadingStatus.complete
			});
		}
		if ( addExisted.length ) {
			for (const idx in addExisted) {
				const item = addExisted[idx];

				updateStepAdvancedLoader({
					id: 'addExisted',
					text: `Adding price to ${compactString(item.token.contractAddress)}:${item.token.tokenId}`,
					current: parseInt(idx)+1,
					status: _AdvancedLoadingStatus.loading
				});

				try {
					await addTokenPrice(web3, displayToEdit.contractAddress, item.token, item.prices);
				} catch(e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot add prices to token`,
						details: [
							`User address: ${userAddress}`,
							`Showcase address: ${displayToEdit.contractAddress}`,
							`Showcase hash: ${displayToEdit.nameHash}`,
							`Prices: ${JSON.stringify(item.prices)}`,
							`Item: ${item.token.contractAddress} : ${item.token.tokenId}`,
							'',
							e.message || e,
						],
						buttons: [{
							text: 'Ok',
							clickFunc: () => { window.location.reload(); }
						}]
					});
					return;
				}

			}
			updateStepAdvancedLoader({
				id: 'addExisted',
				text: `Adding prices`,
				current: addExisted.length,
				status: _AdvancedLoadingStatus.complete
			});
		}

		if ( selectedBatches.length ) {
			// ---------- APPROVE 1155 ----------
			if ( tokensToApprove1155.length ) {
				for ( const idx in tokensToApprove1155 ) {
					const item = tokensToApprove1155[idx];

					updateStepAdvancedLoader({
						id: 'approve1155',
						current: parseInt(idx) + 1,
						status: _AdvancedLoadingStatus.loading
					});

					let isApproved: boolean = await checkApprovalERC1155(chain, item, user, displayToEdit.contractAddress);
					if ( !isApproved ) {
						try {
							await setApprovalERC1155(web3, item, user, displayToEdit.contractAddress);
						} catch(e: any) {
							setModal({
								type: _ModalTypes.error,
								title: `Cannot approve tokens of contract`,
								details: [
									`User address: ${userAddress}`,
									`Showcase address: ${displayToEdit.contractAddress}`,
									`Showcase hash: ${displayToEdit.nameHash}`,
									`Contract: ${item}`,
									'',
									e.message || e,
								],
								buttons: [{
									text: 'Ok',
									clickFunc: () => { window.location.reload(); }
								}]
							});
							return;
						}
					}
				}
				updateStepAdvancedLoader({
					id: 'approve1155',
					current: tokensToApprove1155.length,
					status: _AdvancedLoadingStatus.complete
				});
			}
			// ---------- END APPROVE 1155 ----------
			// ---------- APPROVE 721 ----------
			if ( tokensToApprove721.length ) {
				for ( const idx in tokensToApprove721 ) {
					const item = tokensToApprove721[idx];

					updateStepAdvancedLoader({
						id: 'approve721',
						current: parseInt(idx) + 1,
						status: _AdvancedLoadingStatus.loading
					});

					let isApproved: boolean = await checkApprovalForAllERC721(chain, item, user, displayToEdit.contractAddress);
					if ( !isApproved ) {
						try {
							await setApprovalForAllERC721(web3, item, user, displayToEdit.contractAddress);
						} catch(e: any) {
							setModal({
								type: _ModalTypes.error,
								title: `Cannot approve tokens of contract`,
								details: [
									`User address: ${userAddress}`,
									`Showcase address: ${displayToEdit.contractAddress}`,
									`Showcase hash: ${displayToEdit.nameHash}`,
									`Item: ${item}`,
									'',
									e.message || e,
								],
								buttons: [{
									text: 'Ok',
									clickFunc: () => { window.location.reload(); }
								}]
							});
							return;
						}
					}
				}
				updateStepAdvancedLoader({
					id: 'approve721',
					current: tokensToApprove721.length,
					status: _AdvancedLoadingStatus.complete
				});
			}
			// ---------- END APPROVE 721 ----------

			// ---------- ADD ----------
			for ( const idx in selectedBatches ) {
				const item = selectedBatches[idx];

				updateStepAdvancedLoader({
					id: 'add',
					current: parseInt(idx) + 1,
					status: _AdvancedLoadingStatus.loading
				});

				try {
					await addTokensBatchToDisplay(
						web3,
						displayToEdit.contractAddress,
						displayToEdit.nameHash,
						item.tokens,
						item.nftPrices,
					)
				} catch(e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot add tokens`,
						details: [
							`User address: ${userAddress}`,
							`Showcase address: ${displayToEdit.contractAddress}`,
							`Showcase hash: ${displayToEdit.nameHash}`,
							`Tokens: ${item.tokens.map((item) => { return `${item.contractAddress}:${item.tokenId}` }).join('; ')}`,
							'',
							e.message || e,
						],
						buttons: [{
							text: 'Ok',
							clickFunc: () => { window.location.reload(); }
						}]
					});
					return;
				}
			}

			updateStepAdvancedLoader({
				id: 'add',
				current: selectedBatches.length,
				status: _AdvancedLoadingStatus.complete
			});
			// ---------- END ADD ----------
		}

		setModal({
			type: _ModalTypes.success,
			title: 'Tokens successfully placed',
			text: currentChain?.hasOracle ? [
				{ text: 'After oracle synchronization, your changes will appear in launchpad admin panel soon. Please refresh the launchpads list' },
			] : [],
			buttons: [{
				text: 'Ok',
				clickFunc: () => {
					setSavedTokens([]);
					unsetModal();
					navigate(`/summary/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.nameHash}`);
					props.onUpdate();
				}
			}],
			links: [{
				text: `View showcase`,
				url: `/launchpad/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.name}`
			}]
		});
	}

	const getBackBtn = () => {
		if ( hasChanges ) {
			return (
				<button
					className="btn btn-gray"
					onClick={() => {
						setModal({
							type: _ModalTypes.info,
							title: 'You have unsaved data. Continue?',
							buttons: [
								{
									text: 'Yes',
									clickFunc: () => {
										navigate(`/`);
										unsetModal();
									},
								},
								{
									text: 'No',
									clickFunc: () => { unsetModal(); },
									clazz: 'btn-grad'
								},
							]
						})
					}}
				>Back</button>
			)
		}

		if ( !displayToEdit ) {
			return (
				<Link
					className="btn btn-gray"
					to={ `/` }
				>Back</Link>
			)
		}

		return (
			<Link
				className="btn btn-gray"
				to={ `/discounts/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.nameHash}` }
			>Back</Link>
		)
	}

	return (
		<>
		<div className="container">

			<div className="breadcrumbs">
				<Link className="item-back btn btn-sm btn-gray" to="/">
					<svg className="mr-2" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M12 5.25H2.8725L7.065 1.0575L6 0L0 6L6 12L7.0575 10.9425L2.8725 6.75H12V5.25Z" fill="white"></path>
					</svg>
					<span>To the showcase list</span>
				</Link>
			</div>

			<div className="row mb-4">
				<div className="col-12 col-sm">
					<div className="h3 mb-3 mt-0">{ displayToEdit?.title || '<Untitled>' }</div>
				</div>
				{
					displayToEdit ? (
						<div className="col-12 col-sm-auto">
							<div className="btns-group">
								<div className="btn-group">
									<a
										className="btn btn-md btn-gray"
										href={ `/launchpad/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.name}` }
										target="_blank" rel="noopener noreferrer"
									>Go to the showcase</a>
								</div>
								<div className="btn-group">
								<CopyToClipboard
									text={ `${window.location.origin}/launchpad/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.name}` }
									onCopy={() => {
										setDisplayLinkCopied(true);
										setTimeout(() => { setDisplayLinkCopied(false); }, 5*1000);
									}}
								>
									<button className="btn btn-md btn-gray btn-img">
										<img src= { i_copy } alt="" />
										<span className="btn-action-info" style={{ display: displayLinkCopied ? 'block' : 'none' }}>Copied</span>
									</button>
								</CopyToClipboard>
								</div>
							</div>
						</div>
					) : null
				}
			</div>
			<div className="row">
				<div className="col-md-2 col-lg-2">
					<StepsProgress current="token" display={ displayToEdit } confirmClose={ hasChanges } />
				</div>
				<div className="col-md-10 col-lg-10">
					<div className="lp-wrap">
						<div className="lp-wrap__header">
							<div className="row align-items-center">
								<div className="col-12 col-sm-auto">
									<div className="h3-wrap mr-2">Add your </div>
								</div>
								<div className="col mt-3 mt-sm-0">
									<div className="d-flex">
										<button
											className="btn btn-outline btn-rounded mr-3"
											onClick={() => {
												if ( !userWNFTs.length && !userNFTs.length ) { fetchUserTokens(); }
												setShowTokensModal('nft');
											}}
										>
											<img className="mr-2" src={ i_nft } alt="" />
											<span>NFTs</span>
										</button>
										<button
											className="btn btn-outline btn-rounded"
											onClick={() => {
												if ( !userWNFTs.length && !userNFTs.length ) { fetchUserTokens(); }
												setShowTokensModal('wnft');
											}}
										>
											<img className="mr-2" src={ i_wnft } alt="" />
											<span>wNFTs</span>
										</button>
									</div>
								</div>
							</div>
						</div>
						{
							displayItemsLoading && !savedTokens.length ? (
								<div className="lp-list__footer">
									<img className="loading" src={ icon_loading } alt="" />
								</div>
							) : null
						}
						{
							savedTokens.length ? (
								<>
								<div className="mb-4">You have <b className="text-green">{ savedTokens.length } items</b> in your showcase</div>
								<div className="c-wrap__table mb-6">
									{
										savedTokens
											.sort((item, prev) => { return item.id - prev.id })
											.map((item) => { return getBatchSaved(item) })
									}
								</div>
								</>
							) : null
						}
						{
							selectedBatches
								.sort((item, prev) => { return item.id - prev.id })
								.map((item) => { return getBatchAdded(item) })
						}
						<div className="lp-steps-footer">
							<div className="row justify-content-between">
								<div className="col-auto">
									{ getBackBtn() }
								</div>
								<div className="col-auto">
									<button
										className="btn btn-grad"
										onClick={ updateSubmit }
									>
										Save and proceed
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		{ 	showTokensModal ===  'nft' ? (
				<AddTokensModal
					closePopup={(selected: Array<NFTorWNFT>) => {
						if ( selected.length ) {
							setHasChanges(true);
							setSelectedBatches(
								[
									...selectedBatches,
									{ id: new Date().getTime(), tokens: selected, batchType: 'nft', source: 'added' }
								].filter((item: AddedBatch) => { return item.tokens.length !== 0 })
							);
						}
						setShowTokensModal('');
					}}
					tokens={ userNFTs }
					tokensLoading={ tokensLoading }
					excludeTokens={ selectedBatches.flatMap((item) => { return item.tokens }) }
					tokensType="nft"
				/>
			) : null
		}
		{ 	showTokensModal === 'wnft' ? (
				<AddTokensModal
					closePopup={(selected: Array<NFTorWNFT>) => {
						if ( selected.length ) {
							setHasChanges(true);
							setSelectedBatches(
								[
									...selectedBatches,
									{ id: new Date().getTime(), tokens: selected, batchType: 'wnft', source: 'added' }
								].filter((item: AddedBatch) => { return item.tokens.length !== 0 })
							);
						}
						setShowTokensModal('');
					}}
					tokens={ userWNFTs }
					tokensLoading={ tokensLoading }
					excludeTokens={ selectedBatches.flatMap((item) => { return item.tokens }) }
					tokensType="wnft"
				/>
			) : null
		}
		{ 	showTokenPriceModal !== undefined ? (
				<TokenPriceModal
					closePopup={(batch?: AddedBatch) => {
						setShowTokenPriceModal(undefined);

						if ( batch ) {
							setHasChanges(true);
							if ( batch.source === 'saved' ) {
								setSavedTokens([
									...savedTokens.filter((item) => { return item.id !== batch.id }),
									batch
								]);
								return;
							}

							setSelectedBatches([
								...selectedBatches.filter((item) => { return item.id !== batch.id }),
								batch
							]);
						}
					}}
					batch={ showTokenPriceModal }
					defaultPrice={defaultPriceSaved}
					collateralPrice={collateralPriceSaved}
				/>
			) : null
		}
		</>
	)
}