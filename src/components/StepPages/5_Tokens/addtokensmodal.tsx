
import React, {
	useContext,
	useMemo,
	useState
} from "react";

import {
	BigNumber,
	NFTorWNFT,
	assetTypeToString,
	chainTypeToERC20,
	compactString,
} from "@envelop/envelop-client-core";

import NFTCardCompact from "../../NFTCardCompact";
import {
	ERC20Context, Web3Context
} from "../../../dispatchers";

import icon_loading from '../../../static/pics/loading.svg';
import default_icon from '../../../static/pics/coins/_default.svg';

type AddTokensModalProps = {
	closePopup: (selected: Array<NFTorWNFT>) => void,
	tokens: Array<NFTorWNFT>,
	tokensLoading: boolean,
	excludeTokens: Array<NFTorWNFT>,
	tokensType: 'nft' | 'wnft',
}

export default function AddTokensModal(props: AddTokensModalProps) {

	const {
		tokens,
		tokensLoading,
		excludeTokens,
		tokensType
	} = props;

	const [ filterByString,    setFilterByString    ] = useState<string>('');
	const [ filterByToken,     setFilterByToken     ] = useState<Array<string>>([]);
	const [ filterCoinsOpened, setFilterCoinsOpened ] = useState(false);
	const [ filterSortOpened,  setFilterSortOpened  ] = useState(false);
	const [ sortBy,            setSortBy            ] = useState<'' | 'Address' | 'With image'>('');

	const filterCoinsRef = React.useRef<HTMLInputElement>(null);
	const filterSortRef  = React.useRef<HTMLInputElement>(null);

	const [ selectedTokens,    setSelectedTokens    ] = useState<Array<NFTorWNFT>>([]);

	const [ searchType, setSearchType ] = useState<'search' | 'range'>('search');
	const [ inputRangeAddress, setInputRangeAddress ] = useState('');
	const [ inputRangeTokenIds, setInputRangeTokenIds ] = useState('');
	const [ rangeAllowedTokenIds, setRangeAllowedTokenIds ] = useState<Array<string>>([]);


	const {
		erc20List,
		requestERC20Token,
	} = useContext(ERC20Context);
	const {
		currentChain,
	} = useContext(Web3Context);

	const tokensFiltered = useMemo(() => {
		const filtered = tokens
		.filter((item) => {
			return !excludeTokens.find((iitem) => { return iitem.contractAddress.toLowerCase() === item.contractAddress.toLowerCase() && iitem.tokenId.toLowerCase() === item.tokenId.toLowerCase() })
		})
		.filter((item) => {

			if ( searchType === 'search' ) {

				if ( filterByString !== '' ) {

					if ( item.contractAddress.toLowerCase().includes(filterByString.toLowerCase()) ) { return true; }
					if ( `${item.tokenId}`.toLowerCase().includes(filterByString.toLowerCase()) ) { return true; }

					if ( assetTypeToString(item.assetType, '').toLowerCase().includes(filterByString.toLowerCase())) { return true; }

					return false;
				}

				if ( filterByToken.length ) {

					if ( item.fees && item.fees.length ) {
						const feeFound = filterByToken.find((iitem) => {
							if ( !item.fees || !item.fees.length ) { return false; }
							return iitem.toLowerCase() === item.fees[0].token.toLowerCase()
						});
						if ( feeFound ) { return true; }
					}

					if ( item.collateral && item.collateral.length ) {
						const collateralFound = filterByToken.find((iitem) => {
							if ( !item.collateral || !item.collateral.length ) { return false; }
							return !!item.collateral.find((iiitem) => { return iitem.toLowerCase() === iiitem.contractAddress.toLowerCase() });
						});
						if ( collateralFound ) { return true; }
					}

					return false;
				}
			}

			if ( searchType === 'range' ) {
				if ( inputRangeAddress !== '' && item.contractAddress.toLowerCase() !== inputRangeAddress.toLowerCase() ) { return false; }
				if ( rangeAllowedTokenIds.length && !rangeAllowedTokenIds.includes(item.tokenId) ) { return false; }
			}

			return true;
		})

		let sorted;
		if ( sortBy === 'With image' ) {
			sorted = filtered
				.sort((item, prev) => {
					if ( item.image && !prev.image ) { return -1 }
					if ( prev.image && !item.image ) { return  1 }

					if ( item.contractAddress.toLowerCase() < prev.contractAddress.toLowerCase() ) { return -1 }
					if ( item.contractAddress.toLowerCase() > prev.contractAddress.toLowerCase() ) { return  1 }

					if ( prev.contractAddress.toLowerCase() === item.contractAddress.toLowerCase() ) {
						try {
							if ( new BigNumber(item.tokenId).isNaN() || new BigNumber(prev.tokenId).isNaN() ) {
								if ( parseInt(`${item.tokenId}`) < parseInt(`${prev.tokenId}`) ) { return  1 }
								if ( parseInt(`${item.tokenId}`) > parseInt(`${prev.tokenId}`) ) { return -1 }
							}
							const itemTokenIdNumber = new BigNumber(item.tokenId);
							const prevTokenIdNumber = new BigNumber(prev.tokenId);

							if ( itemTokenIdNumber.lt(prevTokenIdNumber) ) { return  1 }
							if ( itemTokenIdNumber.gt(prevTokenIdNumber) ) { return -1 }
						} catch ( ignored ) {
							if ( `${item.tokenId}`.toLowerCase() < `${prev.tokenId}`.toLowerCase() ) { return  1 }
							if ( `${item.tokenId}`.toLowerCase() > `${prev.tokenId}`.toLowerCase() ) { return -1 }
						}
					}

					return 0
				})
		} else {
			sorted = filtered
				.sort((item, prev) => {

					if ( prev.contractAddress.toLowerCase() === item.contractAddress.toLowerCase() ) {
						try {
							if ( new BigNumber(item.tokenId).isNaN() || new BigNumber(prev.tokenId).isNaN() ) {
								if ( parseInt(`${item.tokenId}`) < parseInt(`${prev.tokenId}`) ) { return  1 }
								if ( parseInt(`${item.tokenId}`) > parseInt(`${prev.tokenId}`) ) { return -1 }
							}
							const itemTokenIdNumber = new BigNumber(item.tokenId);
							const prevTokenIdNumber = new BigNumber(prev.tokenId);

							if ( itemTokenIdNumber.lt(prevTokenIdNumber) ) { return  1 }
							if ( itemTokenIdNumber.gt(prevTokenIdNumber) ) { return -1 }
						} catch ( ignored ) {
							if ( `${item.tokenId}`.toLowerCase() < `${prev.tokenId}`.toLowerCase() ) { return  1 }
							if ( `${item.tokenId}`.toLowerCase() > `${prev.tokenId}`.toLowerCase() ) { return -1 }
						}
					}

					return item.contractAddress.toLowerCase().localeCompare(prev.contractAddress.toLowerCase());
				})
		}

		return sorted;

	}, [ tokens, filterByString, filterByToken, erc20List, sortBy, inputRangeAddress, inputRangeTokenIds ]);

	const getFooter = () => {

		if ( !selectedTokens.length ) { return; }

		return (
			<div className="modal__footer-fixed footer-lg">
				<div className="container">
					<div className="modal__footer-content">
						<div className="row justify-content-center align-items-center">
							<div className="col-auto">
								<div className="my-2">You have chosen <b>{ selectedTokens.length } { selectedTokens.length === 1 ? 'item' : 'items' }</b></div>
							</div>
							<div className="col-auto">
								<button
									className="btn btn-primary"
									onClick={() => { props.closePopup(selectedTokens) }}
								>
									Add to the showcase
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}

	const getFilterCoinsList = () => {
		if ( !filterCoinsOpened ) { return null; }
		if ( !currentChain ) { return null; }

		return (
			<ul className="options-list list-gray">

				{
					[
						chainTypeToERC20(currentChain),
						...erc20List,
					]
					.map((item) => { return item.contractAddress }).map((item) => {
						const added = !!filterByToken.find((iitem) => { return item.toLowerCase() === iitem.toLowerCase() });
						const foundToken = item === '0x0000000000000000000000000000000000000000' ?
							chainTypeToERC20(currentChain) :
							erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === item.toLowerCase() });

						if ( added ) {
							return (
								<li
									key={ item }
									className="option selected"
									onClick={() => {
										setFilterByToken(filterByToken.filter((iitem) => {
											return iitem.toLowerCase() !== item.toLowerCase();
										}));
									}}
								>
									<div className="option-coin">
										<span className="i-coin"><img src={ foundToken?.icon || default_icon } alt="" /></span>
										<span className="name">{ foundToken?.symbol || compactString(item) }</span>
									</div>
								</li>
							)
						} else {
							return (
								<li
									key={ item }
									className="option"
									onClick={() => {
										setFilterByToken([
											...filterByToken,
											item,
										]);
									}}
								>
									<div className="option-coin">
										<span className="i-coin"><img src={ foundToken?.icon || default_icon } alt="" /></span>
										<span className="name">{ foundToken?.symbol || compactString(item) }</span>
									</div>
								</li>
							)
						}
					})
				}

			</ul>
		)
	}
	const openFilterCoinsList = () => {
		setTimeout(() => {
			const body = document.querySelector('body');
			if ( !body ) { return; }
			body.onclick = (e: any) => {
				if ( !filterCoinsRef.current ) { return; }
				const _path = e.composedPath() || e.path;
				if ( _path && _path.includes(filterCoinsRef.current) ) { return; }
				closeFilterCoinsList();
			};
		}, 100);
		setFilterCoinsOpened(true);
	}
	const closeFilterCoinsList = () => {
		const body = document.querySelector('body');
		if ( !body ) { return; }
		body.onclick = null;
		setFilterCoinsOpened(false);
	}
	const getFilterSortList = () => {
		if ( !filterSortOpened ) { return null; }

		return (
			<ul className="options-list list-gray" style={{ display: 'block' }}>
				<li
					className="option"
					onClick={() => {
						setSortBy('Address');
						closeFilterSortList();
					}}
				>Address</li>
				<li
					className="option"
					onClick={() => {
						setSortBy('With image');
						closeFilterSortList();
					}}
				>With image</li>
			</ul>
		)
	}
	const openFilterSortList = () => {
		setTimeout(() => {
			const body = document.querySelector('body');
			if ( !body ) { return; }
			body.onclick = (e: any) => {
				if ( !filterSortRef.current ) { return; }
				const _path = e.composedPath() || e.path;
				if ( _path && _path.includes(filterSortRef.current) ) { return; }
				closeFilterSortList();
			};
		}, 100);
		setFilterSortOpened(true);
	}
	const closeFilterSortList = () => {
		const body = document.querySelector('body');
		if ( !body ) { return; }
		body.onclick = null;
		setFilterSortOpened(false);
	}
	const getFilterBlock = () => {

		if ( !currentChain ) { return null; }

		let searchFieldClazz = 'col-12 col-md-5 col-lg-6 mb-4';
		let selectTokensClazz = 'col-12 col-sm-8 col-md-4 col-lg-4 mb-4';
		let sortFieldClazz = 'col-12 col-sm-4 col-md-3 col-lg-2 mb-4';

		if ( tokensType === 'nft' ) {
			searchFieldClazz  = 'col-12 col-sm-8 col-md-8 col-lg-8 mb-4';
			sortFieldClazz    = 'col-12 col-sm-4 col-md-4 col-lg-4 mb-4';
		}

		return (
			<div className="row">
				<div className={ searchFieldClazz }>
					<input
						className="input-control control-gray control-search"
						type="text"
						placeholder="NFT Address, ID..."
						value={ filterByString }
						onChange={(e) => {
							const strUpdated = e.target.value;
							setFilterByString(strUpdated);
						}}
					/>
				</div>
				{
					tokensType === 'wnft' ? (
						<div className={ selectTokensClazz }>
							<div
								className="select-custom select-collateral"
								ref={ filterCoinsRef }
							>
								<div
									onClick={() => {
										if ( filterCoinsOpened ) {
											closeFilterCoinsList();
										} else {
											openFilterCoinsList();
										}
									}}
									className={`input-control control-gray ${ filterCoinsOpened ? 'active' : '' }`}
								>
									{
										filterByToken.length ?
										(
											<span className="coins">
												{
													filterByToken.map((item) => {
														const foundToken = item === '0x0000000000000000000000000000000000000000' ?
															chainTypeToERC20(currentChain) :
															erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === item.toLowerCase() });
														let icon = default_icon;
														if ( foundToken ) {
															icon = foundToken.icon
														} else {
															requestERC20Token(item);
														}
														return ( <span className="i-coin"><img src={ icon } alt="" /></span> )
													})
												}
											</span>
										): ( <span className="empty">Select tokens</span> )
									}
								</div>

									{ getFilterCoinsList() }
							</div>
						</div>
					) : null
				}
				<div className={ sortFieldClazz }>
					<div
						className="select-custom"
						ref={ filterSortRef }
					>
						<div
							className={`input-control control-gray ${ filterSortOpened ? 'active' : '' }`}
							onClick={() => {
								if ( filterSortOpened ) {
									closeFilterSortList();
								} else {
									openFilterSortList();
								}
							}}
						>
							<span className="empty">{ sortBy === '' ? 'Sort by' : sortBy }</span>
						</div>
						{ getFilterSortList() }
					</div>
				</div>
			</div>
		)
	}

	const getRangeBlock = () => {
		return (
			<div className="row">
				<div className="col-12 col-sm-8 col-md mb-4">
					<input
						className="input-control control-gray"
						type="text"
						placeholder="Contract Address"
						value={ inputRangeAddress }
						onChange={(e) => {
							const value = e.target.value.replace(/[^a-fA-F0-9xX]/g, "");
							setInputRangeAddress(value);
						}}
					/>
				</div>
				<div className="col-12 col-sm-4 col-md-5 mb-4">
					<input
						className="input-control control-gray"
						type="text"
						placeholder="ID range"
						value={ inputRangeTokenIds }
						onChange={(e) => {
							const value = e.target.value.replace(/[^0-9,\-—]/g, "").replaceAll('—', '-');
							setInputRangeTokenIds(value);

							if ( value !== '' ) {
								let allowedIds: Array<string> = [];
								const groups = value.split(',');
								for ( const idx in groups ) {

									const group = groups[idx];

									if ( group.includes('-') ) {
										const start = parseInt(group.split('-')[0]);
										const end = parseInt(group.split('-')[1]);
										if ( !isNaN(start) && !isNaN(end) && end > start ) {
											allowedIds = [
												...allowedIds,
												...[...Array(end - start)].map((_, idx) => { return `${idx + start + 1}` })
											]
										}
									}
									if ( !isNaN(parseInt(group)) ) {
										allowedIds = [
											...allowedIds,
											`${parseInt(group)}`
										];
									}
								}
								setRangeAllowedTokenIds(allowedIds);
							} else {
								setRangeAllowedTokenIds([]);
							}
						}}
					/>
				</div>
			</div>
		)
	}

	const selectedAll = () => {
		for ( const idx in tokensFiltered ) {
			const item = tokensFiltered[idx];
			const found = selectedTokens.find((iitem) => {
				return iitem.contractAddress.toLowerCase() === item.contractAddress.toLowerCase() &&
				iitem.tokenId === item.tokenId
			});
			if ( !found ) { return false; }
		}
		return true;
	}

	const getBody = () => {

		return (
			<>
			<div className="modal modal-lg modal-with-footer">
				<div
					className="modal__inner"
					onMouseDown={(e) => {
						e.stopPropagation();
						if ((e.target as HTMLTextAreaElement).className === 'modal__inner' || (e.target as HTMLTextAreaElement).className === 'container') {
							props.closePopup([]);
						}
					}}
				>
					<div className="modal__bg"></div>
					<div className="container">
						<div className="modal__content">
							<div
								className="modal__close"
								onClick={() => { props.closePopup([]) }}
							>
								<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
									<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
								</svg>
							</div>
							<div className="c-add">
								<div className="c-add__text">
									<div className="h2">Select {tokensType === 'nft' ? 'NFTs' : 'wNFTs' }</div>
								</div>

								<div className="d-flex align-items-center mb-4">
									<span className="mr-3">by</span>
									<label className="checkbox mr-3">
										<input
											type="radio"
											name="select-mode"
											checked={ searchType === 'search' }
											onChange={() => {
												setSearchType('search');
												setInputRangeAddress('');
												setInputRangeTokenIds('');
												setFilterByString('');
												setFilterByToken([]);
												setSortBy('');
												setRangeAllowedTokenIds([]);
											}}
										/>
										<span className="check"> </span>
										<span className="check-text">Search</span>
									</label>
									<label className="checkbox">
										<input
											type="radio"
											name="select-mode"
											checked={ searchType === 'range' }
											onChange={() => {
												setSearchType('range');
												setInputRangeAddress('');
												setInputRangeTokenIds('');
												setFilterByString('');
												setFilterByToken([]);
												setSortBy('');
												setRangeAllowedTokenIds([]);
											}}
										/>
										<span className="check"> </span>
										<span className="check-text">Range</span>
									</label>
								</div>

								{ searchType === 'search' ? getFilterBlock() : getRangeBlock() }

								<div className="result mb-4">
									<label className="checkbox">
										<input
											type="checkbox"
											name="price-type"
											checked={ selectedAll() }
											onChange={() => {
												if ( selectedAll() ) {
													setSelectedTokens([]);
												} else {
													setSelectedTokens(tokensFiltered);
												}
											}}
										/>
										<span className="check"> </span>
										<span className="check-text">
											Select all
											{ ' ' }
											<b className="ml-1">{ tokensFiltered.length } { tokensFiltered.length === 1 ? 'item' : 'items' }</b>
										</span>
									</label>
								</div>
								<div className="row">
									{
										tokensFiltered.map((item) => {
										// tokens.map((item) => {

											const checked = selectedTokens.find((iitem) => { return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() && item.tokenId.toLowerCase() === iitem.tokenId.toLowerCase() })

											return <NFTCardCompact
												key={`${item.contractAddress}${item.tokenId}`}
												token={ item }
												checked={ !!checked }
												onSelect={(token: NFTorWNFT) => {
													setSelectedTokens([
														...selectedTokens.filter((item) => { return item.contractAddress.toLowerCase() !== token.contractAddress.toLowerCase() || item.tokenId.toLowerCase() !== token.tokenId.toLowerCase() }),
														token
													])
												}}
												onUnselect={(token: NFTorWNFT) => {
													setSelectedTokens(selectedTokens.filter((item) => { return item.contractAddress.toLowerCase() !== token.contractAddress.toLowerCase() || item.tokenId.toLowerCase() !== token.tokenId.toLowerCase() }))
												}}
											/>
										})
									}
								</div>

								{
									tokensLoading ? (
										<div className="lp-list__footer">
											<img className="loading" src={ icon_loading } alt="" />
										</div>
									) : null
								}
							</div>
						</div>
					</div>
				</div>
			</div>
			{ getFooter() }
			</>
		)

	}

	return getBody()
}