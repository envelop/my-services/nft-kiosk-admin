
import React, {
	useContext,
	useEffect,
	useState
} from "react";
import { Link } from "react-router-dom";
import CopyToClipboard from "react-copy-to-clipboard";

import {
	BigNumber,
	DenominatedPrice,
	DiscountUntil,
	Display,
	ERC20Type,
	Price,
	_AssetType,
	_DiscountType,
	compactString,
	dateToStr,
	dateToStrWithMonth,
	derationalize,
	groupBy,
	tokenToFloat,
	unixtimeToDate
} from "@envelop/envelop-client-core";

import {
	ERC20Context,
	Web3Context
} from "../../../dispatchers";
import {
	getCountOfAssetItemsOfDisplayChain,
	getDisplayCollateralUnitPricesChain,
	getDisplayDefaultPricesChain
} from "../../../models";

import StepsProgress from "../../StepsProgress";

import i_copy       from "../../../static/pics/icons/i-copy.svg";
import default_coin from "@envelop/envelop-client-core/static/pics/coins/_default.svg";

type SummaryStepPageProps = {
	displayToEdit: Display | undefined,
}

export default function Summary(props: SummaryStepPageProps) {

	const { displayToEdit } = props;

	const {
		currentChainId,
	} = useContext(Web3Context);
	const {
		erc20List
	} = useContext(ERC20Context);

	const [ displayLinkCopied, setDisplayLinkCopied ] = useState(false);
	const [ tokensCount      , setTokensCount       ] = useState<number | undefined>(undefined);

	const [ defaultPriceSaved         , setDefaultPriceSaved          ] = useState<Array<Price>>([]);
	const [ collateralPriceSaved      , setCollateralPriceSaved       ] = useState<Array<{ collateralToken: string, price: Array<DenominatedPrice> }>>([]);

	const [ defaultPriceBlockOpened, setDefaultPriceBlockOpened ] = useState(false);
	const [ collateralPriceBlockOpened, setCollateralPriceBlockOpened ] = useState(false);
	const [ discountsBlockOpened, setDiscountsBlockOpened ] = useState(false);

	useEffect(() => {
		if ( !displayToEdit ) { return; }
		getCountOfAssetItemsOfDisplayChain(currentChainId, displayToEdit.contractAddress, displayToEdit.nameHash)
			.then((data) => { setTokensCount(data) });

	}, [ displayToEdit ])

	useEffect(() => {
		const getPrices = async () => {
			if ( displayToEdit ) {
				const prices = await getDisplayDefaultPricesChain(currentChainId, displayToEdit.priceModel, displayToEdit.nameHash);
				setDefaultPriceSaved(
					prices.filter((item) => {
						if ( item.amount.eq(0) ) { return false; }

						return true;
					})
				);

				const collPrices = await getDisplayCollateralUnitPricesChain(currentChainId, displayToEdit.priceModel, displayToEdit.nameHash, erc20List.map((item) => { return item.contractAddress }));
				setCollateralPriceSaved(
					collPrices.filter((item) => {
						if (
							item.price.length &&
							item.price[0].amount.eq(0) &&
							item.price[0].denominator.eq(0) &&
							item.price[0].payWith === '0x0000000000000000000000000000000000000000'
						) { return false; }

						return true;
					})
				)
			}
		}

		getPrices();
	}, [ displayToEdit ])


	const getBasicInfoBlock = () => {
		if ( !displayToEdit ) { return null; }

		return (
			<div className="lp-wrap pb-3">
				<div className="row">
					<div className="col-auto mb-3">
						<div className="mb-2">Type <br /></div>
						<div><b>{ compactString(displayToEdit.priceModel) }</b></div>
					</div>
					<div className="col-auto mb-3">
						<div className="mb-2">Life time <br /></div>
						<div><b>from { dateToStrWithMonth(unixtimeToDate(new BigNumber(displayToEdit.enableAfter))) } to { dateToStrWithMonth(unixtimeToDate(new BigNumber(displayToEdit.disableAfter))) }</b></div>
					</div>
					<div className="col-auto mb-3">
						<div className="mb-2">Avalable <br /></div>
						<div><b>{ tokensCount } NFTs</b></div>
					</div>
				</div>
			</div>
		)
	}

	const getDefaultPricesBlock = () => {
		return (
			<div className="c-wrap p-0">
				<div
					className={`c-wrap__toggle ${ defaultPriceBlockOpened ? 'active' : '' }`}
					onClick={() => { setDefaultPriceBlockOpened(!defaultPriceBlockOpened) }}
				>
					<div> <b>Default price for NFTs</b></div>
				</div>
				<div className="c-wrap__dropdown">
					<div className="c-wrap__table">
						{
							defaultPriceSaved.map((item) => {
								let foundToken: ERC20Type | undefined = erc20List.find((iitem: ERC20Type) => { return iitem.contractAddress.toLowerCase() === item.payWith.toLowerCase() });
								if ( !foundToken ) {
									foundToken = {
										assetType: _AssetType.ERC20,
										contractAddress: item.payWith,
										decimals: 18,
										balance: new BigNumber(0),
										icon: default_coin,
										name: compactString(item.payWith),
										symbol: compactString(item.payWith),
										allowance: [],
									}
								}

								return (
									<div className="item" key={ item.idx }>
										<div className="row">
											<div className="col-auto col-md-2 mb-2">
												<div className="tb-coin"><span className="i-coin"><img src={ foundToken.icon } alt="" /></span><span className="name">{ foundToken.symbol }</span></div>
											</div>
											<div className="col mb-2 d-flex align-items-center"><span className="text-break">{ tokenToFloat(new BigNumber(item.amount), foundToken.decimals).toString() }</span></div>
										</div>
									</div>
								)
							})
						}
					</div>
				</div>
			</div>
		)
	}
	const getCollateralPricesBlock = () => {

		const groups = groupBy(collateralPriceSaved, (item) => { return item.collateralToken });

		return (
			<div className="c-wrap p-0">
				<div
					className={`c-wrap__toggle ${ collateralPriceBlockOpened ? 'active' : '' }`}
					onClick={() => { setCollateralPriceBlockOpened(!collateralPriceBlockOpened) }}
				>
					<div> <b>Default price for 1&nbsp;collateral token in&nbsp;wNFTs</b></div>
				</div>
				<div className="c-wrap__dropdown">
					{
						Object.keys(groups).map((idx: string) => {
							let foundCollateralToken: ERC20Type | undefined = erc20List.find((iitem: ERC20Type) => { return iitem.contractAddress.toLowerCase() === idx.toLowerCase() });
							if ( !foundCollateralToken ) {
								foundCollateralToken = {
									assetType: _AssetType.ERC20,
									contractAddress: idx,
									decimals: 18,
									balance: new BigNumber(0),
									icon: default_coin,
									name: compactString(idx),
									symbol: compactString(idx),
									allowance: [],
								}
							}

							return (
								<React.Fragment key={ idx }>
									<div className="d-flex align-center my-3">
										<span className="i-coin mr-2"><img src={ foundCollateralToken.icon } alt="" /></span><span className="name"> <b className="mr-1">1 { foundCollateralToken.symbol }</b>for</span>
									</div>
									<div className="c-wrap__table mb-6">
										{
											groups[idx][0].price.map((iitem) => {
												let foundToken: ERC20Type | undefined = erc20List.find((iiitem: ERC20Type) => { return iiitem.contractAddress.toLowerCase() === iitem.payWith.toLowerCase() });
												if ( !foundToken ) {
													foundToken = {
														assetType: _AssetType.ERC20,
														contractAddress: iitem.payWith,
														decimals: 18,
														balance: new BigNumber(0),
														icon: default_coin,
														name: compactString(iitem.payWith),
														symbol: compactString(iitem.payWith),
														allowance: [],
													}
												}

												return (
													<div className="item" key={ iitem.payWith }>
														<div className="row">
															<div className="col-auto col-md-2 mb-2">
																<div className="tb-coin"> <span className="i-coin"><img src={ foundToken.icon } alt="" /></span><span className="name">{ foundToken.symbol }</span></div>
															</div>
															<div className="col mb-2 d-flex align-items-center"><span className="text-break">{ derationalize(iitem.amount, iitem.denominator, new BigNumber((foundCollateralToken?.decimals || 18) - foundToken.decimals)).toString() }</span></div>
														</div>
													</div>
												)
											})
										}
									</div>
								</React.Fragment>
							)
						})
					}
				</div>
			</div>
		)
	}

	const getDiscountsBlock = () => {
		if ( !displayToEdit ) { return null; }
		return (
			<div className="c-wrap p-0">
				<div
					className={`c-wrap__toggle ${ discountsBlockOpened ? 'active' : '' }`}
					onClick={() => { setDiscountsBlockOpened(!discountsBlockOpened) }}
				>
					<div> <b>Discount rules</b></div>
				</div>
				<div className="c-wrap__dropdown">
					<div className="my-3"><b className="text-green">Time discount</b></div>
					<div className="c-wrap__table">
						{
							displayToEdit.discounts.map((item) => {
								return (
									<div className="item" key={item.untilDate.toString()}>
										<div className="row">
											<div className="col-sm-1 col-md-1 mb-2"> <span>{ item.discount.dsctPercent.toString() }%</span></div>
											<div className="col-sm-10 mb-2"> <span className="text-break">until { dateToStr(unixtimeToDate(item.untilDate)) } { unixtimeToDate(item.untilDate).getHours().toString().padStart(2, '0') }:{ unixtimeToDate(item.untilDate).getMinutes().toString().padStart(2, '0') }</span></div>
										</div>
									</div>
								)
							})
						}
					</div>
				</div>
			</div>
		)
	}

	return (
		<div className="container">

			<div className="breadcrumbs">
				<Link className="item-back btn btn-sm btn-gray" to="/">
					<svg className="mr-2" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M12 5.25H2.8725L7.065 1.0575L6 0L0 6L6 12L7.0575 10.9425L2.8725 6.75H12V5.25Z" fill="white"></path>
					</svg>
					<span>To the showcase list</span>
				</Link>
			</div>

			<div className="row mb-4">
				<div className="col-12 col-sm">
					<div className="h3 mb-3 mt-0">{ displayToEdit?.title || '<Untitled>' }</div>
				</div>
				{
					displayToEdit ? (
						<div className="col-12 col-sm-auto">
							<div className="btns-group">
								<div className="btn-group">
									<a
										className="btn btn-md btn-gray"
										href={ `/launchpad/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.name}` }
										target="_blank" rel="noopener noreferrer"
									>Go to the showcase</a>
								</div>
								<div className="btn-group">
									<CopyToClipboard
										text={ `${window.location.origin}/launchpad/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.name}` }
										onCopy={() => {
											setDisplayLinkCopied(true);
											setTimeout(() => { setDisplayLinkCopied(false); }, 5*1000);
										}}
									>
										<button className="btn btn-md btn-gray btn-img">
											<img src= { i_copy } alt="" />
											<span className="btn-action-info" style={{ display: displayLinkCopied ? 'block' : 'none' }}>Copied</span>
										</button>
									</CopyToClipboard>
								</div>
							</div>
						</div>
					) : null
				}
			</div>
			<div className="row">
				<div className="col-md-2 col-lg-2">
					<StepsProgress current="summary" display={ displayToEdit } />
				</div>
				<div className="col-md-10">
					{ getBasicInfoBlock() }

					{ getDefaultPricesBlock() }
					{ getCollateralPricesBlock() }

					{ getDiscountsBlock() }

					<div className="lp-wrap">
						<div className="row justify-content-between">
							<div className="col-auto">
								{
									displayToEdit ? (
										<Link
											className="btn btn-gray"
											to={ `/token/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.nameHash}` }
										>Back</Link>
									) : (
										<Link
											className="btn btn-gray"
											to={ `/` }
										>Back</Link>
									)
								}
							</div>
							<div className="col-auto">
								<Link
									className="btn btn-gray"
									to={ `/` }
								>Showcases list</Link>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}