
import {
	useContext,
	useEffect,
	useState
} from "react";
import {
	Link,
	useNavigate,
} from "react-router-dom";
import StepsProgress       from "../../StepsProgress";
import TippyWrapper        from "../../TippyWrapper"
import { CopyToClipboard } from 'react-copy-to-clipboard';
import ReactDatePicker     from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import {
	BigNumber,
	DiscountUntil,
	_DiscountType,
	_Display,
	dateToStr,
	dateToUnixtime,
	unixtimeToDate,
	Display,
} from "@envelop/envelop-client-core";

import {
	setPromocodeDiscountInChain,
	setReferrerDiscountInChain,
	setTimeDiscountInChain,
	editTimeDiscountInChain,
} from "../../../models";

import i_edit from "../../../static/pics/icons/i-edit.svg";
import i_del  from "../../../static/pics/icons/i-del.svg";
import i_copy from "../../../static/pics/icons/i-copy.svg";
import i_lock from "../../../static/pics/icons/i-lock.svg";

import {
	AdvancedLoaderStageType,
	InfoModalContext,
	Web3Context,
	_AdvancedLoadingStatus,
	_ModalTypes
} from "../../../dispatchers";

type EditableFieldValue = {
	amount: string,
	param?: string,
	until: Date
}
type EditableField = {
	currentValue  : EditableFieldValue,
	editedValue?  : EditableFieldValue,
	originalvalue?: EditableFieldValue,
	isEdit: boolean,
	id: number,
	idx?: number,
}

type DiscountsStepPageProps = {
	displayToEdit: Display | undefined,
	onUpdate     : () => void,
}

export default function DiscountsStepPage(props: DiscountsStepPageProps) {

	const { displayToEdit } = props;
	const navigate          = useNavigate();

	const {
		setModal,
		unsetModal,
		createAdvancedLoader,
		updateStepAdvancedLoader,
	} = useContext(InfoModalContext);
	const {
		web3,
		userAddress,
		currentChain,
	} = useContext(Web3Context);

	const [ inputTimeAmount     , setInputTimeAmount      ] = useState('');
	const [ inputTimeUntil      , setInputTimeUntil       ] = useState<Date | null>(null);
	const [ inputTimeSaved      , setInputTimeSaved       ] = useState<Array<EditableField>>([]);
	const [ inputTimeToEdit     , setInputTimeToEdit      ] = useState<Array<EditableField>>([]);
	const [ inputTimeToAdd      , setInputTimeToAdd       ] = useState<Array<EditableField>>([]);
	const [ inputTimeToDel      , setInputTimeToDel       ] = useState<Array<EditableField>>([]);

	const [ inputPromoAmount    , setInputPromoAmount     ] = useState('');
	const [ inputPromoCode      , setInputPromoCode       ] = useState('');
	const [ inputPromoUntil     , setInputPromoUntil      ] = useState<Date | null>(null);
	const [ inputPromoList      , setInputPromoList       ] = useState<Array<EditableField>>([]);

	const [ inputReferrerAmount , setInputReferrerAmount  ] = useState('');
	const [ inputReferrerAddress, setInputReferrerAddress ] = useState('');
	const [ inputReferrerUntil  , setInputReferrerUntil   ] = useState<Date | null>(null);
	const [ inputReferrerList   , setInputReferrerList    ] = useState<Array<EditableField>>([]);

	const [ displayLinkCopied   , setDisplayLinkCopied    ] = useState(false);

	useEffect(() => {
		if ( !displayToEdit || !displayToEdit.discounts ) { return; }

		const discountsParsedTime = displayToEdit.discounts
			.filter((item) => { return item.discount.dsctType === _DiscountType.TIME })
			.filter((item) => { return !item.discount.dsctPercent.eq(0) && !item.untilDate.eq(0) })

		setInputTimeToEdit([]);
		setInputTimeToAdd([]);
		setInputTimeSaved([
			...discountsParsedTime.map((item, idx) => {
				const field = {
					amount: item.discount.dsctPercent.toFixed(2, BigNumber.ROUND_DOWN),
					until: unixtimeToDate(item.untilDate)
				};
				return {
					currentValue: field,
					id: new Date().getTime() + idx,
					isEdit: false,
					idx,
				}
			})
		])

	}, [ displayToEdit ])

	const getTimeDiscountDelBtn = (row: EditableField) => {
		const alreadyAddedSaved  = inputTimeSaved.find( (item) => { return item.id === row.id });
		const alreadyAddedToAdd  = inputTimeToAdd.find( (item) => { return item.id === row.id });
		const alreadyAddedToEdit = inputTimeToEdit.find((item) => { return item.id === row.id });

		if ( alreadyAddedToAdd ) {
			return (
				<div className="btn-group">
					<button
						className="btn btn-md btn-gray btn-img"
						onClick={() => {
							setInputTimeToAdd(inputTimeToAdd.filter((item) => { return item.id !== row.id }))
						}}
					>
						<img src= { i_del } alt="" />
					</button>
				</div>
			)
		}

		if ( alreadyAddedSaved || alreadyAddedToEdit ) {
			return (
				<div className="btn-group">
					<button
						className="btn btn-md btn-gray btn-img"
						onClick={() => {
							setInputTimeToEdit(inputTimeToEdit.filter((item) => { return item.id !== row.id }));
							setInputTimeSaved(inputTimeSaved.filter((item) => { return item.id !== row.id }));
							setInputTimeToDel([
								...inputTimeToDel.filter((item) => { return item.id !== row.id }),
								row
							]);
						}}
					>
						<img src= { i_del } alt="" />
					</button>
				</div>
			)
		}

		return (
			<div className="btn-group">
				<button
					className="btn btn-md btn-gray btn-img"
					disabled={ true }
				>
					<img src={ i_lock } alt="" />
				</button>
			</div>
		)
	}
	const getTimeDiscountRow = (row: EditableField) => {
		if ( row.isEdit ) {

			const undoEdit = () => {
				const alreadyAddedSaved = inputTimeSaved.find((item) => { return item.id === row.id });
				if ( alreadyAddedSaved ) {
					setInputTimeSaved([
						...inputTimeSaved.filter((item) => { return item.id !== row.id }),
						{
							...row,
							isEdit: false,
							editedValue: undefined
						}
					]);
					return;
				}
				const alreadyAddedToEdit = inputTimeToEdit.find((item) => { return item.id === row.id });
				if ( alreadyAddedToEdit ) {
					setInputTimeToEdit([
						...inputTimeToEdit.filter((item) => { return item.id !== row.id }),
						{
							...row,
							isEdit: false,
							editedValue: undefined
						}
					]);
					return;
				}

				setInputTimeToAdd([
					...inputTimeToAdd.filter((item) => { return item.id !== row.id }),
					{
						...row,
						isEdit: false,
							editedValue: undefined
					}
				]);
			}
			const saveEdit = () => {
				const alreadyAddedSaved = inputTimeSaved.find((item) => { return item.id === row.id });
				if ( alreadyAddedSaved ) {
					setInputTimeSaved(inputTimeSaved.filter((item) => { return item.id !== row.id }));
					setInputTimeToEdit([
						...inputTimeToEdit.filter((item) => { return item.id !== row.id }),
						{
							...row,
							isEdit: false,
							currentValue: {
								amount: row.editedValue?.amount || '',
								until: row.editedValue?.until || new Date(),
								param: row.editedValue?.param,
							},
							editedValue: undefined
						}
					]);
					return;
				}
				const alreadyAddedToEdit = inputTimeToEdit.find((item) => { return item.id === row.id });
				if ( alreadyAddedToEdit ) {
					setInputTimeToEdit([
						...inputTimeToEdit.filter((item) => { return item.id !== row.id }),
						{
							...row,
							isEdit: false,
							currentValue: {
								amount: row.editedValue?.amount || '',
								until: row.editedValue?.until || new Date(),
								param: row.editedValue?.param,
							},
							editedValue: undefined
						}
					]);
					return;
				}

				setInputTimeToAdd([
					...inputTimeToAdd.filter((item) => { return item.id !== row.id }),
					{
						...row,
						isEdit: false,
							currentValue: {
								amount: row.editedValue?.amount || '',
								until: row.editedValue?.until || new Date(),
								param: row.editedValue?.param,
							},
							editedValue: undefined
					}
				]);
			}

			return (
				<div className="item item-narrow" key={ row.id }>
					<div className="row-edit">
						<div className="col-sm-2 col-lg-2 mb-2">
							<input
								className="input-control"
								type="text"
								placeholder=""
								value={row.editedValue?.amount.toString()}
								onChange={(e) => {

									let value = e.target.value.replaceAll(' ', '').replaceAll(',', '.');
									const valueParsed = new BigNumber(value);

									if ( value === '' ) {
										value = '';
									} else {
										if ( valueParsed.isNaN() ) { return; }
										if ( valueParsed.gt(100) ) { return; }
									}

									const alreadyAddedSaved = inputTimeSaved.find((item) => { return item.id === row.id });
									if ( alreadyAddedSaved ) {
										setInputTimeSaved([
											...inputTimeSaved.filter((item) => { return item.id !== row.id }),
											{
												...row,
												editedValue: {
													amount: value,
													param: row.editedValue?.param,
													until: row.editedValue?.until || new Date(),
												}
											}
										]);
										return;
									}
									const alreadyAddedToEdit = inputTimeToEdit.find((item) => { return item.id === row.id });
									if ( alreadyAddedToEdit ) {
										setInputTimeToEdit([
											...inputTimeToEdit.filter((item) => { return item.id !== row.id }),
											{
												...row,
												editedValue: {
													amount: value,
													param: row.editedValue?.param,
													until: row.editedValue?.until || new Date(),
												}
											}
										]);
										return;
									}

									setInputTimeToAdd([
										...inputTimeToAdd.filter((item) => { return item.id !== row.id }),
										{
											...row,
											editedValue: {
												amount: value,
												param: row.editedValue?.param,
												until: row.editedValue?.until || new Date(),
											}
										}
									])
								}}
								onBlur={() => {
									const alreadyAddedSaved = inputTimeSaved.find((item) => { return item.id === row.id });
									if ( alreadyAddedSaved ) {
										setInputTimeSaved([
											...inputTimeSaved.filter((item) => { return item.id !== row.id }),
											{
												...row,
												editedValue: {
													amount: new BigNumber(row.editedValue?.amount || 0).toFixed(2, BigNumber.ROUND_DOWN) || '',
													param: row.editedValue?.param,
													until: row.editedValue?.until || new Date(),
												}
											}
										]);
										return;
									}
									const alreadyAddedToEdit = inputTimeToEdit.find((item) => { return item.id === row.id });
									if ( alreadyAddedToEdit ) {
										setInputTimeToEdit([
											...inputTimeToEdit.filter((item) => { return item.id !== row.id }),
											{
												...row,
												editedValue: {
													amount: new BigNumber(row.editedValue?.amount || 0).toFixed(2, BigNumber.ROUND_DOWN) || '',
													param: row.editedValue?.param,
													until: row.editedValue?.until || new Date(),
												}
											}
										]);
										return;
									}

									setInputTimeToAdd([
										...inputTimeToAdd.filter((item) => { return item.id !== row.id }),
										{
											...row,
											editedValue: {
												amount: new BigNumber(row.editedValue?.amount || 0).toFixed(2, BigNumber.ROUND_DOWN) || '',
												param: row.editedValue?.param,
												until: row.editedValue?.until || new Date(),
											}
										}
									]);
								}}
							/>
						</div>
						<div className="col-sm-4 col-lg-3 mb-2">
							<ReactDatePicker
								className="input-control control-calendar"
								portalId="calendarParent"
								selected={row.editedValue?.until}
								onChange={(date) => {
									if ( !date ) { return null; }
									const alreadyAddedSaved = inputTimeSaved.find((item) => { return item.id === row.id });
									if ( alreadyAddedSaved ) {
										setInputTimeSaved(inputTimeSaved.filter((item) => { return item.id !== row.id }));
										setInputTimeToEdit([
											...inputTimeToEdit.filter((item) => { return item.id !== row.id }),
											{
												...row,
												editedValue: {
													amount: row.editedValue?.amount || '',
													param: row.editedValue?.param,
													until: date,
												}
											}
										]);
										return;
									}
									const alreadyAddedToEdit = inputTimeToEdit.find((item) => { return item.id === row.id });
									if ( alreadyAddedToEdit ) {
										setInputTimeToEdit([
											...inputTimeToEdit.filter((item) => { return item.id !== row.id }),
											{
												...row,
												editedValue: {
													amount: row.editedValue?.amount || '',
													param: row.editedValue?.param,
													until: date,
												}
											}
										]);
										return;
									}

									setInputTimeToAdd([
										...inputTimeToAdd.filter((item) => { return item.id !== row.id }),
										{
											...row,
											editedValue: {
												amount: row.editedValue?.amount || '',
												param: row.editedValue?.param,
												until: date,
											}
										}
									]);
								}}
								showTimeSelect
								dateFormat="yyyy-MM-dd HH:mm"
								locale={'en-US'}
								shouldCloseOnSelect={false}
							/>
						</div>
						<div className="col-6 col-sm-2 offset-sm-2 offset-lg-3 mb-2">
							<button
								className="btn btn-primary w-100"
								disabled={ row.editedValue && new BigNumber(row.editedValue.amount).eq(0) }
								onClick={() => {

									if (
										row.editedValue &&
										row.currentValue.amount.toLowerCase() === row.editedValue.amount.toLowerCase() &&
										row.currentValue.until.getTime() === row.editedValue.until.getTime()
									) {
										undoEdit();
										return;
									}

									saveEdit();
								}}
							>
									Save
							</button>
						</div>
						<div className="col-6 col-sm-2 mb-2">
							<button
								className="btn btn-gray w-100"
								onClick={() => {
									undoEdit();
								}}
							>
									Undo
							</button>
						</div>
					</div>
				</div>
			)
		} else {
			return (
				<div className="item item-narrow" key={ row.id }>
					<div className="row">
						<div className="col-sm-1 mb-2">
							<span>{ row.currentValue.amount.toString() }%</span>
						</div>
						<div className="col-sm-8 mb-2">
							<span className="text-break">
								until { dateToStr(unixtimeToDate(new BigNumber(row.currentValue.until.getTime()).dividedToIntegerBy(1000))) } { row.currentValue.until.getHours().toString().padStart(2, '0') }:{ row.currentValue.until.getMinutes().toString().padStart(2, '0') }
							</span>
						</div>
						<div className="col-sm-3 mb-2 d-flex">
							<div className="btns-group mr-0 ml-auto">
								<div className="btn-group">
									<button
										className="btn btn-md btn-gray btn-img"
										onClick={() => {
											const alreadyAddedSaved = inputTimeSaved.find((item) => { return item.id === row.id });
											if ( alreadyAddedSaved ) {
												setInputTimeSaved([
													...inputTimeSaved.filter((item) => { return item.id !== row.id }),
													{
														...row,
														isEdit: true,
														editedValue: row.currentValue,
													}
												]);
												return;
											}
											const alreadyAddedToEdit = inputTimeToEdit.find((item) => { return item.id === row.id });
											if ( alreadyAddedToEdit ) {
												setInputTimeToEdit([
													...inputTimeToEdit.filter((item) => { return item.id !== row.id }),
													{
														...row,
														isEdit: true,
														editedValue: row.currentValue,
													}
												]);
												return;
											}

											setInputTimeToAdd([
												...inputTimeToAdd.filter((item) => { return item.id !== row.id }),
												{
													...row,
													isEdit: true,
													editedValue: row.currentValue,
												}
											]);
										}}
									>
										<img src={ i_edit } alt="" />
									</button>
								</div>
								{ getTimeDiscountDelBtn(row) }
							</div>
						</div>
					</div>
				</div>
			)
		}
	}
	const getTimeDiscountBlock = () => {
		return (
			<div className="lp-step-block">
				<h4>
					Time discount
					<TippyWrapper msg="Discounts that are valid until some point in time. You can set up several discounts with different start and end periods. The first discount starts from the current moment" elClass="ml-1" />
				</h4>
				<div className="row">
					<div className="col-sm-6 col-md-2">
						<div className="input-group">
							<label className="input-label">%</label>
							<input
								className="input-control"
								type="text"
								placeholder="0%"
								value={ inputTimeAmount }
								onChange={(e) => {
									let value = e.target.value.replaceAll(' ', '').replaceAll(',', '.');
									const valueParsed = new BigNumber(value);

									if ( value === '' ) {
										value = '';
									} else {
										if ( valueParsed.isNaN() ) { return; }
										if ( valueParsed.gt(100) ) { return; }
									}

									setInputTimeAmount(value)
								}}
								onBlur={() => {
									if ( inputTimeAmount === '' ) { return; }
									setInputTimeAmount(new BigNumber(inputTimeAmount).toFixed(2, BigNumber.ROUND_DOWN))
								}}
							/>
						</div>
					</div>
					<div className="col-sm-6 col-md-3">
						<div className="input-group">
							<label className="input-label">Active until</label>
							<ReactDatePicker
								className="input-control control-calendar"
								portalId="calendarParent"
								selected={inputTimeUntil}
								onChange={(date) => {
									setInputTimeUntil(date)
								}}
								showTimeSelect
								dateFormat="yyyy-MM-dd HH:mm"
								locale={'en-US'}
								shouldCloseOnSelect={false}
							/>
						</div>
					</div>
					<div className="col-md-2">
						<div className="input-group">
							<label className="input-label d-none d-md-block">&nbsp;</label>
							<button
								className="btn btn-outline w-100"
								type="submit"
								disabled={ inputTimeAmount === '' || inputTimeUntil === null || new BigNumber(inputTimeAmount).eq(0) }
								onClick={() => {
									if ( !inputTimeUntil ) { return; }
									setInputTimeAmount('');
									setInputTimeUntil(null);
									setInputTimeToAdd([
										...inputTimeToAdd,
										{
											id: new Date().getTime(),
											isEdit: false,
											currentValue: {
												amount: inputTimeAmount,
												until: inputTimeUntil
											}
										}
									].sort((item, prev) => { return item.currentValue.until.getTime() - prev.currentValue.until.getTime() }))
								}}
							>Add</button>
						</div>
					</div>
				</div>
				<div className="c-wrap__table">
					{
						[
							...inputTimeSaved,
							...inputTimeToEdit,
							...inputTimeToAdd,
						]
							.sort((item, prev) => { return item.id - prev.id })
							.map((item) => { return getTimeDiscountRow(item); })
					}
				</div>
			</div>
		)
	}

	const getPromocodeDiscountRow = (row: EditableField) => {
		if ( row.isEdit ) {
			return (
				<div className="item item-narrow" key={ row.id }>
					<div className="row-edit">
						<div className="col-sm-4 col-lg-1 mb-2">
							<input
								className="input-control"
								type="text"
								placeholder=""
								value={row.editedValue?.amount.toString()}
								onChange={(e) => {

									let value = e.target.value.replaceAll(' ', '').replaceAll(',', '.');
									const valueParsed = new BigNumber(value);

									if ( value === '' ) {
										value = '';
									} else {
										if ( valueParsed.isNaN() ) { return; }
										if ( valueParsed.gt(100) ) { return; }
									}

									setInputPromoList([
										...inputPromoList.filter((item) => { return item.id !== row.id }),
										{
											...row,
											editedValue: {
												amount: value,
												param: row.editedValue?.param,
												until: row.editedValue?.until || new Date(),
											}
										}
									])
								}}
								onBlur={() => {
									setInputPromoList([
										...inputPromoList.filter((item) => { return item.id !== row.id }),
										{
											...row,
											editedValue: {
												amount: new BigNumber(row.editedValue?.amount || 0).toFixed(2, BigNumber.ROUND_DOWN) || '',
												param: row.editedValue?.param,
												until: row.editedValue?.until || new Date(),
											}
										}
									])
								}}
							/>
						</div>
						<div className="col-sm-8 col-lg-3 mb-2">
							<ReactDatePicker
								className="input-control control-calendar"
								portalId="calendarParent"
								selected={row.editedValue?.until}
								onChange={(date) => {
									if ( !date ) { return null; }
									setInputPromoList([
										...inputPromoList.filter((item) => { return item.id !== row.id }),
										{
											...row,
											editedValue: {
												amount: row.editedValue?.amount || '',
												param: row.editedValue?.param,
												until: date,
											}
										}
									])
								}}
								showTimeSelect
								dateFormat="yyyy-MM-dd HH:mm"
								locale={'en-US'}
								shouldCloseOnSelect={false}
							/>
						</div>
						<div className="col-lg-4 mb-2">
							<input
								className="input-control"
								type="text"
								placeholder="PROMOCODE"
								value={row.editedValue?.param}
								onChange={(e) => {
									setInputPromoList([
										...inputPromoList.filter((item) => { return item.id !== row.id }),
										{
											...row,
											editedValue: {
												amount: row.editedValue?.amount || '',
												param: e.target.value,
												until: row.editedValue?.until || new Date(),
											}
										}
									])
								}}
							/>
                        </div>
						<div className="col-6 col-sm-2 mb-2">
							<button
								className="btn btn-primary w-100"
								disabled={ row.editedValue && new BigNumber(row.editedValue.amount).eq(0) }
								onClick={() => {
									setInputPromoList([
										...inputPromoList.filter((item) => { return item.id !== row.id }),
										{
											...row,
											isEdit: false,
											currentValue: {
												amount: row.editedValue?.amount || '',
												until: row.editedValue?.until || new Date(),
												param: row.editedValue?.param,
											}
										}
									])
								}}
							>
									Save
							</button>
						</div>
						<div className="col-6 col-sm-2 mb-2">
							<button
								className="btn btn-gray w-100"
								onClick={() => {
									setInputPromoList([
										...inputPromoList.filter((item) => { return item.id !== row.id }),
										{
											...row,
											isEdit: false,
											editedValue: undefined
										}
									])
								}}
							>
									Undo
							</button>
						</div>
					</div>
				</div>
			)
		} else {
			return (
				<div className="item item-narrow" key={ row.id }>
					<div className="row">
						<div className="col-sm-1 mb-2">
							<span>{ row.currentValue.amount.toString() }%</span>
						</div>
						<div className="col-sm-3 mb-2">
							<span className="text-break">
								until { dateToStr(row.currentValue.until) } { row.currentValue.until.getHours().toString().padStart(2, '0') }:{ row.currentValue.until.getMinutes().toString().padStart(2, '0') }
							</span>
						</div>
						<div className="col-sm-5 col-lg-6 mb-2">
							<span className="text-break">
								{ row.currentValue.param }
							</span>
						</div>
						<div className="col-sm-3 col-lg-2 mb-2 d-flex">
							<div className="btns-group mr-0 ml-auto">
								<div className="btn-group">
									<button
										className="btn btn-md btn-gray btn-img"
										onClick={() => {
											setInputPromoList([
												...inputPromoList.filter((item) => { return row.id !== item.id }),
												{
													...row,
													isEdit: true,
													editedValue: row.currentValue,
												}
											])
										}}
									>
										<img src={ i_edit } alt="" />
									</button>
								</div>
								<div className="btn-group">
									<button
										className="btn btn-md btn-gray btn-img"
										onClick={() => {
											setInputPromoList(inputPromoList.filter((item) => { return row.id !== item.id }))
										}}
									>
										<img src={ i_del } alt="" />
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			)
		}
	}
	const getPromocodeDiscountBlock = () => {
		return (
			<div className="lp-step-block">
				<h4>
					Promocode discount
					<TippyWrapper msg="Buyer discount for promocode. More than one promocode can be active at the same time in the showcase" elClass="ml-1" />
				</h4>
				<div className="row">
					<div className="col-sm-6 col-md-2">
						<div className="input-group">
							<label className="input-label">%</label>
							<input
								className="input-control"
								type="text"
								placeholder="0%"
								value={ inputPromoAmount }
								onChange={(e) => {
									let value = e.target.value.replaceAll(' ', '').replaceAll(',', '.');
									const valueParsed = new BigNumber(value);

									if ( value === '' ) {
										value = '';
									} else {
										if ( valueParsed.isNaN() ) { return; }
										if ( valueParsed.gt(100) ) { return; }
									}

									setInputPromoAmount(value)
								}}
								onBlur={() => {
									if ( inputPromoAmount === '' ) { return; }
									setInputPromoAmount(new BigNumber(inputPromoAmount).toFixed(2, BigNumber.ROUND_DOWN))
								}}
							/>
						</div>
					</div>
					<div className="col-sm-6 col-md-3">
						<div className="input-group">
							<label className="input-label">Active until</label>
							<ReactDatePicker
								className="input-control control-calendar"
								portalId="calendarParent"
								selected={ inputPromoUntil }
								onChange={(date) => {
									setInputPromoUntil(date)
								}}
								showTimeSelect
								dateFormat="yyyy-MM-dd HH:mm"
								locale={'en-US'}
								shouldCloseOnSelect={false}
							/>
						</div>
					</div>
					<div className="col-md-5">
						<div className="input-group">
							<label className="input-label">Promo code</label>
							<input
								className="input-control"
								type="text"
								placeholder=""
								value = { inputPromoCode }
								onChange={(e) => { setInputPromoCode(e.target.value.replaceAll(' ', '').replace(/[^a-zA-Z0-9]/g, "")) }}
							/>
						</div>
					</div>
					<div className="col-md-2">
						<div className="input-group">
							<label className="input-label d-none d-md-block">&nbsp;</label>
							<button
								className="btn btn-outline w-100"
								type="submit"
								disabled={ inputPromoAmount === '' || inputPromoCode === '' || inputPromoUntil === null || new BigNumber(inputPromoAmount).eq(0) }
								onClick={() => {
									if ( !inputPromoUntil ) { return; }
									setInputPromoAmount('');
									setInputPromoCode('');
									setInputPromoUntil(null);
									setInputPromoList([
										...inputPromoList,
										{
											id: new Date().getTime(),
											isEdit: false,
											currentValue: {
												amount: inputPromoAmount,
												param: inputPromoCode,
												until: inputPromoUntil
											}
										}
									].sort((item, prev) => { return item.currentValue.until.getTime() - prev.currentValue.until.getTime() }))
								}}
							>Add</button>
						</div>
					</div>
				</div>
				<div className="c-wrap__table">
					{ inputPromoList.map((item) => { return getPromocodeDiscountRow(item) }) }
				</div>
			</div>
		)
	}

	const getReferrerDiscountRow = (row: EditableField) => {
		if ( row.isEdit ) {
			return (
				<div className="item item-narrow" key={ row.id }>
					<div className="row-edit">
						<div className="col-sm-4 col-lg-1 mb-2">
							<input
								className="input-control"
								type="text"
								placeholder=""
								value={row.editedValue?.amount.toString()}
								onChange={(e) => {

									let value = e.target.value.replaceAll(' ', '').replaceAll(',', '.');
									const valueParsed = new BigNumber(value);

									if ( value === '' ) {
										value = '';
									} else {
										if ( valueParsed.isNaN() ) { return; }
										if ( valueParsed.gt(100) ) { return; }
									}

									setInputReferrerList([
										...inputReferrerList.filter((item) => { return item.id !== row.id }),
										{
											...row,
											editedValue: {
												amount: value,
												param: row.editedValue?.param,
												until: row.editedValue?.until || new Date(),
											}
										}
									])
								}}
								onBlur={() => {
									setInputReferrerList([
										...inputReferrerList.filter((item) => { return item.id !== row.id }),
										{
											...row,
											editedValue: {
												amount: new BigNumber(row.editedValue?.amount || 0).toFixed(2, BigNumber.ROUND_DOWN) || '',
												param: row.editedValue?.param,
												until: row.editedValue?.until || new Date(),
											}
										}
									])
								}}
							/>
						</div>
						<div className="col-sm-8 col-lg-3 mb-2">
							<ReactDatePicker
								className="input-control control-calendar"
								portalId="calendarParent"
								selected={row.editedValue?.until}
								onChange={(date) => {
									if ( !date ) { return null; }
									setInputReferrerList([
										...inputReferrerList.filter((item) => { return item.id !== row.id }),
										{
											...row,
											editedValue: {
												amount: row.editedValue?.amount || '',
												param: row.editedValue?.param,
												until: date,
											}
										}
									])
								}}
								showTimeSelect
								dateFormat="yyyy-MM-dd HH:mm"
								locale={'en-US'}
								shouldCloseOnSelect={false}
							/>
						</div>
						<div className="col-lg-4 mb-2">
							<input
								className="input-control"
								type="text"
								placeholder="PROMOCODE"
								value={row.editedValue?.param}
								onChange={(e) => {
									setInputReferrerList([
										...inputReferrerList.filter((item) => { return item.id !== row.id }),
										{
											...row,
											editedValue: {
												amount: row.editedValue?.amount || '',
												param: e.target.value,
												until: row.editedValue?.until || new Date(),
											}
										}
									])
								}}
							/>
                        </div>
						<div className="col-6 col-sm-2 mb-2">
							<button
								className="btn btn-primary w-100"
								disabled={ row.editedValue && new BigNumber(row.editedValue.amount).eq(0) }
								onClick={() => {
									setInputReferrerList([
										...inputReferrerList.filter((item) => { return item.id !== row.id }),
										{
											...row,
											isEdit: false,
											currentValue: {
												amount: row.editedValue?.amount || '',
												until: row.editedValue?.until || new Date(),
												param: row.editedValue?.param,
											}
										}
									])
								}}
							>
									Save
							</button>
						</div>
						<div className="col-6 col-sm-2 mb-2">
							<button
								className="btn btn-gray w-100"
								onClick={() => {
									setInputReferrerList([
										...inputReferrerList.filter((item) => { return item.id !== row.id }),
										{
											...row,
											isEdit: false,
											editedValue: undefined
										}
									])
								}}
							>
									Undo
							</button>
						</div>
					</div>
				</div>
			)
		} else {
			return (
				<div className="item item-narrow" key={ row.id }>
					<div className="row">
						<div className="col-sm-1 mb-2">
							<span>{ row.currentValue.amount.toString() }%</span>
						</div>
						<div className="col-sm-3 mb-2">
							<span className="text-break">
								until { dateToStr(row.currentValue.until) } { row.currentValue.until.getHours().toString().padStart(2, '0') }:{ row.currentValue.until.getMinutes().toString().padStart(2, '0') }
							</span>
						</div>
						<div className="col-sm-5 col-lg-6 mb-2">
							<span className="text-break">
								{ row.currentValue.param }
							</span>
						</div>
						<div className="col-sm-3 col-lg-2 mb-2 d-flex">
							<div className="btns-group mr-0 ml-auto">
								<div className="btn-group">
									<button
										className="btn btn-md btn-gray btn-img"
										onClick={() => {
											setInputReferrerList([
												...inputReferrerList.filter((item) => { return row.id !== item.id }),
												{
													...row,
													isEdit: true,
													editedValue: row.currentValue,
												}
											])
										}}
									>
										<img src={ i_edit } alt="" />
									</button>
								</div>
								<div className="btn-group">
									<button
										className="btn btn-md btn-gray btn-img"
										onClick={() => {
											setInputReferrerList(inputReferrerList.filter((item) => { return row.id !== item.id }))
										}}
									>
										<img src={ i_del } alt="" />
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			)
		}
	}
	const getReferralDiscountBlock = () => {
		return (
			<div className="lp-step-block">
				<h4>
					Referral links
					<TippyWrapper msg="Buyer discount for referral address. More than one referral discount can be active at the same time in the showcase" elClass="ml-1" />
				</h4>
				<div className="row">
					<div className="col-sm-6 col-md-2">
						<div className="input-group">
							<label className="input-label">%</label>
							<input
								className="input-control"
								type="text"
								placeholder="0%"
								value={ inputReferrerAmount }
								onChange={(e) => {
									let value = e.target.value.replaceAll(' ', '').replaceAll(',', '.');
									const valueParsed = new BigNumber(value);

									if ( value === '' ) {
										value = '';
									} else {
										if ( valueParsed.isNaN() ) { return; }
										if ( valueParsed.gt(100) ) { return; }
									}

									setInputReferrerAmount(value)
								}}
								onBlur={() => {
									if ( inputReferrerAmount === '' ) { return; }
									setInputReferrerAmount(new BigNumber(inputReferrerAmount).toFixed(2, BigNumber.ROUND_DOWN))
								}}
							/>
						</div>
					</div>
					<div className="col-sm-6 col-md-3">
						<div className="input-group">
							<label className="input-label">Active until</label>
							<ReactDatePicker
								className="input-control control-calendar"
								portalId="calendarParent"
								selected={inputReferrerUntil}
								onChange={(date) => {
									setInputReferrerUntil(date)
								}}
								showTimeSelect
								dateFormat="yyyy-MM-dd HH:mm"
								locale={'en-US'}
								shouldCloseOnSelect={false}
							/>
						</div>
					</div>
					<div className="col-md-5">
						<div className="input-group">
							<label className="input-label">Referrer address</label>
							<input
								className="input-control"
								type="text"
								placeholder=""
								value = { inputReferrerAddress }
								onChange={(e) => { setInputReferrerAddress(e.target.value.replaceAll(' ', '').replace(/[^a-fA-F0-9xX]/g, "")) }}
							/>
						</div>
					</div>
					<div className="col-md-2">
						<div className="input-group">
							<label className="input-label d-none d-md-block">&nbsp;</label>
							<button
								className="btn btn-outline w-100"
								type="submit"
								disabled={ inputReferrerAmount === '' || inputReferrerAddress === '' || inputReferrerUntil === null || new BigNumber(inputReferrerAmount).eq(0) }
								onClick={() => {
									if ( !inputReferrerUntil ) { return; }
									setInputReferrerAmount('');
									setInputReferrerAddress('');
									setInputReferrerUntil(null);
									setInputReferrerList([
										...inputReferrerList,
										{
											id: new Date().getTime(),
											isEdit: false,
											currentValue: {
												amount: inputReferrerAmount,
												param: inputReferrerAddress,
												until: inputReferrerUntil
											}
										}
									].sort((item, prev) => { return item.currentValue.until.getTime() - prev.currentValue.until.getTime() }))
								}}
							>Add</button>
						</div>
					</div>
				</div>
				<div className="c-wrap__table">
					{ inputReferrerList.map((item) => { return getReferrerDiscountRow(item) }) }
				</div>
			</div>
		)
	}

	const saveBtnDsabled = () => {

		if ( !!inputTimeToAdd.find((item) => { return item.isEdit }) ) { return true; }
		if ( !!inputTimeToEdit.find((item) => { return item.isEdit }) ) { return true; }
		if ( !!inputPromoList.find((item) => { return item.isEdit }) ) { return true; }
		if ( !!inputReferrerList.find((item) => { return item.isEdit }) ) { return true; }

		return false;
	}
	const updateSubmit = async () => {

		if ( !displayToEdit ) { return; }
		if ( !web3 ) { return; }

		const pageEdited = !!inputTimeToEdit.length || !!inputTimeToAdd.length || !!inputTimeToDel.length || !!inputPromoList.length || !!inputReferrerList.length;
		if ( !pageEdited ) {
			navigate(`/token/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.nameHash}`);
			return;
		}

		const loaderStages: Array<AdvancedLoaderStageType> = [];
		if ( inputTimeToAdd.length || inputTimeToEdit.length || inputTimeToDel.length ) {
			loaderStages.push({
				id: 'time_add',
				sortOrder: 2,
				text: `Updating time discounts`,
				status: _AdvancedLoadingStatus.queued
			});
		}
		if ( inputPromoList.length ) {
			loaderStages.push({
				id: 'promo',
				sortOrder: 10,
				text: `Updating promocode discounts`,
				total: inputPromoList.length,
				current: 0,
				status: _AdvancedLoadingStatus.queued
			});
		}
		if ( inputReferrerList.length ) {
			loaderStages.push({
				id: 'referrer',
				sortOrder: 20,
				text: `Updating refferer discounts`,
				total: inputReferrerList.length,
				current: 0,
				status: _AdvancedLoadingStatus.queued
			});
		}
		const advLoader = {
			title: 'Waiting to update',
			stages: loaderStages
		};
		createAdvancedLoader(advLoader);

		// ---------- TIME ----------
		if ( inputTimeToAdd.length || inputTimeToEdit.length || inputTimeToDel.length ) {
			updateStepAdvancedLoader({
				id: 'time_add',
				status: _AdvancedLoadingStatus.loading
			});

			let timeDiscountsParsed: Array<DiscountUntil> = [
				...inputTimeSaved,
				...inputTimeToEdit,
				...inputTimeToAdd,
			]
				.sort((item, prev) => {
					return item.currentValue.until.getTime() - prev.currentValue.until.getTime()
				})
				.map((item) => {
					return {
						discount: {
							dsctType: _DiscountType.TIME,
							dsctPercent: new BigNumber(item.currentValue.amount),
						},
						untilDate: dateToUnixtime(item.currentValue.until)
					}
				})

			if ( !timeDiscountsParsed.length ) {
				timeDiscountsParsed = [
					{
						discount: {
							dsctType: _DiscountType.TIME,
							dsctPercent: new BigNumber(0),
						},
						untilDate: new BigNumber(0),
					}
				]
			}

			try {
				await setTimeDiscountInChain(
					web3,
					displayToEdit.priceModel,
					displayToEdit.nameHash,
					timeDiscountsParsed,
				)
			} catch(e: any) {
				setModal({
					type: _ModalTypes.error,
					title: `Cannot add time discounts`,
					details: [
						`User address: ${userAddress}`,
						`Price model: ${displayToEdit.priceModel}`,
						`Showcase hash: ${displayToEdit.nameHash}`,
						`Price: ${JSON.stringify(timeDiscountsParsed)}`,
						'',
						e.message || e,
					]
				});
				return;
			}

			updateStepAdvancedLoader({
				id: 'time_add',
				status: _AdvancedLoadingStatus.complete
			});
		}
		// ---------- END TIME ----------

		// ---------- PROMOCODE ----------
		for ( const idx in inputPromoList ) {
			const item = inputPromoList[idx];

			updateStepAdvancedLoader({
				id: 'promo',
				current: parseInt(idx) + 1,
				status: _AdvancedLoadingStatus.loading
			});

			const discountParsed: DiscountUntil = {
				discount: {
					dsctType: _DiscountType.PROMO,
					dsctPercent: new BigNumber(item.currentValue.amount)
				},
				untilDate: dateToUnixtime(item.currentValue.until)
			}
			try {
				await setPromocodeDiscountInChain(
					web3,
					displayToEdit.priceModel,
					displayToEdit.nameHash,
					item.currentValue.param || '',
					discountParsed,
				)
			} catch(e: any) {
				setModal({
					type: _ModalTypes.error,
					text: [{ text: `Cannot update promocode discounts: ${item.currentValue.param}, ${e || e.message}` }],
				});
				return;
			}
		}

		updateStepAdvancedLoader({
			id: 'promo',
			total: inputPromoList.length,
			current: inputPromoList.length,
			status: _AdvancedLoadingStatus.complete
		});
		// ---------- END PROMOCODE ----------

		// ---------- REFERRER ----------
		for ( const idx in inputReferrerList ) {
			const item = inputReferrerList[idx];

			updateStepAdvancedLoader({
				id: 'referrer',
				total: inputReferrerList.length,
				current: parseInt(idx) + 1,
				status: _AdvancedLoadingStatus.loading
			});

			const discountParsed: DiscountUntil = {
				discount: {
					dsctType: _DiscountType.REFERRAL,
					dsctPercent: new BigNumber(item.currentValue.amount)
				},
				untilDate: dateToUnixtime(item.currentValue.until)
			}
			try {
				await setReferrerDiscountInChain(
					web3,
					displayToEdit.priceModel,
					displayToEdit.nameHash,
					item.currentValue.param || '',
					discountParsed,
				)
			} catch(e: any) {
				setModal({
					type: _ModalTypes.error,
					text: [{ text: `Cannot update referrer discounts: ${item.currentValue.param}, ${e || e.message}` }],
				});
				return;
			}
		}

		updateStepAdvancedLoader({
			id: 'referrer',
			total: inputReferrerList.length,
			current: inputReferrerList.length,
			status: _AdvancedLoadingStatus.complete
		});
		// ---------- END REFERRER ----------

		setModal({
			type: _ModalTypes.success,
			title: 'Showcase discounts successfully updated',
			text: currentChain?.hasOracle ? [
				{ text: 'After oracle synchronization, your changes will appear in launchpad admin panel soon. Please refresh the launchpads list' },
			] : [],
			buttons: [{
				text: 'Ok',
				clickFunc: () => {
					unsetModal();
					navigate(`/token/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.nameHash}`);
					props.onUpdate();
				}
			}],
			// links: [{
			// 	text: `View showcase`,
			// 	url: `/launchpad/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.name}`
			// }]
		});

	}

	const getBackBtn = () => {
		const pageEdited = !!inputTimeToEdit.length || !!inputTimeToAdd.length || !!inputPromoList.length || !!inputReferrerList.length;
		if ( pageEdited ) {
			return (
				<button
					className="btn btn-gray"
					onClick={() => {
						setModal({
							type: _ModalTypes.info,
							title: 'You have unsaved data. Continue?',
							buttons: [
								{
									text: 'Yes',
									clickFunc: () => {
										navigate(`/`);
										unsetModal();
									},
								},
								{
									text: 'No',
									clickFunc: () => { unsetModal(); },
									clazz: 'btn-grad'
								},
							]
						})
					}}
				>Back</button>
			)
		}

		if ( !displayToEdit ) {
			return (
				<Link
					className="btn btn-gray"
					to={ `/` }
				>Back</Link>
			)
		}

		return (
			<Link
				className="btn btn-gray"
				to={ `/price/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.nameHash}` }
			>Back</Link>
		)
	}

	return (
		<div className="container">

			<div className="breadcrumbs">
				<Link className="item-back btn btn-sm btn-gray" to="/">
					<svg className="mr-2" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M12 5.25H2.8725L7.065 1.0575L6 0L0 6L6 12L7.0575 10.9425L2.8725 6.75H12V5.25Z" fill="white"></path>
					</svg>
					<span>To the showcase list</span>
				</Link>
			</div>

			<div className="row mb-4">
				<div className="col-12 col-sm">
					<div className="h3 mb-3 mt-0">{ displayToEdit?.title || '<Untitled>' }</div>
				</div>
				{
					displayToEdit ? (
						<div className="col-12 col-sm-auto">
							<div className="btns-group">
								<div className="btn-group">
									<a
										className="btn btn-md btn-gray"
										href={ `/launchpad/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.name}` }
										target="_blank" rel="noopener noreferrer"
									>Go to the showcase</a>
								</div>
								<div className="btn-group">
								<CopyToClipboard
									text={ `${window.location.origin}/launchpad/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.name}` }
									onCopy={() => {
										setDisplayLinkCopied(true);
										setTimeout(() => { setDisplayLinkCopied(false); }, 5*1000);
									}}
								>
									<button className="btn btn-md btn-gray btn-img">
										<img src= { i_copy } alt="" />
										<span className="btn-action-info" style={{ display: displayLinkCopied ? 'block' : 'none' }}>Copied</span>
									</button>
								</CopyToClipboard>
								</div>
							</div>
						</div>
					) : null
				}
			</div>
			<div className="row">
				<div className="col-md-2 col-lg-2">
					<StepsProgress current="discount" display={ displayToEdit } confirmClose={ !!inputTimeToEdit.length || !!inputTimeToAdd.length || !!inputPromoList.length || !!inputReferrerList.length } />
				</div>
				<div className="col-md-10 col-lg-10" id="calendarParent">
					<div className="lp-wrap">
						<div className="lp-wrap__header">
							<div className="h3-wrap">Discount rules</div>
						</div>
						<div className="row">
							<div className="col-lg-10">
								<p className="mt-0">The list of&nbsp;showcase discounts depends on&nbsp;the selected showcase type . Discounts can be&nbsp;related to&nbsp;time, promo codes, referral discounts. Several types of&nbsp;discounts can be&nbsp;active at&nbsp;one time in&nbsp;the showcase and can be&nbsp;cumulative for NFT or&nbsp;wNFT. Showcase owner can have several promo codes, referral discounts, several time periods each with its own discount. </p>
							</div>
						</div>

						{ getTimeDiscountBlock() }
						{/* { getPromocodeDiscountBlock() }
						{ getReferralDiscountBlock() } */}

						<div className="lp-steps-footer">
							<div className="row justify-content-between">
								<div className="col-auto">
									{ getBackBtn() }
								</div>
								<div className="col-auto">
									<button
										className="btn btn-grad"
										disabled={ saveBtnDsabled() }
										onClick={ updateSubmit }
									>Save and proceed</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}