import {
	useContext,
	useEffect,
	useState
} from "react"
import {
	Link,
	useNavigate
} from "react-router-dom";
import { CopyToClipboard }      from 'react-copy-to-clipboard';
import DatePicker               from "react-datepicker";
import StepsProgress            from "../../StepsProgress";
import TippyWrapper             from "../../TippyWrapper"
import InputWithOptions         from "../../InputWithOptions";
import {
	updateDisplayInChain,
	updateDisplayInAPI
} from "../../../models";

import {
	BigNumber,
	getDisplayFromAPI,
	getStrHash,
	_Display,
	waitUntilAsync,
	compactString,
	dateToUnixtime,
	unixtimeToDate,
	dateToStrWithMonth,
	Display,
	addDays,
	getDisplayFromChain,
} from "@envelop/envelop-client-core";

import "react-datepicker/dist/react-datepicker.css";

import i_copy from "../../../static/pics/icons/i-copy.svg";
import {
	AdvancedLoaderStageType,
	InfoModalContext,
	SubscriptionContext,
	SubscriptionRenderer,
	Web3Context,
	_AdvancedLoadingStatus,
	_ModalTypes
} from "../../../dispatchers";

import config from '../../../app.config.json';

export type PriceModelListItem = {
	value: string,
	label: string,
}

type ShowcaseStepPageProps = {
	displayToEdit   : Display | undefined,
	onUpdate        : () => void,
	kioskAddress    : string,
}

export default function ShowcaseStepPage(props: ShowcaseStepPageProps) {

	const [ formLocked, setFormLocked ] = useState(true);

	const { displayToEdit } = props;

	let defaultStartDate = new Date(new Date());
	defaultStartDate.setHours(0,0,0);
	let defaultEndDate = new Date();
	defaultEndDate.setMonth(defaultEndDate.getMonth() + 1)
	defaultEndDate.setHours(0,0,0);
	let defaultEndDateChecked = false;

	if ( displayToEdit ) {
		defaultStartDate = unixtimeToDate(new BigNumber(displayToEdit?.enableAfter ).multipliedBy(1000));
		defaultEndDate = unixtimeToDate(new BigNumber(displayToEdit?.disableAfter).multipliedBy(1000));
		if ( defaultEndDate.getTime() > 2145887000000 ) {
			defaultEndDateChecked = false;
		} else {
			defaultEndDateChecked = true;
		}
		if ( formLocked ) { setFormLocked(false); }
	}

	const [ inputTitle         , setInputTitle          ] = useState(displayToEdit?.title || '');
	const [ inputUrlTitle      , setInputUrlTitle       ] = useState(displayToEdit?.name || '');
	const [ urlExists          , setUrlExists           ] = useState(false);
	const [ inputDescription   , setInputDescription    ] = useState(displayToEdit?.description || '');
	const [ priceModelsList    , setPriceModelsList     ] = useState<Array<PriceModelListItem>>([]);
	const [ inputPriceModel    , setInputPriceModel     ] = useState('');
	const [ inputStartDate     , setInputStartDate      ] = useState<Date>(defaultStartDate);
	const [ inputEndDateChecked, setInputEndDateChecked ] = useState<boolean>(defaultEndDateChecked);
	const [ inputEndDate       , setInputEndDate        ] = useState<Date>(defaultEndDate);

	const [ displayLinkCopied  , setDisplayLinkCopied   ] = useState(false);

	const [ hasChanges         , setHasChanges          ] = useState<Array<'api' | 'chain'>>([]); // 'api' for api update; 'chain' for create chain tx

	const {
		web3,
		userAddress,
		currentChain,
		currentChainId,
	} = useContext(Web3Context);
	const {
		createAdvancedLoader,
		setModal,
		updateStepAdvancedLoader,
		unsetModal,
	} = useContext(InfoModalContext);
	const {
		subscriptionRemainings,
		subscriptionServiceExist,
	} = useContext(SubscriptionContext);

	useEffect(() => {
		if ( !currentChain ) { return; }

		const foundChainConfig: any = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === currentChain.chainId });
		if ( !foundChainConfig ) { return; }
		if ( !foundChainConfig.priceModels || !foundChainConfig.priceModels.length ) {
			setPriceModelsList([]);
			setInputPriceModel('');
			return;
		}

		const priceModelsListParsed = foundChainConfig.priceModels.map((item: { contractAddress: string, label: string }) => {
			return { value: item.contractAddress, label: `${item.label} ${compactString(item.contractAddress)}` }
		})
		setPriceModelsList(priceModelsListParsed);
		if ( inputPriceModel === '' ) { setInputPriceModel(priceModelsListParsed[0].value) }

	}, [ currentChain ])

	useEffect(() => {
		if ( displayToEdit ) {
			setInputTitle(displayToEdit?.title || '');
			setInputUrlTitle(displayToEdit?.name || '');
			setInputDescription(displayToEdit?.description || '');
			setInputStartDate(unixtimeToDate(new BigNumber(displayToEdit?.enableAfter)));
			setInputEndDate(unixtimeToDate(new BigNumber(displayToEdit?.disableAfter)));
			if ( unixtimeToDate(new BigNumber(displayToEdit?.disableAfter)).getTime() > 2145887000000 ) {
				setInputEndDateChecked(false);
			} else {
				setInputEndDateChecked(true);
			}
			if ( formLocked ) { setFormLocked(false); }
		}
	}, [ displayToEdit, formLocked ])
	useEffect(() => {
		if ( !subscriptionServiceExist ) { setFormLocked(false); return; }

		if ( !subscriptionRemainings ) { setFormLocked(true); return; }

		if ( subscriptionRemainings.countsLeft.gt(0) ) { setFormLocked(false); }

		const now = new BigNumber(new Date().getTime()).dividedToIntegerBy(1000);
		if ( subscriptionRemainings.validUntil.gt(now) ) {
			setFormLocked(false);
		}

	}, [ subscriptionRemainings, subscriptionServiceExist ])

	const navigate = useNavigate();

	const createDisplay = async () => {

		if ( !web3 ) { return; }
		if ( !userAddress ) { return; }
		if ( !currentChain ) { return; }

		const loaderStages: Array<AdvancedLoaderStageType> = [
			{
				id: 'chain',
				sortOrder: 1,
				text: `Creating showcase in contract`,
				status: _AdvancedLoadingStatus.loading
			},
			{
				id: 'wait',
				sortOrder: 2,
				text: `Waiting response from oracle`,
				status: _AdvancedLoadingStatus.queued
			},
			{
				id: 'sign',
				sortOrder: 3,
				text: `Signing message for oracle`,
				status: _AdvancedLoadingStatus.queued
			},
			{
				id: 'api',
				sortOrder: 4,
				text: `Updating params in oracle`,
				status: _AdvancedLoadingStatus.queued
			},
		];
		const advLoader = {
			title: 'Waiting to create',
			stages: loaderStages
		};
		createAdvancedLoader(advLoader);

		let txResp;

		try {
			txResp = await updateDisplayInChain(
				web3,
				props.kioskAddress,
				inputUrlTitle,
				userAddress,
				dateToUnixtime(inputStartDate),
				inputEndDateChecked ? dateToUnixtime(inputEndDate) : new BigNumber('2145887999'),
				inputPriceModel
			)
		} catch(e: any) {
			setModal({
				type: _ModalTypes.error,
				title: `Cannot create showcase in contract`,
				details: [
					`User address: ${userAddress}`,
					`Showcase hash: ${getStrHash(inputUrlTitle)}`,
					'',
					e.message || e,
				]
			});
			return;
		}

		updateStepAdvancedLoader({
			id: 'chain',
			status: _AdvancedLoadingStatus.complete
		});
		updateStepAdvancedLoader({
			id: 'wait',
			status: _AdvancedLoadingStatus.loading
		});

		try {
			await waitUntilAsync(
				async () => {
					let r;
					try{
						r = await getDisplayFromAPI(
							currentChain.chainId,
							props.kioskAddress,
							getStrHash(inputUrlTitle)
						);
					} catch( ignored ) {}

					if ( r !== undefined ) { return true; }

					return false;
				},
				10*1000,
				12
			);
		} catch(e: any) {
			setModal({
				type: _ModalTypes.error,
				title: `Cannot fetch showcase from oracle. Try to find it on dashboard`,
				details: [
					`User address: ${userAddress}`,
					`Showcase hash: ${getStrHash(inputUrlTitle)}`,
					'',
					e.message || e,
				],
				buttons: [{
					text: 'Go to dashboard',
					clickFunc: () => {
						unsetModal();
						navigate(`/`);
					}
				}],
			});
			return;
		}
		updateStepAdvancedLoader({
			id: 'wait',
			status: _AdvancedLoadingStatus.complete
		});
		updateStepAdvancedLoader({
			id: 'sign',
			status: _AdvancedLoadingStatus.loading
		});

		let sign;
		try {
			sign = await web3.eth.personal.sign(`Showcase: ${getStrHash(inputUrlTitle)}`, userAddress, '');
		} catch(e: any) {
			setModal({
				type: _ModalTypes.error,
				title: `Cannot sign message`,
				details: [
					`User address: ${userAddress}`,
					`Showcase hash: ${getStrHash(inputUrlTitle)}`,
					'',
					e.message || e,
				]
			});
			return;
		}

		updateStepAdvancedLoader({
			id: 'sign',
			status: _AdvancedLoadingStatus.complete
		});
		updateStepAdvancedLoader({
			id: 'api',
			status: _AdvancedLoadingStatus.loading
		});

		try {
			await updateDisplayInAPI(
				web3,
				props.kioskAddress,
				getStrHash(inputUrlTitle),
				sign,
				{
					title: inputTitle,
					description: inputDescription,
					show_widget: true,
				}
			)
		} catch(e: any) {
			setModal({
				type: _ModalTypes.error,
				title: `Cannot update showcase in oracle`,
				details: [
					`User address: ${userAddress}`,
					`Showcase hash: ${getStrHash(inputUrlTitle)}`,
					'',
					e.message || e,
				]
			});
			return;
		}

		setModal({
			type: _ModalTypes.success,
			text: [{ text: 'Showcase successfully created' }],
			buttons: [{
				text: 'Ok',
				clickFunc: () => {
					unsetModal();
					// navigate(`/project/${currentChain.chainId}/${props.kioskAddress}/${getStrHash(inputUrlTitle)}`);
					navigate(`/price/${currentChain.chainId}/${props.kioskAddress}/${getStrHash(inputUrlTitle)}`);
					props.onUpdate();
				}
			}],
			links: [
				{
					text: `View on ${currentChain.explorerName || ''}`,
					url: `${currentChain.explorerBaseUrl || ''}/tx/${txResp?.transactionHash || ''}`
				},
				// {
				// 	text: `View showcase`,
				// 	url: `/launchpad/${currentChain.chainId}/${props.kioskAddress}/${inputUrlTitle}`
				// }
			]
		});

	}
	const updateDisplay = async () => {

		if ( !displayToEdit ) { return; }
		if ( !hasChanges.length ) {
			// navigate(`/project/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.nameHash}`);
			navigate(`/price/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.nameHash}`);
			return;
		}

		if ( !web3 ) { return; }
		if ( !userAddress ) { return; }
		if ( !currentChain ) { return; }

		const loaderStages: Array<AdvancedLoaderStageType> = [];
		if ( hasChanges.includes('chain') ) {
			loaderStages.push({
				id: 'chain',
				sortOrder: 1,
				text: `Updating showcase in contract`,
				status: _AdvancedLoadingStatus.loading
			});
		}
		if ( hasChanges.includes('api') ) {
			loaderStages.push({
				id: 'sign',
				sortOrder: 2,
				text: `Signing message for oracle`,
				status: _AdvancedLoadingStatus.queued
			});
			loaderStages.push({
				id: 'api',
				sortOrder: 3,
				text: `Updating showcase in oracle`,
				status: _AdvancedLoadingStatus.queued
			});
		}

		const advLoader = {
			title: 'Waiting to update',
			stages: loaderStages
		};
		createAdvancedLoader(advLoader);

		if ( hasChanges.includes('chain') ) {
			try {
				await updateDisplayInChain(
					web3,
					props.kioskAddress,
					inputUrlTitle,
					userAddress,
					dateToUnixtime(inputStartDate),
					inputEndDateChecked ? dateToUnixtime(inputEndDate) : new BigNumber('2145887999'),
					inputPriceModel,
				)
			} catch(e: any) {
				setModal({
					type: _ModalTypes.error,
					title: `Cannot update showcase in contract`,
					details: [
						`User address: ${userAddress}`,
						`Showcase hash: ${getStrHash(inputUrlTitle)}`,
						'',
						e.message || e,
					]
				});
				return;
			}

			updateStepAdvancedLoader({
				id: 'chain',
				status: _AdvancedLoadingStatus.complete
			});
		}

		if ( hasChanges.includes('api') ) {
			updateStepAdvancedLoader({
				id: 'sign',
				status: _AdvancedLoadingStatus.loading
			});
			let sign;
			try {
				sign = await web3.eth.personal.sign(`Showcase: ${getStrHash(inputUrlTitle)}`, userAddress, '');
			} catch(e: any) {
				setModal({
					type: _ModalTypes.error,
					title: `Cannot sign message`,
					details: [
						`User address: ${userAddress}`,
						`Showcase hash: ${getStrHash(inputUrlTitle)}`,
						'',
						e.message || e,
					]
				});
				return;
			}

			updateStepAdvancedLoader({
				id: 'sign',
				status: _AdvancedLoadingStatus.complete
			});
			updateStepAdvancedLoader({
				id: 'api',
				status: _AdvancedLoadingStatus.loading
			});

			try {
				await updateDisplayInAPI(
					web3,
					props.kioskAddress,
					getStrHash(inputUrlTitle),
					sign,
					{
						title: inputTitle,
						description: inputDescription,
						show_widget: true,
					}
				)
			} catch(e: any) {
				setModal({
					type: _ModalTypes.error,
					title: `Cannot update showcase in oracle`,
					details: [
						`User address: ${userAddress}`,
						`Showcase hash: ${getStrHash(inputUrlTitle)}`,
						'',
						e.message || e,
					]
				});
				return;
			}
		}

		setTimeout(() => {
			setModal({
				type: _ModalTypes.info,
				text: [{ text: 'Showcase successfully updated' }],
				buttons: [{
				text: 'Ok',
					clickFunc: () => {
						unsetModal();
						// navigate(`/project/${currentChain.chainId}/${props.kioskAddress}/${getStrHash(inputUrlTitle)}`);
						navigate(`/price/${currentChain.chainId}/${props.kioskAddress}/${getStrHash(inputUrlTitle)}`);
						props.onUpdate();
					}
				}],
				// links: [{
				// 	text: `View showcase`,
				// 	url: `/launchpad/${currentChain.chainId}/${props.kioskAddress}/${inputUrlTitle}`
				// }]
			});
		}, 1000);
	}

	const displaySubmit = async () => {
		if ( displayToEdit ) {
			updateDisplay();
		} else {
			createDisplay();
		}
	}

	const saveBtnDsabled = () => {
		if ( formLocked ) { return true; }

		if ( !displayToEdit && inputUrlTitle === '' ) { return true; }

		if ( !displayToEdit && urlExists ) { return true; }

		return false;
	}

	const getBackBtn = () => {
		if ( hasChanges ) {
			return (
				<button
					className="btn btn-gray"
					onClick={() => {
						setModal({
							type: _ModalTypes.info,
							title: 'You have unsaved data. Continue?',
							buttons: [
								{
									text: 'Yes',
									clickFunc: () => {
										navigate(`/`);
										unsetModal();
									},
								},
								{
									text: 'No',
									clickFunc: () => { unsetModal(); },
									clazz: 'btn-grad'
								},
							]
						})
					}}
				>Back</button>
			)
		}

		return (
			<Link
				className="btn btn-gray"
				to={ `/` }
			>Back</Link>
		)
	}

	return (
		<div className="container">

			<div className="breadcrumbs">
				<Link className="item-back btn btn-sm btn-gray" to="/">
					<svg className="mr-2" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M12 5.25H2.8725L7.065 1.0575L6 0L0 6L6 12L7.0575 10.9425L2.8725 6.75H12V5.25Z" fill="white"></path>
					</svg>
					<span>To the showcase list</span>
				</Link>
			</div>

			{
				!displayToEdit ? (
					<SubscriptionRenderer />
				) : null
			}

			<div className="row mb-4">
				<div className="col-12 col-sm">
				{
					!displayToEdit ? (
						<div className="h3 mb-3 mt-0">Creating a new showcase</div>
					) : (
						<div className="h3 mb-3 mt-0">{ displayToEdit?.title || '<Untitled>' }</div>
					)
				}
				</div>
				{
					displayToEdit ? (
						<div className="col-12 col-sm-auto">
							<div className="btns-group">
								<div className="btn-group">
									<a
										className="btn btn-md btn-gray"
										href={ `/launchpad/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.name}` }
										target="_blank" rel="noopener noreferrer"
									>Go to the showcase</a>
								</div>
								<div className="btn-group">
								<CopyToClipboard
									text={ `${window.location.origin}/launchpad/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.name}` }
									onCopy={() => {
										setDisplayLinkCopied(true);
										setTimeout(() => { setDisplayLinkCopied(false); }, 5*1000);
									}}
								>
									<button className="btn btn-md btn-gray btn-img">
										<img src= { i_copy } alt="" />
										<span className="btn-action-info" style={{ display: displayLinkCopied ? 'block' : 'none' }}>Copied</span>
									</button>
								</CopyToClipboard>
								</div>
							</div>
						</div>
					) : null
				}
			</div>
			<div className="row">
				<div className="col-md-2 col-lg-2">
					<StepsProgress
						current="showcase"
						display={ displayToEdit }
						confirmClose={ !!hasChanges.length }
					/>
				</div>
				<div className="col-md-10 col-lg-10" id="calendarParent">
					<div className="lp-wrap">
						<div className="lp-wrap__header">
							<div className="h3-wrap">Showcase</div>
						</div>
						<div className="row">
							<div className="col-lg-8">
							<div className="input-group">
								<label className="input-label">Title</label>
								<input
									className="input-control"
									type="text"
									placeholder=""
									disabled={ formLocked }
									value={ inputTitle }
									onChange={(e) => {
										setInputTitle(e.target.value);
										setHasChanges([ ...hasChanges.filter((item) => { return item !== 'api' }), 'api' ]);
									}}
								/>
							</div>
							<div className="input-group">
								<label className="input-label">Description</label>
								<textarea
									className="input-control"
									rows={ 4 }
									disabled={ formLocked }
									value={ inputDescription }
									onChange={(e) => {
										setInputDescription(e.target.value);
										setHasChanges([ ...hasChanges.filter((item) => { return item !== 'api' }), 'api' ]);
									}}
								> </textarea>
							</div>
							</div>
						</div>
						<div className="row">
							<div className="col-lg-8">
							<div className="input-group">
								<label className="input-label">
									URL title
									<TippyWrapper msg="A unique one-word showcase name that is used as part of the browser URL" elClass="ml-1" />
								</label>
								<input
									className="input-control"
									type="text"
									placeholder=""
									disabled={ formLocked || !!displayToEdit }
									value={ inputUrlTitle }
									onChange={(e) => {
										setUrlExists(false);
										setInputUrlTitle(e.target.value.replaceAll(' ', '').replace(/[^a-zA-Z0-9\-]/g, ""));
										setHasChanges([ ...hasChanges.filter((item) => { return item !== 'chain' }), 'chain' ]);
									}}
									onBlur={() => {
										if ( displayToEdit ) { return; }
										if ( inputUrlTitle === '' ) { return; }

										getDisplayFromChain(currentChainId, props.kioskAddress, getStrHash(inputUrlTitle))
											.then((data) => {
												if ( !data ) { return; }
												if (
													data.owner === '0x0000000000000000000000000000000000000000' &&
													data.priceModel === '0x0000000000000000000000000000000000000000'
												) {
													setUrlExists(false);
												} else {
													setUrlExists(true);
												}
											})
									}}
								/>
								{
									inputUrlTitle === '' ? ( <div className="input-error">Required field</div> ) : null
								}
								{
									urlExists ? ( <div className="input-error">Launchpad with this url already exists</div> ) : null
								}
							</div>
							<div className="input-group">
								<label className="input-label">Type</label>
								<InputWithOptions
									blockClass="input-control"
									placeholder=""
									value={ inputPriceModel }
									disabled={ formLocked }
									onSelect={(item) => {
										setInputPriceModel(item.value);
										setHasChanges([ ...hasChanges.filter((item) => { return item !== 'chain' }), 'chain' ]);
									}}
									options={priceModelsList}
								/>
								<div className="text-muted mt-3"><b>Basic Showcase type</b> allows you to create time-based and referral discounts, promo codes, default prices for NFT and for the ERC20 token in wNFT  </div>
							</div>
							</div>
						</div>
						<div className="row">
							<div className="col-sm-6 col-lg-4">
								<div className="input-group">
									<label className="input-label">Showcase life time</label>
									<DatePicker
										className="input-control control-calendar"
										portalId="calendarParent"
										disabled={ formLocked }
										selected={inputStartDate}
										onChange={(date) => {
											setInputStartDate(date || new Date());
											setHasChanges([ ...hasChanges.filter((item) => { return item !== 'chain' }), 'chain' ]);
										}}
										showTimeSelect
										dateFormat="dd.MM.yyyy HH:mm"
										locale={'en-US'}
										shouldCloseOnSelect={false}

									/>
								</div>
							</div>

							<div className="col-sm-6 col-lg-4">
								<div className="input-group">
									<div className="input-label">
										<label className="checkbox">
											<input
												type="checkbox"
												name="price-type"
												disabled={ formLocked }
												checked={ inputEndDateChecked }
												onChange={() => {
													setInputEndDateChecked(!inputEndDateChecked);
													setHasChanges([ ...hasChanges.filter((item) => { return item !== 'chain' }), 'chain' ]);
												}}
											/>
											<span className="check"> </span>
											<span className="check-text">Set end date</span>
										</label>
									</div>
									<DatePicker
										className="input-control control-calendar"
										portalId="calendarParent"
										disabled={ formLocked || !inputEndDateChecked }
										selected={ inputEndDateChecked ? inputEndDate : null }
										onChange={(date) => {
											setInputEndDate(date || new Date());
											setHasChanges([ ...hasChanges.filter((item) => { return item !== 'chain' }), 'chain' ]);
										}}
										showTimeSelect
										dateFormat="dd.MM.yyyy HH:mm"
										locale={'en-US'}
										shouldCloseOnSelect={false}
									/>
								</div>
							</div>
							{
								inputEndDateChecked ?
								inputEndDate.getTime() < inputStartDate.getTime() ? (
										<div className="col-12">
											<div className="mt-3 input-error">End date cannot be less then start</div>
										</div>
									) : (
										<div className="col-12">
											<div className="mt-3 text-orange">Available from { dateToStrWithMonth(unixtimeToDate(new BigNumber(inputStartDate.getTime()).dividedToIntegerBy(1000))) } to { inputEndDate ? dateToStrWithMonth(unixtimeToDate(new BigNumber(inputEndDate.getTime()).dividedToIntegerBy(1000))) : '' }</div>
										</div>
									)
								: null
							}
						</div>
						<div className="lp-steps-footer">
							<div className="row justify-content-between">
							<div className="col-auto">
								{ getBackBtn() }
							</div>
							<div className="col-auto">
								<button
									className="btn btn-grad"
									disabled={ saveBtnDsabled() }
									onClick={ displaySubmit }
								>Save and proceed</button>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}