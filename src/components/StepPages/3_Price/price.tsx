
import React, {
	useContext,
	useEffect,
	useState
} from "react";
import {
	Link,
	useNavigate
} from "react-router-dom";
import { CopyToClipboard } from 'react-copy-to-clipboard';
import StepsProgress       from "../../StepsProgress";
import TippyWrapper        from "../../TippyWrapper"
import CoinSelector        from "../../CoinSelector";

import {
	BigNumber,
	ERC20Type,
	Web3,
	_AssetType,
	_Display,
	addThousandSeparator,
	compactString,
	connect,
	removeThousandSeparator,
	tokenToFloat,
	tokenToInt,
	Price,
	getUserAddress,
	_DenominatedPrice,
	rationalize,
	Display,
	derationalize,
	groupBy,
	DenominatedPrice,
} from "@envelop/envelop-client-core";

import default_coin from "@envelop/envelop-client-core/static/pics/coins/_default.svg";
import i_edit       from "../../../static/pics/icons/i-edit.svg";
import i_del        from "../../../static/pics/icons/i-del.svg";
import i_copy       from "../../../static/pics/icons/i-copy.svg";
import {
	setDefaultPriceInChain,
	setCollateralPriceInChain,
	getDisplayCollateralUnitPricesChain,
	getDisplayDefaultPricesChain
} from "../../../models";
import {
	AdvancedLoaderStageType,
	ERC20Context,
	InfoModalContext,
	Web3Context,
	_AdvancedLoadingStatus,
	 _ModalTypes
} from "../../../dispatchers";

import { PriceModelListItem } from "../1_Showcase/showcase";

import config from '../../../app.config.json';

type DefaultEditableField = {
	currentValue: {
		token     : string,
		amount    : string,
	},
	editedValue?: {
		token     : string,
		amount    : string,
	},
	isEdit: boolean,
	id: number,
	idx?: number,
}
type CollateralEditableField = {
	currentValue: {
		token     : string,
		amount    : string,
	},
	editedValue?: {
		token     : string,
		amount    : string,
	},
	collateralToken: string,
	idx: number | undefined,
	source: 'saved' | 'edited' | 'added',
	isEdit: boolean,
	id: number,
}

type PriceStepPageProps = {
	onUpdate     : () => void,
	displayToEdit: Display | undefined,
}

export default function PriceStepPage(props: PriceStepPageProps) {

	const { displayToEdit } = props;
	const navigate          = useNavigate();

	const {
		setModal,
		unsetModal,
		createAdvancedLoader,
		updateStepAdvancedLoader,
	} = useContext(InfoModalContext);
	const {
		userAddress,
		currentChain,
		currentChainId,
	} = useContext(Web3Context);
	const {
		erc20List,
		requestERC20Token,
	} = useContext(ERC20Context);

	const [ inputDefaultPriceToken    , setInputDefaultPriceToken     ] = useState('');
	const [ inputDefaultPriceAmount   , setInputDefaultPriceAmount    ] = useState('');

	const [ defaultPriceToAdd         , setDefaultPriceToAdd          ] = useState<Array<DefaultEditableField>>([]);
	const [ defaultPriceSaved         , setDefaultPriceSaved          ] = useState<Array<DefaultEditableField>>([]);
	const [ defaultPriceToEdit        , setDefaultPriceToEdit         ] = useState<Array<DefaultEditableField>>([]);
	const [ defaultPriceToDelete      , setDefaultPriceToDelete       ] = useState<Array<DefaultEditableField>>([]);

	const [ inputCollateralToken      , setInputCollateralToken       ] = useState('');
	const [ inputCollateralPriceToken , setInputCollateralPriceToken  ] = useState('');
	const [ inputCollateralPriceAmount, setInputCollateralPriceAmount ] = useState('');
	const [ collateralPriceToAdd      , setCollateralPriceToAdd       ] = useState<Array<CollateralEditableField>>([]);
	const [ collateralPriceSaved      , setCollateralPriceSaved       ] = useState<Array<CollateralEditableField>>([]);
	const [ collateralPriceToEdit     , setCollateralPriceToEdit      ] = useState<Array<CollateralEditableField>>([]);
	const [ collateralPriceToDelete   , setCollateralPriceToDelete    ] = useState<Array<CollateralEditableField>>([]);

	const [ displayLinkCopied         , setDisplayLinkCopied          ] = useState(false);

	const [ priceModelsList , setPriceModelsList  ] = useState<Array<PriceModelListItem>>([]);

	useEffect(() => {
		const getDefaultPrices = async () => {
			if ( displayToEdit ) {
				setDefaultPriceToAdd([]);
				setDefaultPriceToEdit([]);
				setDefaultPriceToDelete([]);
				// setDefaultPriceSaved(displayToEdit.defaultPrices.map((item, idx) => {
				// 	return {
				// 		id: new Date().getTime() + idx,
				// 		isEdit: false,
				// 		currentValue: {
				// 			amount: tokenToFloat(item.amount).toString(),
				// 			token: item.payWith,
				// 		}
				// 	}
				// }))
				const prices = await getDisplayDefaultPricesChain(currentChainId, displayToEdit.priceModel, displayToEdit.nameHash);
				setDefaultPriceSaved(
					prices
						.filter((item) => {
							return !item.amount.eq(0)
						})
						.map((item, idx) => {
							return {
								id: new Date().getTime() + idx,
								idx: idx,
								isEdit: false,
								currentValue: {
									amount: tokenToFloat(item.amount).toString(),
									token: item.payWith,
								}
							}
						})
				)
			}
		}

		getDefaultPrices();
	}, [ displayToEdit ])
	useEffect(() => {
		const getCollateralPrices = async () => {

			if ( !displayToEdit ) { return; }
			if ( !erc20List.length ) { return; }

			setCollateralPriceToAdd([]);
			setCollateralPriceToEdit([]);
			setCollateralPriceToDelete([]);

			const collPrices = await getDisplayCollateralUnitPricesChain(currentChainId, displayToEdit.priceModel, displayToEdit.nameHash, erc20List.map((item) => { return item.contractAddress }));
			let collPricesParsed: Array<CollateralEditableField> = [];
			collPrices.forEach((item, priceIdx) => {
				if ( !item ) { return; }

				if (
					item.price.find((iitem) => {
						return iitem.amount.eq(0) &&
							iitem.denominator.eq(0) &&
							iitem.payWith === '0x0000000000000000000000000000000000000000'
					})
				) {
					return;
				}

				let collateralDecimals = 18;
				const foundCollateralToken = erc20List.find((erc20) => { return erc20.contractAddress.toLowerCase() === item.collateralToken.toLowerCase() });
				if ( foundCollateralToken ) { collateralDecimals = foundCollateralToken.decimals }

				item.price.forEach((iitem) => {

					let priceDecimals = 18;
					const foundPriceToken = erc20List.find((erc20) => { return erc20.contractAddress.toLowerCase() === iitem.payWith.toLowerCase() });
					if ( foundPriceToken ) { priceDecimals = foundPriceToken.decimals }

					collPricesParsed = [
						...collPricesParsed,
						{
							id: new Date().getTime() + (iitem.idx * 10000) + (priceIdx * 100),
							isEdit: false,
							collateralToken: item.collateralToken,
							idx: iitem.idx,
							source: 'saved',
							currentValue: {
								token: iitem.payWith,
								amount: derationalize(iitem.amount, iitem.denominator, new BigNumber(collateralDecimals - priceDecimals)).toString()
							}
						}
					]
				})
			})
			setCollateralPriceSaved(collPricesParsed);

		}

		getCollateralPrices();
	}, [ displayToEdit, erc20List ])

	useEffect(() => {
		if ( !currentChainId ) { return; }

		const foundChainConfig: any = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === currentChainId });
		if ( !foundChainConfig ) { return; }
		if ( !foundChainConfig.priceModels || !foundChainConfig.priceModels.length ) {
			setPriceModelsList([]);
			return;
		}

		const priceModelsListParsed = foundChainConfig.priceModels.map((item: { contractAddress: string, label: string }) => {
			return { value: item.contractAddress, label: `${item.label} ${compactString(item.contractAddress)}` }
		});
		setPriceModelsList(priceModelsListParsed);

	}, [ currentChainId ])

	const getPriceModelBlock = () => {

		if ( !displayToEdit ) { return null; }

		const foundPriceModel = priceModelsList.find((item) => { return item.value.toLowerCase() === displayToEdit.priceModel.toLowerCase() });
		if ( foundPriceModel ) {
			return (
				<div className="text-muted">
					<small>for the showcase type: <b>{ foundPriceModel.label }</b></small>
				</div>
			)
		}

		return (
			<div className="text-muted">
				<small>for the showcase type: <b>{ compactString(displayToEdit.priceModel) }</b></small>
			</div>
		)
	}

	const getDefaultPriceInputBlock = () => {
		return (
			<div className="lp-step-block">
				<h4>
					Default price for NFTs
					<TippyWrapper msg="The price that will be applied to all NFTs that don't have individual prices. If you don't plan to set individual prices for each NFT you sell, you can set prices in this section for all NFTs" elClass="ml-1" />
				</h4>
				<div className="row">
					<div className="col-md-10">
						<div className="input-group">
							<label className="input-label">Choose ERC-20 and set amount</label>
							<div className="select-group">
								<CoinSelector
									tokens={erc20List}
									onChange={(item) => { setInputDefaultPriceToken(item) }}
									selectedToken={ inputDefaultPriceToken }
								/>
								<input
									className="input-control"
									type="text"
									placeholder="0x000"
									value={ inputDefaultPriceToken }
									onChange={(e) => {
										const value = e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "");
										if ( Web3.utils.isAddress(value) ) {
											const foundToken = erc20List.find((item: ERC20Type) => { return item.contractAddress.toLowerCase() === value.toLowerCase() });
											if ( !foundToken ) {
												requestERC20Token(value, userAddress);
											}
										}
										setInputDefaultPriceToken(value);
									}}
								/>
							</div>
						</div>
					</div>
					<div className="col-md-10">
						<input
							className="input-control"
							type="text"
							placeholder="0.00"
							value={ addThousandSeparator(inputDefaultPriceAmount) }
							onChange={(e) => {
								let value = removeThousandSeparator(e.target.value);
								if ( value !== '' && !value.endsWith('.') && !value.endsWith('0') ) {
									if ( new BigNumber(value).isNaN() ) { return; }
									value = new BigNumber(value).toString();
								}
								setInputDefaultPriceAmount(value)
							}}
						/>
					</div>
					<div className="col-md-2">
						<div className="input-group">
							<button
								className="btn btn-outline w-100"
								disabled={ inputDefaultPriceToken === '' || inputDefaultPriceAmount === '' || new BigNumber(inputDefaultPriceAmount).eq(0) }
								onClick={() => {
									if ( inputDefaultPriceToken === '' || inputDefaultPriceAmount === '' || new BigNumber(inputDefaultPriceAmount).eq(0) ) { return; }

									setInputDefaultPriceToken('');
									setInputDefaultPriceAmount('');

									let foundToken: ERC20Type | undefined = erc20List.find((iitem: ERC20Type) => { return iitem.contractAddress.toLowerCase() === inputDefaultPriceToken.toLowerCase() });

									const isInEdited = defaultPriceToEdit.find((item) => { return item.currentValue.token.toLowerCase() === inputDefaultPriceToken.toLowerCase() });
									if ( isInEdited ) {
										setDefaultPriceToEdit([
											...defaultPriceToEdit.filter((item) => { return item.currentValue.token.toLowerCase() !== inputDefaultPriceToken.toLowerCase() }),
											{
												id: new Date().getTime(),
												isEdit: false,
												currentValue: {
													token: inputDefaultPriceToken,
													amount: tokenToInt(new BigNumber(inputDefaultPriceAmount), foundToken?.decimals || 18).toString()
												}
											}
										]);
										return;
									}
									const isInSaved = defaultPriceSaved.find((item) => { return item.currentValue.token.toLowerCase() === inputDefaultPriceToken.toLowerCase() });
									if ( isInSaved ) {
										setDefaultPriceSaved([
											...defaultPriceSaved.filter((item) => { return item.currentValue.token.toLowerCase() !== inputDefaultPriceToken.toLowerCase() }),
										]);
										setDefaultPriceToEdit([
											...defaultPriceToEdit.filter((item) => { return item.currentValue.token.toLowerCase() !== inputDefaultPriceToken.toLowerCase() }),
											{
												id: new Date().getTime(),
												isEdit: false,
												currentValue: {
													token: inputDefaultPriceToken,
													amount: tokenToInt(new BigNumber(inputDefaultPriceAmount), foundToken?.decimals || 18).toString()
												}
											}
										]);
										return;
									}

									setDefaultPriceToAdd([
										...defaultPriceToAdd.filter((item) => { return item.currentValue.token.toLowerCase() !== inputDefaultPriceToken.toLowerCase() }),
										{
											id: new Date().getTime(),
											isEdit: false,
											currentValue: {
												token: inputDefaultPriceToken,
												amount: tokenToInt(new BigNumber(inputDefaultPriceAmount), foundToken?.decimals || 18).toString()
											}
										}
									]);
								}}
							>Add</button>
						</div>
					</div>
				</div>
			</div>
		)
	}
	const getDefaultPriceDelBtn = (item: DefaultEditableField, recordType: 'saved' | 'edited' | 'added') => {
		if ( recordType === 'added' ) {
			return (
				<div className="btn-group">
					<button
						className="btn btn-md btn-gray btn-img"
						onClick={() => {
							setDefaultPriceToAdd(defaultPriceToAdd.filter((iitem) => { return item.id !== iitem.id }))
						}}
					>
						<img src= { i_del } alt="" />
					</button>
				</div>
			)
		}

		return (
			<div className="btn-group">
				<button
					className="btn btn-md btn-gray btn-img"
					onClick={() => {
						if ( recordType === 'saved' ) {
							setDefaultPriceSaved(defaultPriceSaved.filter((iitem) => { return item.id !== iitem.id }));
							setDefaultPriceToDelete([
								...defaultPriceToDelete,
								item
							])
						}
						if ( recordType === 'edited' ) {
							setDefaultPriceToEdit(defaultPriceToEdit.filter((iitem) => { return item.id !== iitem.id }));
							setDefaultPriceToDelete([
								...defaultPriceToDelete,
								item
							])
						}
					}}
				>
					<img src= { i_del } alt="" />
				</button>
			</div>
		)
	}
	const getDefaultPriceRowEditMode = (item: DefaultEditableField, recordType: 'saved' | 'edited' | 'added') => {
		let foundToken: ERC20Type | undefined = erc20List.find((iitem: ERC20Type) => { return iitem.contractAddress.toLowerCase() === item.currentValue.token.toLowerCase() });
		if ( !foundToken ) {
			foundToken = {
				assetType: _AssetType.ERC20,
				contractAddress: item.currentValue.token,
				decimals: 18,
				balance: new BigNumber(0),
				icon: default_coin,
				name: compactString(item.currentValue.token),
				symbol: compactString(item.currentValue.token),
				allowance: [],
			}
		}

		const updatedValue = tokenToInt(new BigNumber(item.editedValue?.amount || '0'), foundToken?.decimals).toString();

		const undoEdit = () => {
			if ( recordType === 'added' ) {
				setDefaultPriceToAdd([
					...defaultPriceToAdd.filter((iitem) => { return item.id !== iitem.id }),
					{
						...item,
						isEdit: false,
						editedValue: undefined
					}
				])
			}
			if ( recordType === 'edited' ) {
				setDefaultPriceToEdit([
					...defaultPriceToEdit.filter((iitem) => { return item.id !== iitem.id }),
					{
						...item,
						isEdit: false,
						editedValue: undefined
					}
				])
			}
			if ( recordType === 'saved' ) {
				setDefaultPriceSaved([
					...defaultPriceSaved.filter((iitem) => { return item.id !== iitem.id }),
					{
						...item,
						isEdit: false,
						editedValue: undefined
					}
				])
			}
		}
		const saveEdit = () => {
			if ( recordType === 'added' ) {
				setDefaultPriceToAdd([
					...defaultPriceToAdd.filter((iitem) => { return item.id !== iitem.id }),
					{
						...item,
						isEdit: false,
						currentValue: {
							token: item.editedValue?.token || '',
							amount: updatedValue,
						},
						editedValue: undefined
					}
				]);
			}
			if ( recordType === 'edited' ) {
				setDefaultPriceToEdit([
					...defaultPriceToEdit.filter((iitem) => { return item.id !== iitem.id }),
					{
						...item,
						isEdit: false,
						currentValue: {
							token: item.editedValue?.token || '',
							amount: updatedValue,
						},
						editedValue: undefined
					}
				]);
			}
			if ( recordType === 'saved' ) {
				setDefaultPriceSaved(defaultPriceSaved.filter((iitem) => { return item.id !== iitem.id }));
				setDefaultPriceToEdit([
					...defaultPriceToEdit.filter((iitem) => { return item.id !== iitem.id }),
					{
						...item,
						isEdit: false,
						currentValue: {
							token: item.editedValue?.token || '',
							amount: updatedValue,
						},
						editedValue: undefined
					}
				]);
			}
		}

		return (
			<div className="item item-narrow" key={ item.id }>
				<div className="row-edit">
					<div className="col-md-8 mb-2">
						<div className="select-group">
							<div className="select-coin">
								<div className="select-coin__value">
									<span className="field-unit">
										<span className="i-coin">
											<img src={ foundToken.icon } alt="" />
										</span>
										{ foundToken.symbol }
									</span>
								</div>
							</div>
							<input
								className="input-control"
								type="text"
								placeholder="0.00"
								value={ item.editedValue?.amount }
								onChange={(e) => {
									let value = removeThousandSeparator(e.target.value);
									if ( value !== '' && !value.endsWith('.') && !value.endsWith('0') ) {
										if ( new BigNumber(value).isNaN() ) { return; }
										value = new BigNumber(value).toString();
									}
									if ( recordType === 'added' ) {
										setDefaultPriceToAdd([
											...defaultPriceToAdd.filter((iitem) => { return item.id !== iitem.id }),
											{
												...item,
												isEdit: true,
												editedValue: {
													token: item.currentValue.token,
													amount: value,
												}
											}
										])
									}
									if ( recordType === 'edited' ) {
										setDefaultPriceToEdit([
											...defaultPriceToEdit.filter((iitem) => { return item.id !== iitem.id }),
											{
												...item,
												isEdit: true,
												editedValue: {
													token: item.currentValue.token,
													amount: value,
												}
											}
										])
									}
									if ( recordType === 'saved' ) {
										setDefaultPriceSaved([
											...defaultPriceSaved.filter((iitem) => { return item.id !== iitem.id }),
											{
												...item,
												isEdit: true,
												editedValue: {
													token: item.currentValue.token,
													amount: value,
												}
											}
										])
									}
								}}
							/>
						</div>
					</div>
					<div className="col-6 col-md-2 mb-2">
						<button
							className="btn btn-primary w-100"
							disabled={ item.editedValue && new BigNumber(item.editedValue.amount).eq(0) }
							onClick={() => {

								if (
									item.editedValue &&
									item.currentValue.amount.toLowerCase() === updatedValue.toLowerCase() &&
									item.currentValue.token.toLowerCase() === item.editedValue.token.toLowerCase()
								) {
									undoEdit();
									return;
								}

								saveEdit();
							}}
						>
							Save
						</button>
					</div>
					<div className="col-6 col-md-2 mb-2">
						<button
							className="btn btn-gray w-100"
							onClick={() => {
								undoEdit();
							}}
						>
							Undo
						</button>
					</div>
				</div>
			</div>
		)
	}
	const getDefaultPriceRow = (item: DefaultEditableField, recordType: 'saved' | 'edited' | 'added') => {
		let foundToken: ERC20Type | undefined = erc20List.find((iitem: ERC20Type) => { return iitem.contractAddress.toLowerCase() === item.currentValue.token.toLowerCase() });
		if ( !foundToken ) {
			foundToken = {
				assetType: _AssetType.ERC20,
				contractAddress: item.currentValue.token,
				decimals: 18,
				balance: new BigNumber(0),
				icon: default_coin,
				name: compactString(item.currentValue.token),
				symbol: compactString(item.currentValue.token),
				allowance: [],
			}
		}

		if ( item.isEdit ) {
			return getDefaultPriceRowEditMode(item, recordType)
		} else {
			return (
				<div className="item item-narrow" key={ item.id }>
					<div className="row">
						<div className="col-auto col-md-2 mb-2">
							<div className="tb-coin">
								<span className="i-coin"><img src={ foundToken.icon } alt="" /></span>
								<span className="name">{ foundToken.symbol }</span>
							</div>
						</div>
						<div className="col mb-2 d-flex align-items-center">
							<span className="text-break">{ tokenToFloat(new BigNumber(item.currentValue.amount), foundToken.decimals).toString() }</span>
						</div>
						<div className="col-sm-3 mb-2 d-flex">
							<div className="btns-group mr-0 ml-auto">
								<div className="btn-group">
									<button
										className="btn btn-md btn-gray btn-img"
										onClick={() => {
											if ( recordType === 'added' ) {
												setDefaultPriceToAdd([
													...defaultPriceToAdd.filter((iitem) => { return item.id !== iitem.id }),
													{
														...item,
														isEdit: true,
														editedValue: {
															token: item.currentValue.token,
															amount: tokenToFloat(new BigNumber(item.currentValue.amount), foundToken?.decimals).toString(),
														},
													}
												])
											}
											if ( recordType === 'edited' ) {
												setDefaultPriceToEdit([
													...defaultPriceToEdit.filter((iitem) => { return item.id !== iitem.id }),
													{
														...item,
														isEdit: true,
														editedValue: {
															token: item.currentValue.token,
															amount: tokenToFloat(new BigNumber(item.currentValue.amount), foundToken?.decimals).toString(),
														},
													}
												])
											}
											if ( recordType === 'saved' ) {
												setDefaultPriceSaved([
													...defaultPriceSaved.filter((iitem) => { return item.id !== iitem.id }),
													{
														...item,
														isEdit: true,
														editedValue: {
															token: item.currentValue.token,
															amount: tokenToFloat(new BigNumber(item.currentValue.amount), foundToken?.decimals).toString(),
														},
													}
												])
											}
										}}
									>
										<img src= { i_edit } alt="" />
									</button>
								</div>
								{ getDefaultPriceDelBtn(item, recordType) }
							</div>
						</div>
					</div>
				</div>
			)
		}
	}
	const getDefaultPriceListBlock = () => {
		return (
			<div className="c-wrap__table">
				{
					defaultPriceSaved
						.sort((item, prev) => { return item.id - prev.id })
						.map((item: DefaultEditableField) => { return getDefaultPriceRow(item, 'saved') })
				}
				{
					defaultPriceToEdit
						.sort((item, prev) => { return item.id - prev.id })
						.map((item: DefaultEditableField) => { return getDefaultPriceRow(item, 'edited') })
				}
				{
					defaultPriceToAdd
						.sort((item, prev) => { return item.id - prev.id })
						.map((item: DefaultEditableField) => { return getDefaultPriceRow(item, 'added') })
				}
			</div>
		)
	}

	const getCollateralPriceDelBtn = (row: CollateralEditableField) => {
		if ( row.source === 'added' ) {
			return (
				<div className="btn-group">
					<button
						className="btn btn-md btn-gray btn-img"
						onClick={() => {
							setCollateralPriceToAdd(collateralPriceToAdd.filter((item) => {
								return item.collateralToken.toLowerCase() !== row.collateralToken.toLowerCase() ||
									item.currentValue.token.toLowerCase() !== row.currentValue.token.toLowerCase()
							}))
						}}
					>
						<img src= { i_del } alt="" />
					</button>
				</div>
			)
		}

		return (
			<div className="btn-group">
				<button
					className="btn btn-md btn-gray btn-img"
					onClick={() => {
						if ( row.source === 'saved' ) {
							setCollateralPriceSaved(collateralPriceSaved.filter((iitem) => { return row.id !== iitem.id }));
							setCollateralPriceToDelete([
								...collateralPriceToDelete,
								row
							])
						}
						if ( row.source === 'edited' ) {
							setCollateralPriceToEdit(collateralPriceToEdit.filter((iitem) => { return row.id !== iitem.id }));
							setCollateralPriceToDelete([
								...collateralPriceToDelete,
								row
							])
						}
					}}
				>
					<img src= { i_del } alt="" />
				</button>
			</div>
		)
	}
	const getCollateralPriceGroup = (_collateralToken: string, prices: Array<CollateralEditableField>) => {

		let collateralToken = erc20List.find((iitem: ERC20Type) => { return _collateralToken.toLowerCase() === iitem.contractAddress.toLowerCase() });
		if ( !collateralToken ) {
			collateralToken = {
				assetType: _AssetType.ERC20,
				contractAddress: _collateralToken,
				decimals: 18,
				balance: new BigNumber(0),
				icon: default_coin,
				name: compactString(_collateralToken),
				symbol: compactString(_collateralToken),
				allowance: [],
			}
		}

		return (
			<React.Fragment key={ collateralToken.contractAddress }>
			<div className="d-flex align-center my-3">
				<span className="i-coin mr-2"><img src={ collateralToken.icon } alt="" /></span>
				<span className="name"><b className="mr-1">1 { collateralToken.symbol }</b>for</span>
			</div>
			<div className="c-wrap__table">
				{
					prices
					.sort((item, prev) => { return (item.idx || 999) - (prev.idx || 999) })
					.map((row) => {

						let priceToken = erc20List.find((iiitem: ERC20Type) => { return iiitem.contractAddress.toLowerCase() === row.currentValue.token.toLowerCase() });
						if ( !priceToken ) {
							priceToken = {
								assetType: _AssetType.ERC20,
								contractAddress: row.currentValue.token,
								decimals: 18,
								balance: new BigNumber(0),
								icon: default_coin,
								name: compactString(row.currentValue.token),
								symbol: compactString(row.currentValue.token),
								allowance: [],
							}
						}

						if ( row.isEdit ) {

							const undoEdit = () => {
								if ( row.source === 'saved' ) {
									setCollateralPriceSaved([
										...collateralPriceSaved.filter((item) => { return row.id !== item.id }),
										{
											...row,
											editedValue: undefined,
											isEdit: false,
										}
									]);
									return;
								}
								if ( row.source === 'edited' ) {
									setCollateralPriceToEdit([
										...collateralPriceToEdit.filter((item) => { return row.id !== item.id }),
										{
											...row,
											editedValue: undefined,
											isEdit: false,
										}
									]);
									return;
								}
								if ( row.source === 'added' ) {
									setCollateralPriceToAdd([
										...collateralPriceToAdd.filter((item) => { return row.id !== item.id }),
										{
											...row,
											editedValue: undefined,
											isEdit: false,
										}
									]);
								}
							}
							const saveEdit = () => {
								if ( row.source === 'saved' ) {
									setCollateralPriceSaved(collateralPriceSaved.filter((item) => { return item.id !== row.id }));
									setCollateralPriceToEdit([
										...collateralPriceToEdit.filter((item) => { return row.id !== item.id }),
										{
											...row,
											isEdit: false,
											source: 'edited',
											currentValue: {
												token: row.currentValue.token,
												amount: row.editedValue?.amount || '0',
											},
											editedValue: undefined
										}
									]);
									return;
								}
								if ( row.source === 'edited' ) {
									setCollateralPriceToEdit([
										...collateralPriceToEdit.filter((item) => { return row.id !== item.id }),
										{
											...row,
											isEdit: false,
											currentValue: {
												token: row.currentValue.token,
												amount: row.editedValue?.amount || '0',
											},
											editedValue: undefined
										}
									]);
									return;
								}
								if ( row.source === 'added' ) {
									setCollateralPriceToAdd([
										...collateralPriceToAdd.filter((item) => { return row.id !== item.id }),
										{
											...row,
											currentValue: {
												token: row.currentValue.token,
												amount: row.editedValue?.amount || '0',
											},
											isEdit: false,
											editedValue: undefined
										}
									]);
								}
							}

							return (
								<div className="item item-narrow" key={ row.id }>
									<div className="row-edit">
										<div className="col-md-8 mb-2">
											<div className="select-group">
												<div className="select-coin">
													<div className="select-coin__value">
														<span className="field-unit">
															<span className="i-coin">
																<img src={ priceToken.icon } alt="" />
															</span>
															{ priceToken.symbol }
														</span>
													</div>
												</div>
												<input
													className="input-control"
													type="text"
													placeholder="0.00"
													value={ row.editedValue?.amount }
													onChange={(e) => {
														let value = removeThousandSeparator(e.target.value);
														if ( value !== '' && !value.endsWith('.') && !value.endsWith('0') ) {
															if ( new BigNumber(value).isNaN() ) { return; }
															value = new BigNumber(value).toString();
														}
														if ( row.source === 'saved' ) {
															setCollateralPriceSaved([
																...collateralPriceSaved.filter((item) => { return row.id !== item.id }),
																{
																	...row,
																	editedValue: {
																		token: row.currentValue.token,
																		amount: value,
																	}
																}
															]);
															return;
														}
														if ( row.source === 'edited' ) {
															setCollateralPriceToEdit([
																...collateralPriceToEdit.filter((item) => { return row.id !== item.id }),
																{
																	...row,
																	editedValue: {
																		token: row.currentValue.token,
																		amount: value,
																	}
																}
															]);
															return;
														}
														if ( row.source === 'added' ) {
															setCollateralPriceToAdd([
																...collateralPriceToAdd.filter((item) => { return row.id !== item.id }),
																{
																	...row,
																	editedValue: {
																		token: row.currentValue.token,
																		amount: value,
																	}
																}
															]);
														}
													}}
												/>
											</div>
										</div>
										<div className="col-6 col-md-2 mb-2">
											<button
												className="btn btn-primary w-100"
												disabled={ row.editedValue && new BigNumber(row.editedValue.amount).eq(0) }
												onClick={() => {

													if (
														row.editedValue &&
														row.currentValue.amount.toLowerCase() === row.editedValue.amount.toLowerCase() &&
														row.currentValue.token.toLowerCase() === row.editedValue.token.toLowerCase()
													) {
														undoEdit();
														return;
													}

													saveEdit();
												}}
											>
												Save
											</button>
										</div>
										<div className="col-6 col-md-2 mb-2">
											<button
												className="btn btn-gray w-100"
												onClick={() => {
													undoEdit();
												}}
											>
												Undo
											</button>
										</div>
									</div>
								</div>
							)

						} else {
							return (
								<div className="item item-narrow" key={ row.id }>
									<div className="row">
										<div className="col-auto col-md-2 mb-2">
											<div className="tb-coin">
												<span className="i-coin"><img src={ priceToken.icon } alt="" /></span>
												<span className="name">{ priceToken.symbol }</span>
											</div>
										</div>
										<div className="col mb-2 d-flex align-items-center">
											<span className="text-break">{ row.currentValue.amount }</span>
										</div>
										<div className="col-sm-3 mb-2 d-flex">
											<div className="btns-group mr-0 ml-auto">
												<div className="btn-group">
													<button
														className="btn btn-md btn-gray btn-img"
														onClick={() => {
															if ( row.source === 'saved' ) {
																setCollateralPriceSaved([
																	...collateralPriceSaved.filter((item) => { return row.id !== item.id }),
																	{
																		...row,
																		isEdit: true,
																		editedValue: {
																			token: row.currentValue.token,
																			amount: row.currentValue.amount,
																		},
																	}
																]);
																return;
															}
															if ( row.source === 'edited' ) {
																setCollateralPriceToEdit([
																	...collateralPriceToEdit.filter((item) => { return row.id !== item.id }),
																	{
																		...row,
																		isEdit: true,
																		editedValue: {
																			token: row.currentValue.token,
																			amount: row.currentValue.amount,
																		},
																	}
																]);
																return;
															}
															if ( row.source === 'added' ) {
																setCollateralPriceToAdd([
																	...collateralPriceToAdd.filter((item) => { return row.id !== item.id }),
																	{
																		...row,
																		isEdit: true,
																		editedValue: {
																			token: row.currentValue.token,
																			amount: row.currentValue.amount,
																		},
																	}
																]);
															}
														}}
													>
														<img src= { i_edit } alt="" />
													</button>
												</div>
												{ getCollateralPriceDelBtn(row) }
											</div>
										</div>
									</div>
								</div>
							)
						}
					})
				}
			</div>
			</React.Fragment>
		)
	}

	const getCollateralPriceRows = () => {
		const groups = groupBy([
			...collateralPriceSaved,
			...collateralPriceToEdit,
			...collateralPriceToAdd
		], (item) => { return item.collateralToken });
		return Object.keys(groups)
			.sort((item, prev) => { return item.localeCompare(prev) })
			.map((item) => { return getCollateralPriceGroup(item, groups[item]) })
	}
	const getCollateralPriceListBlock = () => {

		let collateralToken = erc20List.find((item: ERC20Type) => { return item.contractAddress.toLowerCase() === inputCollateralToken.toLowerCase() });
		if ( !collateralToken ) {
			collateralToken = {
				assetType: _AssetType.ERC20,
				contractAddress: inputCollateralToken,
				decimals: 18,
				balance: new BigNumber(0),
				icon: default_coin,
				name: compactString(inputCollateralToken),
				symbol: compactString(inputCollateralToken),
				allowance: [],
			}
		}

		return (
			<div className="lp-step-block">
				<h4 className="mb-0">
					Default price for 1 collateral token in wNFTs
					<TippyWrapper msg="The price per unit of ERC20 collateral token in wNFTs sold. The value of wNFTs is defined as the number of ERC20 tokens multiplied by the price per token" elClass="ml-1" />
				</h4>
				<div className="row mb-4">
					<div className="col-lg-10">
						<p>The ERC20 token must be the first to be added to the collateral in wNFT for sale. The price per unit of the ERC20 token being sold may be in other ERC20 tokens, or native coins. These prices apply to all wNFTs and their collateral tokens. </p>
					</div>
				</div>
				<div className="row">
				<div className="col-md-10">
						<div className="input-group">
							<label className="input-label">Choose a collateral token</label>
							<div className="select-group">
								<CoinSelector
									tokens={erc20List}
									onChange={(item) => { setInputCollateralToken(item) }}
									selectedToken={ inputCollateralToken }
								/>
								<input
									className="input-control"
									type="text"
									placeholder="0x000"
									value={ inputCollateralToken }
									onChange={(e) => {
										const value = e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "");
										if ( Web3.utils.isAddress(value) ) {
											const foundToken = erc20List.find((item: ERC20Type) => { return item.contractAddress.toLowerCase() === value.toLowerCase() });
											if ( !foundToken ) {
												requestERC20Token(value, userAddress);
											}
										}
										setInputCollateralToken(value);
										setInputCollateralPriceToken('');
									}}
								/>
							</div>
						</div>
					</div>
					<div className="col-12 col-md-10">
						<div className="input-group">
							{/* {
								Web3.utils.isAddress(inputCollateralToken) ? (
									<label className="input-label">Price for <b>1 { collateralToken.symbol }</b></label>
								) : null
							} */}
							<label className="input-label">Choose the token you're selling for</label>
							<div className="select-group">
								<CoinSelector
									tokens={ erc20List.filter((item) => { return item.contractAddress.toLowerCase() !== inputCollateralToken.toLowerCase() }) }
									onChange={(item) => { setInputCollateralPriceToken(item) }}
									selectedToken={ inputCollateralPriceToken }
								/>
								<input
									className="input-control"
									type="text"
									placeholder="0x000"
									value={ inputCollateralPriceToken }
									onChange={(e) => {
										const value = e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "");
										if ( Web3.utils.isAddress(value) ) {
											const foundToken = erc20List.find((item: ERC20Type) => { return item.contractAddress.toLowerCase() === value.toLowerCase() });
											if ( !foundToken ) {
												requestERC20Token(value, userAddress);
											}
										}
										setInputCollateralPriceToken(value);
									}}
								/>
							</div>
						</div>
					</div>
					<div className="col-md-10">
						<div className="input-group">
							<label className="input-label">Set the price per 1 collateral token</label>
							<input
								className="input-control"
								type="text"
								placeholder="0.00"
								value={ addThousandSeparator(inputCollateralPriceAmount) }
								onChange={(e) => {
									let value = removeThousandSeparator(e.target.value);
									if ( value !== '' && !value.endsWith('.') && !value.endsWith('0') ) {
										if ( new BigNumber(value).isNaN() ) { return; }
										value = new BigNumber(value).toString();
									}
									setInputCollateralPriceAmount(value)
								}}
							/>
						</div>
					</div>
					<div className="col-md-2">
						<div className="input-group">
							<label className="input-label">&nbsp;</label>
							<button
								className="btn btn-outline w-100"
								disabled={ inputCollateralToken === '' || inputCollateralPriceToken === '' || inputCollateralPriceAmount === '' || new BigNumber(inputCollateralPriceAmount).eq(0) }
								onClick={() => {

									setInputCollateralToken('');
									setInputCollateralPriceToken('');
									setInputCollateralPriceAmount('');

									let priceToken = erc20List.find((item: ERC20Type) => { return inputCollateralPriceToken.toLowerCase() === item.contractAddress.toLowerCase() });
									if ( !priceToken ) {
										priceToken = {
											assetType: _AssetType.ERC20,
											contractAddress: inputCollateralPriceToken,
											decimals: 18,
											balance: new BigNumber(0),
											icon: default_coin,
											name: compactString(inputCollateralPriceToken),
											symbol: compactString(inputCollateralPriceToken),
											allowance: [],
										}
									}

									const addedCollateralSaved = collateralPriceSaved.find((item) => {
										return item.collateralToken.toLowerCase() === inputCollateralToken.toLowerCase() &&
											item.currentValue.token.toLowerCase() === inputCollateralPriceToken.toLowerCase()
									});
									if ( addedCollateralSaved ) {
										setCollateralPriceSaved(collateralPriceSaved.filter((item) => {
											return item.collateralToken.toLowerCase() !== inputCollateralToken.toLowerCase() ||
												item.currentValue.token.toLowerCase() !== inputCollateralPriceToken.toLowerCase()
										}));
										setCollateralPriceToEdit(
											[
												...collateralPriceToEdit.filter((item) => {
													return item.collateralToken.toLowerCase() !== inputCollateralToken.toLowerCase() ||
														item.currentValue.token.toLowerCase() !== inputCollateralPriceToken.toLowerCase()
												}),
												{
													id: addedCollateralSaved.id,
													isEdit: false,
													collateralToken: addedCollateralSaved.collateralToken,
													idx: addedCollateralSaved.idx,
													source: 'edited',
													currentValue: {
														token: inputCollateralPriceToken,
														amount: inputCollateralPriceAmount,
													}
												}
											]
										);
										return;
									}
									const addedCollateralToEdit = collateralPriceToEdit.find((item) => {
										return item.collateralToken.toLowerCase() === inputCollateralToken.toLowerCase() &&
											item.currentValue.token.toLowerCase() === inputCollateralPriceToken.toLowerCase()
									});
									if ( addedCollateralToEdit ) {
										setCollateralPriceToEdit(
											[
												...collateralPriceToEdit.filter((item) => {
													return item.collateralToken.toLowerCase() !== inputCollateralToken.toLowerCase() ||
														item.currentValue.token.toLowerCase() !== inputCollateralPriceToken.toLowerCase()
												}),
												{
													id: addedCollateralToEdit.id,
													isEdit: false,
													collateralToken: addedCollateralToEdit.collateralToken,
													idx: addedCollateralToEdit.idx,
													source: 'edited',
													currentValue: {
														token: inputCollateralPriceToken,
														amount: inputCollateralPriceAmount,
													}
												}
											]
										);
										return;
									}

									setCollateralPriceToAdd(
										[
											...collateralPriceToAdd.filter((item) => {
												return item.collateralToken.toLowerCase() !== inputCollateralToken.toLowerCase() ||
													item.currentValue.token.toLowerCase() !== inputCollateralPriceToken.toLowerCase()
											}),
											{
												id: new Date().getTime(),
												collateralToken: inputCollateralToken,
												idx: undefined,
												source: 'added',
												isEdit: false,
												currentValue: {
													token: inputCollateralPriceToken,
													amount: inputCollateralPriceAmount,
												}
											}
										]
									);
								}}
							>Add</button>
						</div>
					</div>
				</div>
				{ getCollateralPriceRows() }
			</div>
		)
	}

	const updateSubmit = async () => {

		if ( !displayToEdit ) { return; }

		const pageEdited = !!defaultPriceToAdd.length || !!defaultPriceToEdit.length || !!collateralPriceToEdit.length || !!collateralPriceToAdd.length || !!defaultPriceToDelete.length || !!collateralPriceToDelete.length;
		if ( !pageEdited ) {
			navigate(`/discount/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.nameHash}`);
			return;
		}

		const web3 = await connect();
		if ( !web3 ) { return; }
		const user = await getUserAddress(web3);
		if ( !user ) { return; }

		const loaderStages: Array<AdvancedLoaderStageType> = []
		if ( defaultPriceToEdit.length || defaultPriceToDelete.length || defaultPriceToAdd.length ) {
			loaderStages.push({
				id: 'default_edit',
				sortOrder: 1,
				text: `Updating default prices`,
				status: _AdvancedLoadingStatus.queued,
			});
		}

		const collateralPrices = [
			...collateralPriceSaved,
			...collateralPriceToDelete,
			...collateralPriceToEdit,
			...collateralPriceToAdd,
		]
		const collateralGroups = groupBy(collateralPrices, (item) => { return item.collateralToken });
		const collateralErc20HasChanges: Array<string> = [];
		Object.keys(collateralGroups).forEach((idx) => {
			const item = collateralGroups[idx];
			item.forEach((iitem) => {
				if ( collateralErc20HasChanges.includes(idx) ) { return; }
				const foundInDelete = collateralPriceToDelete.find((iiitem) => { return iiitem.id === iitem.id });
				const foundInEdit   = collateralPriceToEdit.find(  (iiitem) => { return iiitem.id === iitem.id });
				const foundInAdd    = collateralPriceToAdd.find(   (iiitem) => { return iiitem.id === iitem.id });

				if ( foundInDelete || foundInEdit || foundInAdd ) { collateralErc20HasChanges.push(idx) }
			})
		});
		if ( collateralPriceToEdit.length || collateralPriceToDelete.length || collateralPriceToAdd.length ) {
			loaderStages.push({
				id: 'collateral_edit',
				sortOrder: 11,
				text: `Updating collateral price`,
				status: _AdvancedLoadingStatus.queued,
				total: collateralErc20HasChanges.length,
				current: 1,
			});
		}

		const advLoader = {
			title: 'Waiting to update',
			stages: loaderStages
		};
		createAdvancedLoader(advLoader);

		if ( defaultPriceToEdit.length || defaultPriceToDelete.length || defaultPriceToAdd.length ) {
			updateStepAdvancedLoader({
				id: 'default_edit',
				status: _AdvancedLoadingStatus.loading
			});

			let output: Array<Price> = [
				...defaultPriceSaved.map((item) => {
					return {
						idx: 0,
						payWith: item.currentValue.token,
						amount: new BigNumber(item.currentValue.amount),
					}
				}),
				...defaultPriceToEdit.map((item) => {
					return {
						idx: 0,
						payWith: item.currentValue.token,
						amount: new BigNumber(item.currentValue.amount),
					}
				}),
				...defaultPriceToAdd.map((item) => {
					return {
						idx: 0,
						payWith: item.currentValue.token,
						amount: new BigNumber(item.currentValue.amount),
					}
				})
			];
			if ( !output.length ) {
				output = [{
					idx: 0,
					payWith: '0x0000000000000000000000000000000000000000',
					amount: new BigNumber(0),
				}]
			}

			console.log('output', output);
			try {
				await setDefaultPriceInChain(web3, displayToEdit.priceModel, displayToEdit.nameHash, output)
			} catch(e: any) {
				setModal({
					type: _ModalTypes.error,
					title: `Cannot update default prices`,
					details: [
						`User address: ${userAddress}`,
						`Price model: ${displayToEdit.priceModel}`,
						`Showcase hash: ${displayToEdit.nameHash}`,
						`Updated prices: ${JSON.stringify(output)}`,
						'',
						e.message || e,
					],
					buttons: [{
						text: 'Ok',
						clickFunc: () => { window.location.reload(); }
					}]
				});
				return;
			}

			updateStepAdvancedLoader({
				id: 'default_edit',
				status: _AdvancedLoadingStatus.complete
			});
		}


		if ( collateralErc20HasChanges.length ) {
			let txIdx = 1;
			for (const idx in collateralErc20HasChanges) {

				updateStepAdvancedLoader({
					id: 'collateral_edit',
					status: _AdvancedLoadingStatus.loading,
					current: txIdx,
				});

				const collateralTokenAddress = collateralErc20HasChanges[idx];
				const priceItems = collateralGroups[collateralTokenAddress];

				let collateralToken = erc20List.find((item: ERC20Type) => { return collateralTokenAddress.toLowerCase() === item.contractAddress.toLowerCase() });
				if ( !collateralToken ) {
					collateralToken = {
						assetType: _AssetType.ERC20,
						contractAddress: collateralTokenAddress,
						decimals: 18,
						balance: new BigNumber(0),
						icon: default_coin,
						name: compactString(collateralTokenAddress),
						symbol: compactString(collateralTokenAddress),
						allowance: [],
					}
				}

				let collateralPriceParsed: Array<DenominatedPrice> = priceItems
					.filter((item) => {
						return !collateralPriceToDelete.find((iitem) => { return item.id === iitem.id });
					})
					.map((item) => {
						let priceDecimals = 18;
						const foundPriceToken = erc20List.find((erc20) => { return erc20.contractAddress.toLowerCase() === item.currentValue.token.toLowerCase() });
						if ( foundPriceToken ) { priceDecimals = foundPriceToken.decimals }

						const amountParsed = rationalize(new BigNumber(item.currentValue.amount), new BigNumber((collateralToken?.decimals || 18) - priceDecimals));
						return {
							payWith: item.currentValue.token,
							amount: amountParsed.numerator,
							denominator: amountParsed.denominator,
							idx: 0,
						}
					})

				if ( !collateralPriceParsed.length ) {
					collateralPriceParsed = [{
						payWith: '0x0000000000000000000000000000000000000000',
						amount: new BigNumber(0),
						denominator: new BigNumber(0),
						idx: 0,
					}]
				}

				try {
					await setCollateralPriceInChain(web3, displayToEdit.priceModel, displayToEdit.nameHash, collateralTokenAddress, collateralPriceParsed)
				} catch(e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot add collateral price`,
						details: [
							`User address: ${userAddress}`,
							`Price model: ${displayToEdit.priceModel}`,
							`Showcase hash: ${displayToEdit.nameHash}`,
							`Collateral token: ${idx}`,
							`Price: ${JSON.stringify(collateralPriceParsed)}`,
							'',
							e.message || e,
						],
						buttons: [{
							text: 'Ok',
							clickFunc: () => { window.location.reload(); }
						}]
					});
					return;
				}
				txIdx += 1;
			}

		}
		updateStepAdvancedLoader({
			id: 'collateral_edit',
			status: _AdvancedLoadingStatus.complete
		});

		setModal({
			type: _ModalTypes.success,
			title: 'Showcase prices successfully updated',
			text: currentChain?.hasOracle ? [
				{ text: 'After oracle synchronization, your changes will appear in launchpad admin panel soon. Please refresh the launchpads list' },
			] : [],
			buttons: [{
				text: 'Ok',
				clickFunc: () => {
					unsetModal();
					navigate(`/discount/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.nameHash}`);
					props.onUpdate();
				}
			}],
			// links: [{
			// 	text: `View showcase`,
			// 	url: `/launchpad/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.name}`
			// }]
		});

	}
	const saveBtnDsabled = (): boolean => {
		if ( !!defaultPriceSaved.find((item) => { return item.isEdit }) ) { return true; }
		if ( !!defaultPriceToAdd.find((item) => { return item.isEdit }) ) { return true; }
		if ( !!defaultPriceToEdit.find((item) => { return item.isEdit }) ) { return true; }
		if ( !!collateralPriceSaved.find((item) => { return item.isEdit }) ) { return true; }
		if ( !!collateralPriceToEdit.find((item) => { return item.isEdit }) ) { return true; }
		if ( !!collateralPriceToAdd.find((item) => { return item.isEdit }) ) { return true; }

		return false;
	}

	const getBackBtn = () => {
		const pageEdited = !!defaultPriceToAdd.length || !!defaultPriceToEdit.length || !!collateralPriceToEdit.length || !!collateralPriceToAdd.length || !!defaultPriceToDelete.length || !!collateralPriceToDelete.length;
		if ( pageEdited ) {
			return (
				<button
					className="btn btn-gray"
					onClick={() => {
						setModal({
							type: _ModalTypes.info,
							title: 'You have unsaved data. Continue?',
							buttons: [
								{
									text: 'Yes',
									clickFunc: () => {
										navigate(`/`);
										unsetModal();
									},
								},
								{
									text: 'No',
									clickFunc: () => { unsetModal(); },
									clazz: 'btn-grad'
								},
							]
						})
					}}
				>Back</button>
			)
		}

		if ( !displayToEdit ) {
			return (
				<Link
					className="btn btn-gray"
					to={ `/` }
				>Back</Link>
			)
		}

		return (
			<Link
				className="btn btn-gray"
				// to={ `/project/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.nameHash}` }
				to={ `/showcase/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.nameHash}` }
			>Back</Link>
		)
	}

	return (
		<div className="container">

			<div className="breadcrumbs">
				<Link className="item-back btn btn-sm btn-gray" to="/">
					<svg className="mr-2" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M12 5.25H2.8725L7.065 1.0575L6 0L0 6L6 12L7.0575 10.9425L2.8725 6.75H12V5.25Z" fill="white"></path>
					</svg>
					<span>To the showcase list</span>
				</Link>
			</div>

			<div className="row mb-4">
				<div className="col-12 col-sm">
					<div className="h3 mb-3 mt-0">Showcase title</div>
				</div>
				{
					displayToEdit ? (
						<div className="col-12 col-sm-auto">
							<div className="btns-group">
								<div className="btn-group">
									<a
										className="btn btn-md btn-gray"
										href={ `/launchpad/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.name}` }
										target="_blank" rel="noopener noreferrer"
									>Go to the showcase</a>
								</div>
								<div className="btn-group">
								<CopyToClipboard
									text={ `${window.location.origin}/launchpad/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.name}` }
									onCopy={() => {
										setDisplayLinkCopied(true);
										setTimeout(() => { setDisplayLinkCopied(false); }, 5*1000);
									}}
								>
									<button className="btn btn-md btn-gray btn-img">
										<img src= { i_copy } alt="" />
										<span className="btn-action-info" style={{ display: displayLinkCopied ? 'block' : 'none' }}>Copied</span>
									</button>
								</CopyToClipboard>
								</div>
							</div>
						</div>
					) : null
				}
			</div>
			<div className="row">
				<div className="col-md-2 col-lg-2">
					<StepsProgress
						current="price"
						display={ displayToEdit }
						confirmClose={ !!defaultPriceToAdd.length || !!defaultPriceToEdit.length || !!collateralPriceToEdit.length || !!collateralPriceToAdd.length || !!defaultPriceToDelete.length || !!collateralPriceToDelete.length }
					/>
				</div>
				<div className="col-md-10 col-lg-10">
					<div className="lp-wrap">
						<div className="lp-wrap__header">
							<div className="h3-wrap">Price</div>
							{ getPriceModelBlock() }
						</div>

						{ getDefaultPriceInputBlock() }
						{ getDefaultPriceListBlock() }

						{ getCollateralPriceListBlock() }

						<div className="lp-steps-footer">
							<div className="row justify-content-between">
								<div className="col-auto">
									{ getBackBtn() }
								</div>
								<div className="col-auto">
									<button
										className="btn btn-grad"
										disabled={ saveBtnDsabled() }
										onClick={ updateSubmit }
									>Save and proceed</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}