
import { ShowcaseStepPage } from './1_Showcase';
import { ProjectStepPage } from './2_Project';
import { PriceStepPage } from './3_Price';
import { DiscountsStepPage } from './4_Discounts';
import { TokensStepPage } from './5_Tokens';
import { Summary } from './6_Summary';

export {
	ShowcaseStepPage,
	ProjectStepPage,
	PriceStepPage,
	DiscountsStepPage,
	TokensStepPage,
	Summary
}