import {
	useContext,
	useEffect,
	useState
} from "react"
import {
	Link,
	useNavigate
} from "react-router-dom";
import { CopyToClipboard }     from 'react-copy-to-clipboard';

import {
	Display,
	DisplayParam,
	_Display,
	combineURLs,
	getStrHash,
} from "@envelop/envelop-client-core";
import StepsProgress     from "../../StepsProgress";
import InputWithOptions  from "../../InputWithOptions";

import {
	updateDisplayParamsInAPI
} from "../../../models";

import i_edit         from "../../../static/pics/icons/i-edit.svg";
import i_del          from "../../../static/pics/icons/i-del.svg";
import i_copy         from "../../../static/pics/icons/i-copy.svg";
import example_img    from "../../../static/pics/launchpad/example.png";
import default_social from "../../../static/pics/coins/_default.svg";
import {
	AdvancedLoaderStageType,
	InfoModalContext,
	Web3Context,
	_AdvancedLoadingStatus,
	_ModalTypes
} from "../../../dispatchers";

type ProjectStepPageProps = {
	displayToEdit: Display | undefined,
	onUpdate     : () => void,
}

type EditableField = {
	currentValue: {
		name      : string,
		value     : string,
		paramtype?: string,
	},
	editedValue?: {
		name      : string,
		value     : string,
		paramtype?: string,
	},
	id    : number,
	isEdit: boolean,
}

export default function ProjectStepPage(props: ProjectStepPageProps) {

	const { displayToEdit } = props;
	const navigate          = useNavigate();

	const {
		createAdvancedLoader,
		setModal,
		updateStepAdvancedLoader,
		unsetModal,
	} = useContext(InfoModalContext);
	const {
		web3,
		userAddress,
	} = useContext(Web3Context);

	const SOCIAL_NETWORKS_LIST = [
		{ label: 'Youtube', urlPrefix: 'https://youtube.com/channel/' },
		{ label: 'Twitter', urlPrefix: 'https://twitter.com/' },
		{ label: 'Telegram', urlPrefix: 'https://t.me/' },
		{ label: 'Discord', urlPrefix: 'https://discordapp.com/users/' },
		{ label: 'Reddit', urlPrefix: 'https://www.reddit.com/user/' },
		{ label: 'Custom', },
	];
	const PROJECT_PARAMS_LIST = [
		'Swap Deal',
		'Ticker',
		'IDO percent',
		'Network',
		'Site',
		'WP',
	];
	const LINK_PARAMS_LIST = [
		'NFT Discount',
		'NIFTSY discount',
		'Volume discount',
		'NFT-access',
		'Tokenomics',
	];

	const [ showHelpPopup         , setShowHelpPopup         ] = useState(false);
	const [ displayLinkCopied     , setDisplayLinkCopied     ] = useState(false);

	const [ inputTitle            , setInputTitle            ] = useState('');
	const [ inputDescription      , setInputDescription      ] = useState('');

	const [ inputToken            , setInputToken            ] = useState('');

	const [ inputSocialsName      , setInputSocialsName      ] = useState('');
	const [ inputSocialsLink      , setInputSocialsLink      ] = useState('');
	const [ inputSocialsList      , setInputSocialsList      ] = useState<Array<EditableField>>([]);

	const [ inputProjectParam     , setInputProjectParam     ] = useState('');
	const [ inputProjectValue     , setInputProjectValue     ] = useState('');
	const [ inputProjectParamList , setInputProjectParamList ] = useState<Array<EditableField>>([]);

	const [ inputBtnType          , setInputBtnType          ] = useState('');
	const [ inputBtnParam         , setInputBtnParam         ] = useState('');
	const [ inputBtnValue         , setInputBtnValue         ] = useState('');
	const [ inputBtnParamList     , setInputBtnParamList     ] = useState<Array<EditableField>>([]);

	const [ hasChanges , setHasChanges ] = useState(false);

	useEffect(() => {
		if ( !displayToEdit || !displayToEdit.params ) { return; }

		let socialsUpdated: Array<EditableField> = [];
		let projectUpdated: Array<EditableField> = [];
		let btnUpdated    : Array<EditableField> = [];

		displayToEdit.params.forEach((item, idx) => {
			if ( item.key_name.toLowerCase() === 'title' ) { setInputTitle(item.key_value) }
			if ( item.key_name.toLowerCase() === 'description' ) { setInputDescription(item.key_value) }
			if ( item.key_name.toLowerCase() === 'network' ) { setInputToken(item.key_value) }

			if ( item.key_type.toLowerCase() === 'socials' ) {
				socialsUpdated = [
					...socialsUpdated.filter((iitem) => { return item.key_name.toLowerCase() !== iitem.currentValue.name.toLowerCase() || item.key_value.toLowerCase() !== iitem.currentValue.value.toLowerCase() }),
					{
						id: new Date().getTime() + idx,
						isEdit: false,
						currentValue: {
							name: item.key_name,
							value: item.key_value,
						}
					}
				]
			}

			if ( item.key_type.toLowerCase() === 'project' ) {
				projectUpdated = [
					...projectUpdated.filter((iitem) => { return item.key_name.toLowerCase() !== iitem.currentValue.name.toLowerCase() || item.key_value.toLowerCase() !== iitem.currentValue.value.toLowerCase() }),
					{
						id: new Date().getTime() + idx,
						isEdit: false,
						currentValue: {
							name: item.key_name,
							value: item.key_value,
						}
					}
				]
			}

			if ( item.key_type.toLowerCase() === 'links' ) {
				btnUpdated = [
					...btnUpdated.filter((iitem) => { return item.key_name.toLowerCase() !== iitem.currentValue.name.toLowerCase() }),
					{
						id: new Date().getTime() + idx,
						isEdit: false,
						currentValue: {
							paramtype: 'link',
							name: item.key_name,
							value: item.key_value,
						}
					}
				]
			}

			if ( item.key_type.toLowerCase() === 'popup' ) {
				btnUpdated = [
					...btnUpdated.filter((iitem) => { return item.key_name.toLowerCase() !== iitem.currentValue.name.toLowerCase() }),
					{
						id: new Date().getTime() + idx,
						isEdit: false,
						currentValue: {
							paramtype: 'text',
							name: item.key_name,
							value: item.key_value,
						}
					}
				]
			}
		});

		setInputSocialsList(socialsUpdated);
		setInputProjectParamList(projectUpdated);
		setInputBtnParamList(btnUpdated);

	}, [ displayToEdit ])

	const getHelpPopup = () => {
		if ( !showHelpPopup ) { return null; }
		return (
			<div className="modal">
				<div
					className="modal__inner"
					onMouseDown={(e) => {
						e.stopPropagation();
						if ((e.target as HTMLTextAreaElement).className === 'modal__inner' || (e.target as HTMLTextAreaElement).className === 'container') {
							setShowHelpPopup(false);
						}
					}}
				>
					<div className="modal__bg"></div>
					<div className="container">
						<div className="modal__content">
							<div
								className="modal__close"
								onClick={() => { setShowHelpPopup(false) }}
							>
								<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
									<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
								</svg>
							</div>
							<div className="c-add">
								<div className="c-add__text mb-0">
									<div className="h2 mb-4">Project card view </div>
									<div>
										<img className="img-fluid" src={ example_img } style={{ outline: 'red dashed 1px' }} alt="" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
	const getHelpBlock = () => {
		return (
			<div className="lp-project-info">
				<div className="row">
					<div className="col-md-8">
						<p>If you want to make a fundraising widget, then these three blocks display general information about the project:</p>
						<p>1. Project name, a brief description, and links to the project's social networks; </p>
						<p>2. information about the token (general issue, ticker, etc.);</p>
						<p>3. information about fees (if allowed).</p>
					</div>
					<div className="col-md-4">
						<div className="mb-4">
							<img
								className="img-fluid"
								src={ example_img }
								alt=""
								onClick={() =>{ setShowHelpPopup(true) }}
							/>
						</div>
					</div>
					<div className="col-md-12">
						<p className="mb-0 text-muted">If you want to set up the Launchpad for something other than fundraising, you can configure the settings differently. </p>
					</div>
				</div>
			</div>
		)
	}

	const getSocialUrlHint = () => {
		const foundSocial = SOCIAL_NETWORKS_LIST.find((item) => { return item.label.toLowerCase() === inputSocialsName.toLowerCase() });
		if ( foundSocial && foundSocial.urlPrefix && foundSocial.urlPrefix !== '' ) {
			return ( <div className="input-error" style={{ color: '#51eeda' }}>Username only</div> )
		}

		return ( <div className="input-error" style={{ color: '#51eeda' }}>Full link (https://...)</div> )
	}
	const getBasicInfoBlock = () => {
		return (
			<div className="lp-step-block">
				<div className="lp-step-block__num">
					<span className="num">1</span>
					<div className="divider left"></div>
				</div>
				<div className="row">
					<div className="col-md-6">
						<div className="input-group">
							<label className="input-label">Title</label>
							<input
								className="input-control"
								type="text"
								placeholder=""
								value={ inputTitle }
								onChange={(e) => {
									setInputTitle(e.target.value);
									setHasChanges(true);
								}}
							/>
						</div>
						<div className="input-group">
							<label className="input-label">Token</label>
							<input
								className="input-control"
								type="text"
								placeholder=""
								value={ inputToken }
								onChange={(e) => {
									const value = e.target.value.replace(/[^a-zA-Z0-9]/g, "");
									setInputToken(value);
									setHasChanges(true);
								}}
							/>
						</div>
					</div>
					<div className="col-md-6">
						<div className="input-group">
							<label className="input-label">Description</label>
							<textarea
								className="input-control h-2-controls"
								value={ inputDescription }
								onChange={(e) => {
									setInputDescription(e.target.value);
									setHasChanges(true);
								}}
							></textarea>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-md-4">
						<div className="input-group">
							<label className="input-label">Social networks</label>
								<InputWithOptions
									blockClass="input-control"
									placeholder=""
									value={ inputSocialsName }
									onChange={(e) => {
										setInputSocialsName(e.target.value);
									}}
									onSelect={(item) => {
										if ( item.value.toLowerCase() === 'custom' ) {
											setInputSocialsName('');
										} else {
											setInputSocialsName(item.value);
										}
									}}
									options={SOCIAL_NETWORKS_LIST.map((item) => { return { value: item.label, label: item.label } })}
								/>
						</div>
					</div>
					<div className="col-md-6">
						<div className="input-group">
							<label className="input-label">Link</label>
							<input
								className="input-control"
								type="text"
								placeholder=""
								value={ inputSocialsLink }
								onChange={(e) => {
									setInputSocialsLink(e.target.value);
								}}
							/>
							{ getSocialUrlHint() }
						</div>
					</div>
					<div className="col-md-2">
						<div className="input-group">
							<label className="input-label d-none d-md-block">&nbsp;</label>
							<button
								className="btn btn-outline w-100"
								disabled={ inputSocialsName === ''  || inputSocialsLink === '' }
								onClick={() => {

									let valueToSave = inputSocialsLink;
									const foundSocial = SOCIAL_NETWORKS_LIST.find((item) => { return inputSocialsName.toLowerCase() === item.label.toLowerCase() });
									if ( foundSocial && foundSocial.urlPrefix && foundSocial.urlPrefix !== '' ) {
										valueToSave = combineURLs(foundSocial.urlPrefix, inputSocialsLink);
									}

									setInputSocialsList([
										...inputSocialsList.filter((item) => { return item.currentValue.name.toLowerCase() !== inputSocialsName.toLowerCase() || item.currentValue.value.toLowerCase() !== inputSocialsLink.toLowerCase() }),
										{
											id: new Date().getTime(),
											isEdit: false,
											currentValue: {
												paramtype: 'text',
												name: inputSocialsName,
												value: valueToSave,
											}
										}
									]);
									setInputSocialsName('');
									setInputSocialsLink('');
									setHasChanges(true);
								}}
							>Add</button>
						</div>
					</div>
				</div>
				<div className="c-wrap__table">
					{
						inputSocialsList.map((item) => {

							let label = ( <img src={ default_social } alt="" /> );
							try { const icon = require(`../../../static/pics/socials/${item.currentValue.name.toLowerCase()}.jpeg`); label = ( <img src={icon} alt="" /> ); } catch (ignored) {}
							try { const icon = require(`../../../static/pics/socials/${item.currentValue.name.toLowerCase()}.jpg` ); label = ( <img src={icon} alt="" /> ); } catch (ignored) {}
							try { const icon = require(`../../../static/pics/socials/${item.currentValue.name.toLowerCase()}.png` ); label = ( <img src={icon} alt="" /> ); } catch (ignored) {}
							try { const icon = require(`../../../static/pics/socials/${item.currentValue.name.toLowerCase()}.svg` ); label = ( <img src={icon} alt="" /> ); } catch (ignored) {}

							return (
								<div className="item" key={ item.id }>
									<div className="row">
										<div className="col-sm-1 col-md-1 mb-2">
											<div className="tb-icon-24">
												{ label }
											</div>
										</div>
										<div className="col-sm-10">
											<span className="text-break">{ item.currentValue.value }</span>
										</div>
										<button
											className="btn-del"
											onClick={() => {
												setInputSocialsList([
													...inputSocialsList.filter((iitem) => { return item.currentValue.name.toLowerCase() !== iitem.currentValue.name.toLowerCase() || item.currentValue.value.toLowerCase() !== iitem.currentValue.value.toLowerCase() }),
												]);
												setHasChanges(true);
											}}
										>
											<img src={ i_del } alt="" />
										</button>
									</div>
								</div>
							)
						})
					}
				</div>
			</div>
		)
	}

	const getProjectInfoRow = (item: EditableField) => {

		if ( item.isEdit ) {
			return (
				<div className="item item-narrow" key={ item.id }>
					<div className="row-edit">
						<div className="col-sm-3 mb-2">
							<input
								className="input-control"
								type="text"
								placeholder=""
								value={ item.editedValue?.name || '' }
								onChange={(e) => {
									const value = e.target.value;
									setInputProjectParamList([
										...inputProjectParamList.filter((iitem) => { return item.id !== iitem.id }),
										{
											...item,
											editedValue: {
												...item.editedValue || { name: '', value: '' },
												name: value,
											},
										}
									]);
								}}
							/>
						</div>
						<div className="col-sm-5 mb-2">
							<input
								className="input-control"
								type="text"
								placeholder=""
								value={ item.editedValue?.value || '' }
								onChange={(e) => {
									const value = e.target.value;
									setInputProjectParamList([
										...inputProjectParamList.filter((iitem) => { return item.id !== iitem.id }),
										{
											...item,
											editedValue: {
												...item.editedValue || { name: '', value: '' },
												value: value,
											},
										}
									]);
								}}
							/>
						</div>
						<div className="col-6 col-sm-2 mb-2">
							<button
								className="btn btn-primary w-100"
								onClick={() => {
									setInputProjectParamList([
										...inputProjectParamList.filter((iitem) => { return item.id !== iitem.id }),
										{
											...item,
											isEdit: false,
											currentValue: {
												name: item.editedValue?.name || item.currentValue.name,
												value: item.editedValue?.value || item.currentValue.value,
											},
											editedValue: undefined,
										}
									]);
									setHasChanges(true);
								}}
							>Save</button>
						</div>
						<div className="col-6 col-sm-2 mb-2">
							<button
								className="btn btn-gray w-100"
								onClick={() => {
									setInputProjectParamList([
										...inputProjectParamList.filter((iitem) => { return item.id !== iitem.id }),
										{
											...item,
											isEdit: false,
											editedValue: undefined,
										}
									])
								}}
							>Undo</button>
						</div>
					</div>
				</div>
			)
		}

		return (
			<div className="item item-narrow" key={ item.id }>
				<div className="row">
					<div className="col-sm-4 mb-2">
						<span className="text-break">{ item.currentValue.name }</span>
					</div>
					<div className="col-sm-5 mb-2">
						<span className="text-break">{ item.currentValue.value }</span>
					</div>
					<div className="col-sm-3 mb-2 d-flex">
						<div className="btns-group mr-0 ml-auto">
							<div className="btn-group">
								<button
									className="btn btn-md btn-gray btn-img"
									onClick={() => {
										setInputProjectParamList([
											...inputProjectParamList.filter((iitem) => { return item.id !== iitem.id }),
											{
												...item,
												isEdit: true,
												editedValue: {
													name: item.currentValue.name,
													value: item.currentValue.value,
												},
											}
										])
									}}
								>
									<img src={ i_edit } alt="" />
								</button>
							</div>
							<div className="btn-group">
								<button
									className="btn btn-md btn-gray btn-img"
									onClick={() => {
										setInputProjectParamList(inputProjectParamList.filter((iitem) => { return item.id !== iitem.id }));
										setHasChanges(true);
									}}
								>
									<img src={ i_del } alt="" />
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
	const getProjectInfoBlock = () => {
		return (
			<div className="lp-step-block">
				<div className="lp-step-block__num">
					<span className="num">2</span>
					<div className="divider left"></div>
				</div>
				<div className="row mb-4">
					<div className="col-md-10">
						<div>Here you specify the parameters that will display the quantitative characteristics of the project (number of tokens, for example).</div>
					</div>
				</div>
				<div className="row">
					<div className="col-md-4">
						<div className="input-group">
							<label className="input-label">Parameter</label>
							<InputWithOptions
								blockClass="input-control"
								placeholder=""
								value={ inputProjectParam }
								onChange={(e) => { setInputProjectParam(e.target.value) }}
								onSelect={(item) => { setInputProjectParam(item.value) }}
								options={ PROJECT_PARAMS_LIST.map((item) => { return { value: item, label: item } }) }
							/>
						</div>
					</div>
					<div className="col-md-6">
						<div className="input-group">
							<label className="input-label">Value</label>
							<input
								className="input-control"
								type="text"
								placeholder=""
								value={ inputProjectValue }
								onChange={(e) => { setInputProjectValue(e.target.value) }}
							/>
						</div>
					</div>
					<div className="col-md-2">
						<div className="input-group">
							<label className="input-label d-none d-md-block">&nbsp;</label>
							<button
								className="btn btn-outline w-100"
								disabled={ inputProjectParam === ''  || inputProjectValue === '' }
								onClick={() => {
									setInputProjectParamList([
										...inputProjectParamList.filter((item) => { return item.currentValue.name.toLowerCase() !== inputProjectParam.toLowerCase() }),
										{
											id: new Date().getTime(),
											isEdit: false,
											currentValue: {
												name: inputProjectParam,
												value: inputProjectValue,
											}
										}
									]);
									setInputProjectParam('');
									setInputProjectValue('');
									setHasChanges(true);
								}}
							>Add</button>
						</div>
					</div>
				</div>
				<div className="c-wrap__table">
					{ inputProjectParamList.sort((item, prev) => { return item.id - prev.id }).map((item) => { return getProjectInfoRow(item) }) }
				</div>
			</div>
		)
	}

	const getLinksRow = (item: EditableField) => {
		if ( item.isEdit ) {
			return (
				<div className="item item-narrow" key={ item.id }>
					<div className="row-edit">
						<div className="col-sm-3 mb-2 order-sm-1">
							<InputWithOptions
								blockClass="input-control"
								placeholder=""
								value={ item.editedValue?.paramtype || 'link' }
								onChange={(e) => { }}
								onSelect={(iitem) => {
									setInputBtnParamList([
										...inputBtnParamList.filter((iiitem) => { return item.id !== iiitem.id }),
										{
											...item,
											editedValue: {
												...item.editedValue || { name: '', value: '', paramtype: 'Link' },
												paramtype: iitem.value,
											},
										}
									])
								}}
								options={[
									{ value: 'Link', label: 'Link' },
									{ value: 'Text', label: 'Text' },
								]}
							/>
						</div>
						<div className="col-sm-5 mb-2 order-sm-2">
							<input
								className="input-control"
								type="text"
								placeholder=""
								value={ item.editedValue?.name }
								onChange={(e) => {
									const value = e.target.value;
									setInputBtnParamList([
										...inputBtnParamList.filter((iitem) => { return item.id !== iitem.id }),
										{
											...item,
											editedValue: {
												...item.editedValue || { name: '', value: '', paramtype: 'Link' },
												name: value,
											},
										}
									])
								}}
							/>
						</div>
						<div className="col-sm-8 mb-2 order-sm-5">
							<textarea
								className="input-control"
								rows={ 3 }
								value={ item.editedValue?.value || '' }
								onChange={(e) => {
									const value = e.target.value;
									setInputBtnParamList([
										...inputBtnParamList.filter((iitem) => { return item.id !== iitem.id }),
										{
											...item,
											editedValue: {
												...item.editedValue || { name: '', value: '', paramtype: 'Link' },
												value: value,
											},
										}
									])
								}}
							>
							</textarea>
						</div>
						<div className="col-6 col-sm-2 mb-2 order-sm-3">
							<button
								className="btn btn-primary w-100"
								onClick={() => {
									setInputBtnParamList([
										...inputBtnParamList.filter((iitem) => { return item.id !== iitem.id }),
										{
											...item,
											isEdit: false,
											currentValue: {
												paramtype: item.editedValue?.paramtype || item.currentValue.paramtype || 'Link',
												name: item.editedValue?.name || item.currentValue.name,
												value: item.editedValue?.value || item.currentValue.value,
											},
											editedValue: undefined,
										}
									])
								}}
							>
								Save
							</button>
						</div>
						<div className="col-6 col-sm-2 order-sm-4">
							<button
								className="btn btn-gray w-100"
								onClick={() => {
									setInputBtnParamList([
										...inputBtnParamList.filter((iitem) => { return item.id !== iitem.id }),
										{
											...item,
											isEdit: false,
											editedValue: undefined,
										}
									])
								}}
							>
								Close
							</button>
						</div>
					</div>
				</div>
			)
		}

		return (
			<div className="item item-narrow" key={ item.id }>
				<div className="row">
					<div className="col-sm-2 col-lg-1 mb-2 order-sm-1">
						<span className="text-break">{ item.currentValue.paramtype }</span>
					</div>
					<div className="col-sm-7 col-lg-2 mb-2 order-sm-2">
						<span className="text-break">{ item.currentValue.name }</span>
					</div>
					<div className="col-sm-12 col-lg-6 mb-2 order-sm-4 order-lg-3">
						<span className="text-break">
							{ item.currentValue.value }
						</span>
					</div>
					<div className="col-sm-3 col-lg-3 mb-2 d-flex order-sm-3 order-lg-4">
						<div className="btns-group mr-0 ml-auto">
							<div className="btn-group">
								<button
									className="btn btn-md btn-gray btn-img"
									onClick={() => {
										setInputBtnParamList([
											...inputBtnParamList.filter((iitem) => { return item.id !== iitem.id }),
											{
												...item,
												isEdit: true,
												editedValue: {
													paramtype: item.currentValue.paramtype,
													name: item.currentValue.name,
													value: item.currentValue.value,
												},
											}
										])
									}}
								>
									<img src={ i_edit } alt="" />
								</button>
							</div>
							<div className="btn-group">
								<button
									className="btn btn-md btn-gray btn-img"
									onClick={() => { setInputBtnParamList(inputBtnParamList.filter((iitem) => { return item.id !== iitem.id })) }}
								>
									<img src={ i_del } alt="" />
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
	const getLinksBlock = () => {
		return (
			<div className="lp-step-block">
				<div className="lp-step-block__num">
					<span className="num">3</span>
					<div className="divider left"></div>
				</div>
				<div className="row mb-4">
					<div className="col-md-10">
						<div>Here you can specify any textual information about the project, including links and descriptions.</div>
					</div>
				</div>
				<div className="row">
					<div className="col-md-10">
						<div className="row">
							<div className="col-md-4">
								<div className="input-group">
								<label className="input-label">Type</label>
								<div className="select-custom select-token">
									<InputWithOptions
										blockClass="input-control"
										placeholder=""
										value={ inputBtnType }
										onChange={(e) => {}}
										onSelect={(item) => { setInputBtnType(item.value) }}
										options={[
											{ value: 'Link', label: 'Link' },
											{ value: 'Text', label: 'Text' },
										]}
									/>
								</div>
								</div>
							</div>
							<div className="col-md-8">
								<div className="input-group">
									<label className="input-label">Parameter</label>
									<div className="select-custom select-token">
										<InputWithOptions
											blockClass="input-control"
											placeholder=""
											value={ inputBtnParam }
											onChange={(e) => { setInputBtnParam(e.target.value) }}
											onSelect={(item) => { setInputBtnParam(item.value) }}
											options={ LINK_PARAMS_LIST.map((item) => { return { value: item, label: item } }) }
										/>
									</div>
								</div>
							</div>
							<div className="col-md-12">
								<div className="input-group">
									<label className="input-label">Content</label>
									<textarea
										className="input-control"
										rows={ 2 }
										value={ inputBtnValue }
										onChange={(e) => { setInputBtnValue(e.target.value) }}
									></textarea>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-2">
						<div className="input-group">
							<label className="input-label d-none d-md-block">&nbsp;</label>
							<button
								className="btn btn-outline w-100"
								disabled={ inputBtnType === '' || inputBtnParam === ''  || inputBtnValue === '' }
								onClick={() => {
									setInputBtnParamList([
										...inputBtnParamList.filter((item) => { return item.currentValue.name.toLowerCase() !== inputBtnParam.toLowerCase() }),
										{
											id: new Date().getTime(),
											isEdit: false,
											currentValue: {
												paramtype: inputBtnType,
												name: inputBtnParam,
												value: inputBtnValue,
											},
										}
									]);
									setInputBtnType('');
									setInputBtnParam('');
									setInputBtnValue('');
								}}
							>Add</button>
						</div>
					</div>
				</div>
				<div className="c-wrap__table">
					{ inputBtnParamList.sort((item, prev) => { return item.id - prev.id }).map((item) => { return getLinksRow(item) }) }
				</div>
			</div>
		)
	}

	const updateSubmit = async () => {

		if ( !web3 ) { return; }
		if ( !displayToEdit ) { return; }
		if ( !userAddress ) { return; }

		if ( !hasChanges ) {
			navigate(`/price/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.nameHash}`);
			return;
		}

		const output: Array<DisplayParam> = [
			{ key_name: 'title', key_type: 'basic', key_value: inputTitle },
			{ key_name: 'description', key_type: 'basic', key_value: inputDescription },
			{ key_name: 'network', key_type: 'basic', key_value: inputToken },
			...inputSocialsList.map((item) => {

				let valueToSave = item.currentValue.value;
				// const foundSocial = SOCIAL_NETWORKS_LIST.find((iitem) => { return item.currentValue.name.toLowerCase() === iitem.label.toLowerCase() });
				// if ( foundSocial && foundSocial.urlPrefix && foundSocial.urlPrefix !== '' ) {
				// 	valueToSave = combineURLs(foundSocial.urlPrefix, item.currentValue.value);
				// }

				return {
					key_name: item.currentValue.name, key_type: "socials", key_value: valueToSave
				}
			}),
			...inputProjectParamList.map((item) => {
				return {
					key_name: item.currentValue.name, key_type: "project", key_value: item.currentValue.value
				}
			}),
			...inputBtnParamList.map((item) => {
				return {
					key_type: item.currentValue.paramtype === 'Link' ? 'links' : 'popup' , key_name: item.currentValue.name, key_value: item.currentValue.value
				}
			}),
		];

		const loaderStages: Array<AdvancedLoaderStageType> = [
			{
				id: 'sign',
				sortOrder: 3,
				text: `Signing message for oracle`,
				status: _AdvancedLoadingStatus.queued
			},
			{
				id: 'api',
				sortOrder: 4,
				text: `Updating params in oracle`,
				status: _AdvancedLoadingStatus.queued
			},
		];
		const advLoader = {
			title: 'Waiting to create',
			stages: loaderStages
		};
		createAdvancedLoader(advLoader);

		updateStepAdvancedLoader({
			id: 'sign',
			status: _AdvancedLoadingStatus.loading
		});

		let sign;
		try {
			sign = await web3.eth.personal.sign(`Showcase: ${displayToEdit.nameHash}`, userAddress, '');
		} catch(e: any) {
			setModal({
				type: _ModalTypes.error,
				title: `Cannot sign message`,
				details: [
					`User address: ${userAddress}`,
					`Showcase hash: ${displayToEdit.nameHash}`,
					'',
					e.message || e,
				]
			});
			return;
		}

		updateStepAdvancedLoader({
			id: 'sign',
			status: _AdvancedLoadingStatus.complete
		});
		updateStepAdvancedLoader({
			id: 'api',
			status: _AdvancedLoadingStatus.loading
		});

		try {
			await updateDisplayParamsInAPI(
				displayToEdit.chainId,
				displayToEdit.contractAddress,
				displayToEdit.nameHash,
				sign,
				output
			)
		} catch(e: any) {
			setModal({
				type: _ModalTypes.error,
				title: `Cannot update project params`,
				details: [
					`User address: ${userAddress}`,
					`Showcase hash: ${displayToEdit.nameHash}`,
					'',
					e.message || e,
				]
			});
			return;
		}

		setModal({
			type: _ModalTypes.success,
			text: [{ text: 'Project params successfully updated' }],
			buttons: [{
				text: 'Ok',
				clickFunc: () => {
					unsetModal();
					navigate(`/price/${displayToEdit?.chainId || ''}/${displayToEdit?.contractAddress || ''}/${displayToEdit?.nameHash || ''}`);
					props.onUpdate();
				}
			}],
			// links: [{
			// 	text: `View showcase`,
			// 	url: `/launchpad/${displayToEdit?.chainId || ''}/${displayToEdit?.contractAddress || ''}/${displayToEdit?.nameHash || ''}`
			// }]
		});
	}

	const saveBtnDsabled = (): boolean => {
		if ( !!inputBtnParamList.find((item) => { return item.isEdit }) ) { return true; }
		if ( !!inputProjectParamList.find((item) => { return item.isEdit }) ) { return true; }

		return false;
	}

	const getBackBtn = () => {
		if ( hasChanges ) {
			return (
				<button
					className="btn btn-gray"
					onClick={() => {
						setModal({
							type: _ModalTypes.info,
							title: 'You have unsaved data. Continue?',
							buttons: [
								{
									text: 'Yes',
									clickFunc: () => {
										navigate(`/`);
										unsetModal();
									},
								},
								{
									text: 'No',
									clickFunc: () => { unsetModal(); },
									clazz: 'btn-grad'
								},
							]
						})
					}}
				>Back</button>
			)
		}

		if ( !displayToEdit ) {
			return (
				<Link
					className="btn btn-gray"
					to={ `/` }
				>Back</Link>
			)
		}
		return (
			<Link
				className="btn btn-gray"
				to={ `/showcase/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.nameHash}` }
			>Back</Link>
		)
	}

	return (
		<>
		<div className="container">

			<div className="breadcrumbs">
				<Link className="item-back btn btn-sm btn-gray" to="/">
					<svg className="mr-2" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M12 5.25H2.8725L7.065 1.0575L6 0L0 6L6 12L7.0575 10.9425L2.8725 6.75H12V5.25Z" fill="white"></path>
					</svg>
					<span>To the showcase list</span>
				</Link>
			</div>

			<div className="row mb-4">
				<div className="col-12 col-sm">
					<div className="h3 mb-3 mt-0">{ displayToEdit?.title || '<Untitled>' }</div>
				</div>
				{
					displayToEdit ? (
						<div className="col-12 col-sm-auto">
							<div className="btns-group">
								<div className="btn-group">
									<a
										className="btn btn-md btn-gray"
										href={ `/launchpad/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.name}` }
										target="_blank" rel="noopener noreferrer"
									>Go to the showcase</a>
								</div>
								<div className="btn-group">
								<CopyToClipboard
									text={ `${window.location.origin}/launchpad/${displayToEdit.chainId}/${displayToEdit.contractAddress}/${displayToEdit.name}` }
									onCopy={() => {
										setDisplayLinkCopied(true);
										setTimeout(() => { setDisplayLinkCopied(false); }, 5*1000);
									}}
								>
									<button className="btn btn-md btn-gray btn-img">
										<img src= { i_copy } alt="" />
										<span className="btn-action-info" style={{ display: displayLinkCopied ? 'block' : 'none' }}>Copied</span>
									</button>
								</CopyToClipboard>
								</div>
							</div>
						</div>
					) : null
				}
			</div>
			<div className="row">
				<div className="col-md-2 col-lg-2">
					<StepsProgress current="project" display={ displayToEdit } confirmClose={ hasChanges } />
				</div>
				<div className="col-md-10 col-lg-10">
					<div className="lp-wrap">
						<div className="lp-wrap__header">
							<div className="h3-wrap">Project</div>
						</div>

						{ getHelpBlock() }
						{ getBasicInfoBlock() }
						{ getProjectInfoBlock() }
						{ getLinksBlock() }

						<div className="lp-steps-footer">
							<div className="row justify-content-between">
								<div className="col-auto">
									{ getBackBtn() }
								</div>
								<div className="col-auto">
									<button
										className="btn btn-grad"
										disabled={ saveBtnDsabled() }
										onClick={ updateSubmit }
									>Save and proceed</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		{ getHelpPopup() }
		</>
	)
}