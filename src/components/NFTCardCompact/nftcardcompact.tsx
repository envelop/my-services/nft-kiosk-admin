
import { useState } from "react";

import CoinIconViewer  from "../CoinIconViewer";
import CoinViewer      from "../CoinViewer";
import TippyWrapper from "../TippyWrapper";

import {
	NFTorWNFT,
	_AssetType,
	compactString,
} from "@envelop/envelop-client-core"

import icon_loading from '../../static/pics/loading.svg';
import default_nft  from '@envelop/envelop-client-core/static/pics/_default_nft.svg';

type NFTCardCompactProps = {
	token: NFTorWNFT,
	checked: boolean,
	onSelect: (token: NFTorWNFT) => void,
	onUnselect: (token: NFTorWNFT) => void,
}

export default function NFTCardCompact(props: NFTCardCompactProps) {

	const {
		token,
		checked,
		onSelect,
		onUnselect,
	} = props;

	const [ coinListOpened, setCoinListOpened ] = useState(false);

	const ITEMS_TO_SHOW = 5;

	let additionalAssetssQty = token.collateral ? token.collateral.length - ITEMS_TO_SHOW : 0;

	const getTokenMedia = () => {
		if ( token.image === undefined || token.image === 'service:pending' || token.image === 'service:loading' ) {
			return (
				<div className="img">
					<div className="inner">
						<div className="default">
							<img src={ icon_loading } alt="" />
						</div>
					</div>
				</div>
			)
		}

		if ( token.image === null || token.image === 'service:cannotload' ) {
			return (
				<div className="img">
					<div className="inner">
						<div className="default">
							<img src={ default_nft } alt="" />
						</div>
					</div>
				</div>
			)
		}

		if ( token.thumbnail ) {
			return (
				<div className="img">
					<div className="inner">
						<img className="img" src={ token.thumbnail } alt="" />
					</div>
				</div>
			)
		}

		return (
			<div className="img">
				<div className="inner">
					<img className="img" src={ token.image } alt="" />
				</div>
			</div>
		)
	}

	return (
		<div
			key={ `${token.contractAddress}${token.tokenId}` }
			className="col-6 col-sm-4 col-md-3 col-lg-2 mb-4"
			onClick={() => {
				if ( checked ) {
					onUnselect(token);
				} else {
					onSelect(token);
				}
			}}
		>
			<div className={ `lp-batch-card ${ checked ? 'checked' : '' } `}>

				{ getTokenMedia() }

				<div className="check"></div>
				<div className="info">
					<TippyWrapper msg={ token.contractAddress }>
						<div className="mb-2"><span className="text-muted">Address</span> { compactString(token.contractAddress) }</div>
					</TippyWrapper>
					<TippyWrapper msg={ token.tokenId }>
						<div className="mb-2"><span className="text-muted">ID</span> { compactString(token.tokenId) }</div>
					</TippyWrapper>
					{
						token.collateral && token.collateral.length ? (
							<div className="position-relative">
								<div
									className="field-collateral"
									onMouseEnter={() => { setCoinListOpened(true) }}
									onMouseLeave={() => { setCoinListOpened(false) }}
								>
									<CoinIconViewer tokens={token.collateral.slice(0,ITEMS_TO_SHOW)} />
									{
										token.collateral.length > ITEMS_TO_SHOW ?
										(
											<span className="more-assets">
												{ '+' }
												{ additionalAssetssQty || '' }
												{ ' ' }
												{ additionalAssetssQty > 0 ? ( additionalAssetssQty === 1 ? 'asset' : 'assets' ) : 'no assets' }
											</span>
										) : null
									}
									{
									coinListOpened ? (
										<CoinViewer
											tokens = { token.collateral }
										/>
									) : null
								}
								</div>
							</div>
						) : null
					}
				</div>
			</div>
		</div>
	)
}