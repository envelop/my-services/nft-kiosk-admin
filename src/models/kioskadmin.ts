import {
	BigNumber,
	DiscountUntil,
	DisplayParam,
	NFTToAssetItem,
	NFTorWNFT,
	Price,
	Web3,
	_DenominatedPrice,
	combineURLs,
	createAuthToken,
	createContract,
	decodePrice,
	encodeDiscountUntil,
	encodePrice,
	getChainId,
	getDefaultWeb3,
	getNFTById,
	getStrHash,
	getUserAddress,
	getWNFTById,
	DenominatedPrice,
	NFT,
	_AssetItem,
	_Price,
	decodeDenominatedPrice,
	encodeDenominatedPrice,
	_Lock,
	_Discount,
	processSwarmUrl,
	fetchTokenJSON,
	decodeCollaterals,
	decodeLocks,
} from "@envelop/envelop-client-core";

type _DisplayItem = {
	asset_type: number,
	blocknumber: number,
	chain_id: number,
	contract_address: string,
	discounts: Array<_Discount>,
	display_id: number,
	id: number,
	implicit_added: boolean,
	locks: Array<_Lock>,
	logindex: number,
	prices: Array<_Price>,
	sell_tx_hash: string,
	status: string,
	token_id: number,
	token_uri: string,
	collateral?: Array<_AssetItem>
}

export const updateDisplayInChain = async (
	web3: Web3,
	kioskAddress: string,
	displayName: string,
	beneficiary: string,
	enableAfter: BigNumber,
	disableAfter: BigNumber,
	priceModel: string
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'kiosk', kioskAddress);

	const tx = contract.methods.setDisplayParams(
		displayName,
		beneficiary,
		enableAfter.toString(),
		disableAfter.toString(),
		priceModel
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}

export const setDefaultPriceInChain = async (
	web3: Web3,
	priceModelAddress: string,
	displayHash: string,
	prices: Array<Price>,
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'priceModel', priceModelAddress);

	const pricesParsed = prices.map((item) => { return encodePrice(item) });
	const tx = contract.methods.setDefaultNFTPriceForDisplay(
		displayHash,
		pricesParsed
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
export const editDefaultPriceInChain = async (
	web3: Web3,
	priceModelAddress: string,
	displayHash: string,
	price: Price,
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'priceModel', priceModelAddress);

	const priceParsed = encodePrice(price);
	const tx = contract.methods.editDefaultNFTPriceRecordForDisplay(
		displayHash,
		price.idx,
		priceParsed
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
export const deleteDefaultPriceInChain = async (
	web3: Web3,
	priceModelAddress: string,
	displayHash: string,
	priceIdx: number,
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'priceModel', priceModelAddress);
	const tx = contract.methods.deleteDefaultNFTPriceRecordForDisplayByIndex(
		displayHash,
		priceIdx,
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
export const setCollateralPriceInChain = async (
	web3: Web3,
	priceModelAddress: string,
	displayHash: string,
	collateralToken: string,
	prices: Array<DenominatedPrice>,
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'priceModel', priceModelAddress);

	const parsedPrices = prices.map((item) => { return encodeDenominatedPrice(item) });

	const tx = contract.methods.setCollateralPriceForDisplay(
		displayHash,
		collateralToken,
		parsedPrices
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
export const editCollateralPriceInChain = async (
	web3: Web3,
	priceModelAddress: string,
	displayHash: string,
	collateralToken: string,
	priceIdx: number,
	price: DenominatedPrice,
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'priceModel', priceModelAddress);

	const parsedPrice = encodeDenominatedPrice(price);

	const tx = contract.methods.editCollateralPriceRecordForDisplay(
		displayHash,
		collateralToken,
		priceIdx,
		parsedPrice
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
export const deleteCollateralPriceInChain = async (
	web3: Web3,
	priceModelAddress: string,
	displayHash: string,
	collateralToken: string,
	priceIdx: number,
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'priceModel', priceModelAddress);

	const tx = contract.methods.deleteCollateralPriceRecordForDisplayByIndex(
		displayHash,
		collateralToken,
		priceIdx,
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
export const setTimeDiscountInChain = async (
	web3: Web3,
	priceModelAddress: string,
	displayHash: string,
	discounts: Array<DiscountUntil>,
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'priceModel', priceModelAddress);

	const discountsParsed = discounts.map((item) => { return encodeDiscountUntil(item) });
	const tx = contract.methods.setTimeDiscountsForDisplay(
		displayHash,
		discountsParsed
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
export const editTimeDiscountInChain = async (
	web3: Web3,
	priceModelAddress: string,
	displayHash: string,
	discountIdx: number,
	discount: DiscountUntil,
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'priceModel', priceModelAddress);

	const discountParsed = encodeDiscountUntil(discount);
	const tx = contract.methods.editTimeDiscountsForDisplay(
		displayHash,
		discountIdx,
		discountParsed
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
export const setPromocodeDiscountInChain = async (
	web3: Web3,
	priceModelAddress: string,
	displayHash: string,
	promocode: string,
	discount: DiscountUntil,
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'priceModel', priceModelAddress);

	const tx = contract.methods.setPromoDiscountForDisplay(
		displayHash,
		getStrHash(promocode),
		encodeDiscountUntil(discount)
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
export const setReferrerDiscountInChain = async (
	web3: Web3,
	priceModelAddress: string,
	displayHash: string,
	referrer: string,
	discount: DiscountUntil,
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'priceModel', priceModelAddress);

	const tx = contract.methods.setRefereerDiscountForDisplay(
		displayHash,
		referrer,
		encodeDiscountUntil(discount)
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}

export const addTokensBatchToDisplay = async (
	web3: Web3,
	kioskAddress: string,
	displayHash: string,
	tokens: Array<NFTorWNFT>,
	prices?:  Array<Price>
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'kiosk', kioskAddress);

	const tokensParsed = tokens.map((item)  => { return NFTToAssetItem(item) });
	const pricesParsed = prices?.map((item) => { return encodePrice(item)    }) || [];
	const tx = contract.methods.addBatchItemsToDisplayWithSamePrice(
		displayHash,
		tokensParsed,
		pricesParsed,
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
export const removeTokenFromDisplay = async (
	web3: Web3,
	kioskAddress: string,
	token: NFTorWNFT,
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'kiosk', kioskAddress);

	const tx = contract.methods.buyAssetItem(
		NFTToAssetItem(token),
		0,
		userAddress,
		'0x0000000000000000000000000000000000000000',
		''
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
export const addTokenPrice = async (
	web3: Web3,
	kioskAddress: string,
	token: NFT,
	prices: Array<Price>
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'kiosk', kioskAddress);

	const pricesParsed = prices.map((item) => { return encodePrice(item) });
	const tx = contract.methods.addAssetItemPriceAtIndex(
		NFTToAssetItem(token),
		pricesParsed,
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
export const editTokenPrice = async (
	web3: Web3,
	kioskAddress: string,
	token: NFT,
	price: Price
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'kiosk', kioskAddress);

	const priceParsed = encodePrice(price);
	const tx = contract.methods.editAssetItemPriceAtIndex(
		NFTToAssetItem(token),
		price.idx,
		priceParsed,
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
export const removeTokenPrice = async (
	web3: Web3,
	kioskAddress: string,
	token: NFT,
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'kiosk', kioskAddress);

	const tx = contract.methods.removeLastPersonalPriceForAssetItem( NFTToAssetItem(token) );

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}

export const updateDisplayInAPI = async (
	web3: Web3,
	kioskAddress: string,
	displayHash: string,
	sign: string,
	data: {
		title: string,
		description: string,
		show_widget: boolean,
	}
) => {

	const chainId = await getChainId(web3);
	if ( !chainId ) { throw new Error('No chainId'); }

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		const err = 'Cannot create auth token';
		console.log(err);
		throw new Error(err);
	}
	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) {
		console.log('No oracle base url in .env');
		throw new Error('No oracle base url in .env');
	}

	const url = combineURLs(BASE_URL, `/kiosk/display/${chainId}/${kioskAddress}/${displayHash}`);

	let respParsed = undefined;
	try {
		const resp = await fetch(url, {
			method: 'POST',
			headers: {
				'Authorization': authToken,
			},
			body: JSON.stringify({
				sign,
				...data
			})
		});

		if ( resp && resp.ok ) {
			respParsed = await resp.json();
		}
	} catch (e) {
		console.log('Cannot update showcase', e);
		throw new Error('Cannot update showcase');
	}

	if ( !respParsed ) {
		console.log('Cannot update showcase');
		throw new Error('Cannot update showcase');
	}

	return respParsed;
}

export const updateDisplayParamsInAPI = async (
	chainId: number,
	kioskAddress: string,
	displayHash: string,
	sign: string,
	params: Array<DisplayParam>
) => {

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		const err = 'Cannot create auth token';
		console.log(err);
		throw new Error(err);
	}
	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) {
		console.log('No oracle base url in .env');
		throw new Error('No oracle base url in .env');
	}

	const url = combineURLs(BASE_URL, `/kiosk/display_params/${chainId}/${kioskAddress}/${displayHash}`);

	let respParsed = undefined;
	try {
		const resp = await fetch(url, {
			method: 'POST',
			headers: {
				'Authorization': authToken,
			},
			body: JSON.stringify({
				sign,
				params
			})
		});

		if ( resp && resp.ok ) {
			respParsed = await resp.json();
		}
	} catch (e) {
		console.log('Cannot update showcase', e);
		throw new Error('Cannot update showcase');
	}

	if ( !respParsed ) {
		console.log('Cannot update showcase');
		throw new Error('Cannot update showcase');
	}

	return respParsed;
}

export const getDisplayDefaultPricesChain = async (
	chainId: number,
	priceModelAddress: string,
	displayHash: string
) => {
	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}

	const contract = await createContract(web3, 'pricemodel', priceModelAddress);

	// let currIdx = 0;
	// let prices: Array<Price> = [];
	// while (true) {
	// 	try {
	// 		const priceItem = await contract.methods.defaultNFTPriceForDisplay(displayHash, currIdx).call();
	// 		prices = [
	// 			...prices,
	// 			decodePrice(priceItem, currIdx)
	// 		];
	// 		currIdx += 1;
	// 	} catch(e: any) {
	// 		break;
	// 	}
	// }
	// return prices;

	const prices = await contract.methods.getDefaultDisplayPricesForDisplay(displayHash).call();
	const pricesParsed: Array<Price> = prices.map((item: _Price, idx: number) => { return decodePrice(item, idx) })
	return pricesParsed;
}

export const getDisplayCollateralUnitPricesChain = async (
	chainId: number,
	priceModelAddress: string,
	displayHash: string,
	erc20AddressesToCheck: Array<string>
): Promise<Array<{collateralToken: string, price: Array<DenominatedPrice> }>> => {
	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}

	const contract = await createContract(web3, 'pricemodel', priceModelAddress);

	let prices: Array<{collateralToken: string, price: Array<DenominatedPrice> }> = [];

	for (const idx in erc20AddressesToCheck) {
		const item = erc20AddressesToCheck[idx];

		const price = await contract.methods.getCollateralUnitPrice(displayHash, item).call();
		if ( price.length === 0 ) { continue; }
		prices.push({
			collateralToken: item,
			price: price.map((iitem: _DenominatedPrice, idx: number) => {
				return decodeDenominatedPrice(iitem, idx)
			})
		});
	}

	return prices;
}

export const getAssetItemsOfDisplayChain = async (
	chainId: number,
	kioskAddress: string,
	displayHash: string,
): Promise<Array<{ token: NFTorWNFT, tokenType: string, prices: Array<Price> | undefined }>> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}

	const contract = await createContract(web3, 'kiosk', kioskAddress);

	const items = await contract.methods.getDisplayAssetItems(displayHash).call();
	const output: Array<{ token: NFTorWNFT, tokenType: string, prices: Array<Price> | undefined }> = [];
	for (const idx in items) {
		const item = items[idx];
		const wnft = await getWNFTById(chainId, item.nft.asset.contractAddress, item.nft.tokenId);
		if ( wnft ) {
			output.push({
				token: wnft,
				tokenType: 'wnft',
				prices: item.prices.length ? item.prices.map((iitem: _Price, idx: number) => { return decodePrice(iitem, idx) }) : undefined,
			})
		} else {
			const nft = await getNFTById(chainId, item.nft.asset.contractAddress, item.nft.tokenId);
			if ( nft ) {
				output.push({
					token: nft,
					tokenType: 'nft',
					prices: item.prices.length ? item.prices.map((iitem: _Price, idx: number) => { return decodePrice(iitem, idx) }) : undefined,
				})
			}
		}

	}

	return output;
}
export const getAssetItemsPriceOfDisplayChain = async (
	chainId: number,
	kioskAddress: string,
	displayHash: string,
): Promise<Array<{ nft: _AssetItem, owner: string, prices: Array<_Price> }>> => {
	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}

	const contract = await createContract(web3, 'kiosk', kioskAddress);

	return await contract.methods.getDisplayAssetItems(displayHash).call();
}
export const getAssetItemsOfDisplayAPI = async (
	chainId: number,
	kioskAddress: string,
	displayHash: string,
	page: number,
	size: number,
): Promise<Array<{ token: NFTorWNFT, tokenType: string }>> => {
	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		console.log('Cannot create token');
		throw new Error('Cannot create token');
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) {
		console.log('No oracle base url in .env');
		throw new Error('No oracle base url in .env');
	}

	const url = combineURLs(BASE_URL, `/kiosk/tokens/${chainId}/${kioskAddress}/${displayHash}?page=${page}&size=${size}`);

	let respParsed: Array<_DisplayItem> = [];
	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});
		if ( resp && resp.ok ) {
			respParsed = await resp.json();
		}
	} catch (e) {
		console.log('Cannot fetch token from oracle', e);
		throw e;
	}

	return respParsed.map((item) => {
		return {
			token: {
				assetType: item.asset_type,
				chainId: chainId,
				contractAddress: item.contract_address,
				tokenId: `${item.token_id}`,
				tokenUrlRaw: item.token_uri,
				tokenUrl: processSwarmUrl(item.token_uri),
				image: 'service:pending',
				collateral: item.collateral ? decodeCollaterals(item.collateral) : [],
				locks: decodeLocks(item.locks),
				blockNumber: `${item.blocknumber}`,
				logIndex: `${item.logindex}`,
			},
			tokenType: item.collateral && item.collateral.length ? 'wnft' : 'nft',
		}
	})
}

export const getCountOfAssetItemsOfDisplayChain = async (
	chainId: number,
	kioskAddress: string,
	displayHash: string,
): Promise<number> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}

	const contract = await createContract(web3, 'kiosk', kioskAddress);

	const items = await contract.methods.getDisplayAssetItems(displayHash).call();
	return items.length;
}

